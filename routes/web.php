<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {

  

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/', function () {
        return redirect('home');

   });
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/contact', function () {
    return view('contact');
});

Auth::routes();



Route::get('/home', 'HomeController@index')->name('home');

Route::get('/homelist', 'FileController@homelist')->name('homelist');



Route::get('profile', 'UserController@profile');

Route::post('profile', 'UserController@update_avatar');

Route::post('signature', 'UserController@update_signature')->name('signature');

Route::post('contact', 'ContactController@store')->name('contact');

Route::get('verify-user/{code}','Auth\RegisterController@activateUser')->name('activate.user');



//Admin routes


Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function(){
     
    Route::resource('/users', 'UserController', ['except' => ['show', 'create', 'store']]);

});

Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function(){
     
    Route::resource('/registration', 'registrationController', ['except' => ['show', 'create', 'store']]);

});

Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function(){
     
    Route::resource('/group', 'groupController');

});

Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function(){
     
    Route::resource('/groupUser', 'groupUserController', ['except' => ['show']]);

});

Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function(){
     
    Route::resource('/announcement', 'announcementController', ['except' => ['show']]);

});


Route::namespace('Admin')->prefix('admin')->name('admin.')->group(function(){
     
    Route::resource('/notification', 'notificationController');

});





Route::get('/activation', 'HomeController@index')->name('activation');


Route::post('file/upload', 'FileController@upload')->name('file.upload');



Route::post('/upload', 'FileController@upload')->name('upload');

Route::get('push', 'FileController@push')->name('push');


Route::get('/docs/{filename}', 'FileController@getFile');


// Route::get('file','FileController@create');
// Route::post('file','FileController@store');




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


Route::get('video/{file}', 'FileController@video')->name('video');
Route::get('audio/{file}', 'FileController@audio')->name('audio');
Route::get('image/{file}', 'FileController@image')->name('image');
Route::get('text/{file}', 'FileController@text')->name('text');
Route::get('sheet/{file}', 'FileController@sheet')->name('sheet');
Route::get('pdf/{file}', 'FileController@pdf')->name('pdf');
Route::get('slide/{file}', 'FileController@slide')->name('slide');




// folders

Route::get('folder.index', 'FolderController@index')->name('folder.index');

Route::get('folder.create', 'FolderController@createfolder')->name('folder.create');

Route::post('create', 'FolderController@store')->name('create');

//folder files

Route::get('subhome/{folder}', 'FolderfilesController@files')->name('subhome');

Route::post('subfile/upload', 'FolderfilesController@store')->name('subfile.upload');



// Sub folders

Route::get('subFolderLoad/{folder}', 'SubfolderController@subFolderLoad')->name('subFolderLoad');

Route::get('subfolder.create', 'SubfolderController@createfolder')->name('subfolder.create');

Route::post('subfolderCreate', 'SubfolderController@store')->name('subfolderCreate');





Route::get('subvideo/{file}', 'SubfolderController@subvideo')->name('subvideo');
Route::get('subaudio/{file}', 'SubfolderController@subaudio')->name('subaudio');
Route::get('subimage/{file}', 'SubfolderController@subimage')->name('subimage');
Route::get('subtext/{file}', 'SubfolderController@subtext')->name('subtext');
Route::get('subsheet/{file}', 'SubfolderController@subsheet')->name('subsheet');
Route::get('subpdf/{file}', 'SubfolderController@subpdf')->name('subpdf');
Route::get('subslide/{file}', 'SubfolderController@subslide')->name('subslide');




// Return route 
Route::get('back', 'SubfolderController@back')->name('back');

//sharessssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss

Route::get('share/{file}', 'ShareController@share')->name('share');


Route::get('subshare/{file}', 'ShareController@subshare')->name('subshare');

Route::get('rrrrrr/{file}', 'ShareController@subshare')->name('rrrrrr');

Route::post('shareFile', 'ShareController@store')->name('shareFile');











// Notifications
Route::get('notificationindex', 'SubfolderController@back')->name('back');





Route::get('notificationview/{announcement}', 'Admin\notificationController@notificationview')->name('notificationview');
Route::post('notificationDelete/{notification}', 'Admin\notificationController@notificationDelete')->name('notificationDelete');

Route::get('filenotificationview/{filenotification}', 'FileNotificationController@filenotificationview')->name('filenotificationview');
Route::post('filenotificationview/{filenotification}', 'FileNotificationController@filenotificationDelete')->name('filenotificationDelete');











// DELETE 

Route::get('deleteFile/{file}', 'FileController@deleteFile')->name('deleteFile');

Route::get('deleteSubFile/{file}', 'SubfolderController@deleteSubFile')->name('deleteSubFile');





// FOLDER DELETE ?

Route::get('deleteFolder/{folder}', 'FolderController@deleteFolder')->name('deleteFolder');

Route::get('deleteSUbFolder/{folder}', 'SubfolderController@deleteSUbFolder')->name('deleteSUbFolder');






Route::get('inbox', 'ContactController@index')->name('inbox');


Route::get('inboxView/{contact}', 'ContactController@inboxView')->name('inboxView');
Route::Post('inboxDelete/{contact}', 'ContactController@destroy')->name('inboxDelete');

Route::post('inboxTreated/{contact}', 'ContactController@inboxTreated')->name('inboxTreated');
Route::post('inboxProgress/{contact}', 'ContactController@inboxProgress')->name('inboxProgress');
Route::post('inboxNotTreated/{contact}', 'ContactController@inboxNotTreated')->name('inboxNotTreated');













// Route::get('search','HomeController@result');
Route::get('/live_search/action', 'HomeController@action')->name('live_search.action');



//quota
Route::get('quota', 'QuotaController@index')->name('quota');



// comments

Route::get('comment.index', 'CommentController@index')->name('comment.index');

Route::post('comment.create', 'CommentController@store')->name('comment.create');

Route::get('comment.closeComment/{comment}', 'CommentController@closeComment')->name('comment.closeComment');


Route::get('comment.printComment/{file}', 'CommentController@printComment')->name('comment.printComment');


// Replies

Route::get('reply.index', 'ReplyController@index')->name('reply.index');

Route::post('reply.create', 'ReplyController@store')->name('reply.create');

Route::get('reply.closeReply/{reply}', 'ReplyController@closeReply')->name('reply.closeReply');


//Request signature
Route::post('requestSignature', 'FileNotificationController@requestSignature')->name('requestSignature');
Route::post('requestStamp', 'FileNotificationController@requestStamp')->name('requestStamp');
Route::post('requestSignatureStamp', 'FileNotificationController@requestSignatureStamp')->name('requestSignatureStamp');

Route::post('requestSheetSignatureStamp', 'FileNotificationController@requestSheetSignatureStamp')->name('requestSheetSignatureStamp');

Route::post('requestAuthImage', 'FileNotificationController@requestAuthImage')->name('requestAuthImage');


// Sign and stamp docs

Route::get('sign/{file}', 'SignStampController@sign')->name('sign');
Route::get('stamp/{file}', 'SignStampController@stamp')->name('stamp');
Route::get('signstamp/{file}', 'SignStampController@signstamp')->name('signstamp');

Route::post('confirmSS', 'SignStampController@store')->name('confirmSS');
Route::get('downloadSigned/{id}', 'SignStampController@downloadSigned')->name('downloadSigned');
Route::get('downloadStamped/{id}', 'SignStampController@downloadStamped')->name('downloadStamped');
Route::get('downloadsignstamp/{id}', 'SignStampController@downloadsignstamp')->name('downloadsignstamp');

// Sign and stamp sheets

Route::get('signSheet/{file}', 'SignStampController@signSheet')->name('signSheet');
Route::get('stampSheet/{file}', 'SignStampController@stampSheet')->name('stampSheet');
Route::get('signstampSheet/{file}', 'SignStampController@signstampSheet')->name('signstampSheet');

Route::get('authImage/{file}', 'SignStampController@authImage')->name('authImage');


// Download signed file
Route::get('downloadSignedSheet/{id}', 'SignStampController@downloadSignedSheet')->name('downloadSignedSheet');
Route::get('downloadStampedSheet/{id}', 'SignStampController@downloadStampedSheet')->name('downloadStampedSheet');
Route::get('downloadsignstampSheet/{id}', 'SignStampController@downloadsignstampSheet')->name('downloadsignstampSheet');

Route::get('downloadAuthImage/{id}', 'SignStampController@downloadAuthImage')->name('downloadAuthImage');

