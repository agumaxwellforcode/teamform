<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFileNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('url');
            $table->string('owner');
            $table->string('actioneer');
            $table->string('action');
            $table->string('filename');
            $table->string('seen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_notifications');
    }
}
