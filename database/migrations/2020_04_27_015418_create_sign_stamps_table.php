<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSignStampsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sign_stamps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('file');
            $table->string('filename');
            $table->string('file_id');
            $table->string('owner');
            $table->string('action');
            $table->string('actioneer');
            $table->string('signature');
            $table->string('stamp');
            $table->string('code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sign_stamps');
    }
}
