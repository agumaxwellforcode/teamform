@extends('layouts.app')

@section('content')

    <div class="container-fluid">

                     
          
                        <div class="col-md-12 m-0 p-0">
                          @if ($message = Session::get('success'))
              
                          <div class="alert alert-success alert-block col-sm-12">
          
                              <button type="button" class="close" data-dismiss="alert">×</button>
          
                              <strong>{{ $message }}</strong>
          
                          </div>
          
                      @endif
                      </div>
                        <br>
                        <!-- Nav tabs -->
                        <div class="row justify-content-center">
                        <ul class="nav nav-tabs " role="tablist">
                          <li class="nav-item ">
                            <a class="nav-link active " data-toggle="tab" href="#home">Profile Information</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#menu1"> &nbsp;&nbsp;&nbsp; Update Profile &nbsp;&nbsp;&nbsp;</a>
                          </li>
                          @can('manage-users')
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#signature"> &nbsp;&nbsp;&nbsp; Update Signature &nbsp;&nbsp;&nbsp;</a>
                          </li>
                          @endcan
                         
                        </ul>
                        </div>
                      
                        <!-- Tab panes -->
                        <div class="tab-content">
                          <div id="home" class="container tab-pane active">
                            {{-- <br><br> --}}
                            <h3 class=" mb-5 mt-5 text-center">Details</h3>
                            
                            <div class="row justify-content-center">

                              <div class="profile-header-container">
                                  <div class="profile-header-img">
                                      <img class="rounded-circle" height="200px" width="200px" src="/storage/avatars/{{ $user->avatar }}" />
                                      <!-- badge -->
                                      <div class="rank-label-container text-center">
                                          
                                          <span class="label label-default rank-label font-weight-bolder">{{$user->firstname}} {{$user->lastname}}</span>
                                      </div>
                                  </div>
                              </div>
                  
                          </div>
                          <br>
                          <div class="row justify-content-center">
                            <div class="table-responsive col-sm-12 col-md-6 col-lg-4 col-xl-4">
                              <table class="table table-hover border-0">
                               
                                  <tbody>
                                    <tr class="border-0">
                                      <td class="border-0" scope="row"> Firstname</td>
                                      <td  class="text-right border-0">{{$user->firstname}}</td>
                                      
                                    </tr>
                                    <tr>
                                      <td class="border-0" scope="row"> Lastname</td>
                                      <td class="text-right border-0"> {{$user->lastname}}</td>
                                      
                                    </tr>
                                    <tr>
                                      <td class="border-0" scope="row"> Email</td>
                                      <td class="text-right border-0"> {{$user->email}}</td>
                                      
                                    </tr>
                                    <tr>
                                      <td class="border-0" scope="row"> Username</td>
                                      <td class="text-right border-0"> {{$user->username}}</td>
                                      
                                    </tr>
                                   
                                  </tbody>
                              </table>
                              
                              
    
                            </div>
                          </div>
                        
                          
                          </div>










                          <div id="menu1" class="container tab-pane fade"><br>
                            <h3 class=" mb-4 mt-4 text-center">Edit</h3>

                            <div class="row justify-content-center">

                              <div class="profile-header-container">
                                  <div class="profile-header-img">
                                      <img class="rounded-circle" height="200px" width="200px" src="/storage/avatars/{{ $user->avatar }}" />
                                      <!-- badge -->
                                      {{-- <div class="rank-label-container text-center">
                                          
                                          <span class="label label-default rank-label font-weight-bolder">{{$user->firstname}} {{$user->lastname}}</span>
                                      </div> --}}
                                  </div>
                              </div>
                    
                          </div>
                    
                          
                            <div class="row justify-content-center  mt-1">
                                   
                        
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                            <div class="row justify-content-center mt-1">
                                <form action="/profile" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                            
                                        <div class="row justify-content-center">
                                           
                                            <div class="col-sm-4 ">
                                                <div class="row justify-content-center">
                                                <label class="btn btn-warning btn-file">
                                                    Browse image <input type="file" class="form-control-file" name="avatar" id="avatarFile" value="{{ $user->avatar }}" aria-describedby="fileHelp" style="display: none;">
                                                </label>
                                              </div>
                                        </div>
                                          
                                            <small id="fileHelp" class="form-text text-muted text-center col-sm-12">Please upload a valid image file. Size not more than 2MB.</small> <hr>
                                        </div>
                                               
                    
                                                <div class="form-group mb-3{{ $errors->has('firstname') ? ' has-error' : '' }}">
                                                    {{-- <label for="name" class="col-md-4 control-label">Name</label> --}}
                                
                                                    <div class="col-md-12">
                                                      <input id="firstname" type="text" class="form-control" name="firstname"  required
                                                            autofocus value="{{$user->firstname}}">
                                
                                                      @if ($errors->has('firstname'))
                                                        <span class="help-block text-dark">
                                                                            <strong>{{ $errors->first('firstname') }}</strong>
                                                                        </span>
                                                      @endif
                                                    </div>
                                                  </div>
                    
                                                  <div class="form-group mb-3{{ $errors->has('lastname') ? ' has-error' : '' }}">
                                                    {{-- <label for="name" class="col-md-4 control-label">Name</label> --}}
                                
                                                    <div class="col-md-12">
                                                      <input id="lastname" type="text" class="form-control" name="lastname" required
                                                        value="{{$user->lastname}}"   >
                                
                                                      @if ($errors->has('lastname'))
                                                        <span class="help-block text-danger">
                                                                            <strong>{{ $errors->first('lastname') }}</strong>
                                                                        </span>
                                                      @endif
                                                    </div>
                                                  </div>
                              
                                                  <div class="form-group mb-3{{ $errors->has('email') ? ' has-error' : '' }}" >
                                                      {{-- <label for="email" class="col-md-4 control-label">E-Mail Address</label> --}}
                                  
                                                      <div class="col-md-12">
                                                        <input id="email" type="email" class="form-control" name="email"  required value="{{$user->email}}" readonly>
                                  
                                                        @if ($errors->has('email'))
                                                          <span class="help-block text-danger">
                                                                              <strong>{{ $errors->first('email') }}</strong>
                                                                          </span>
                                                        @endif
                                                      </div>
                                                      <small id="fileHelp" class="form-text text-danger text-center col-sm-12">Your email cannot be edited from here, Please send a request to admin.</small> 

                                                    </div>
                                  
                              
                                                <div class="form-group mb-3{{ $errors->has('username') ? ' has-error' : '' }}">
                                                  {{-- <label for="name" class="col-md-4 control-label">Username</label> --}}
                              
                                                  <div class="col-md-12">
                                                    <input id="name" type="text" class="form-control " name="username"  required
                                                      value="{{$user->username}}" >
                              
                                                    @if ($errors->has('username'))
                                                      <span class="help-block text-danger">
                                                                          <strong>{{ $errors->first('username') }}</strong>
                                                                      </span>
                                                    @endif
                                                  </div>
                                                </div>
                    
                                                <div class="form-group mb-3{{ $errors->has('password') ? ' has-error' : '' }}">
                                                    {{-- <label for="password" class="col-md-4 control-label">Password</label> --}}
                                
                                                    <div class="col-md-12">
                                                      <input id="password" type="text" class="form-control border-right-0" name="password" required value="{{$user->password}}" data-toggle="password" readonly>
                                                      {{-- <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span> --}}
                                                      @if ($errors->has('password'))
                                                        <span class="help-block text-danger">
                                                                            <strong>{{ $errors->first('password') }}</strong>
                                                                        </span>
                                                      @endif
                                                    </div>
                                                    <small id="fileHelp" class="form-text text-danger text-center col-sm-12">Your password cannot be edited from here, Please send a request to admin.</small> 

                                                  </div>
                    
                                                  
                                                  <div class="form-group ">
                                                    <div class="col-sm-12">
                                                      <button type="submit" class="btn btn-primary btn-block">
                                                        Update
                                                      </button>
                                                    </div>
                                                  </div>
                                        
                                    
                                </div>
                                </div>
                                  
                                </form>
                            </div>


                            <div id="signature" class="container tab-pane ">
                              {{-- <br><br> --}}
                              <h3 class=" mb-5 mt-5 text-center">Signature</h3>
                              
                              <div class="row justify-content-center">
  
                                <div class="profile-header-container p-3 border border-light shadow">
                                    <div class="profile-header-img">
                                        @if (!$user->signature)
                                          <img class="rounded" height="200px" width="300px" src="/storage/signatures/signature-logo-black-500x202.PNG" />
                                        @else
                                        <img class="rounded" height="200px" width="300px" src="/storage/signatures/{{ $user->signature }}" />
                                        @endif
                                        
                                        
                                    </div>
                                </div>
                    
                            </div>
                            <br>
                                <form action="{{route('signature')}}" method="post" enctype="multipart/form-data">
                                  @csrf
                                  <div class="row justify-content-center">
                                  
                                    <div class="col-sm-3 p-2">
                                        <div class="row justify-content-center">
                                          <div class="col-sm-6 ">
                                              <label class="btn btn-warning btn-file">
                                                @if (!$user->signature)
                                                <i class="fa fa-folder-open" aria-hidden="true"></i> Browse <input type="file" class="form-control-file" name="signature" id="signature" value="/storage/signatures/signature-logo-black-500x202.PNG" aria-describedby="fileHelp" style="display: none;">
                                                @else
                                                <i class="fa fa-folder-open" aria-hidden="true"></i> Browse <input type="file" class="form-control-file" name="signature" id="signature" value="{{ $user->signature }}" aria-describedby="fileHelp" style="display: none;">
                                                @endif
                                              </label>
                                          </div>
                                          <div class="col-sm-6">
                                            <div class="form-group text-right">
                                              <button type="submit" class="btn btn-primary ">
                                               <i class="fa fa-upload" aria-hidden="true"></i> Update
                                              </button>
                                            </div>
                                          </div>
                                        </div>
                                

                                
                              </div>
                                <small id="fileHelp" class="form-text text-muted text-center col-sm-12">Please upload a valid image file. Size not more than 2MB.</small> <hr>
                            </div>
                              </form>
                            
                          
                            
                            </div>




                            <a onclick="goBack()" id="back" class="btn btn-primary " title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                          </div>











                        </div>
                      
        
                       



       
    </div>


    
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  
  <script>
    function goBack() {
      window.history.back();
    }
    </script>
    <style>
      .nav-tabs{
          background-color:  #00968859 !important;
      }
      .nav-tabs .nav-item .nav-link{
          font-weight: bold;
          color:  #2d2d2d !important;
          border-radius: 0px !important;
          border-bottom: 3px solid transparent;
      }
      .nav-tabs .nav-item .active{
          background-color:  #009688 !important;
          color: #fff !important;
          border-radius: 0px !important;
          
      }
      .nav-tabs .nav-link.active,
      .nav-tabs .show > .nav-link {
          color: #fff !important;
          background-color: #009688;
          border-bottom: 3px solid #00cab6 !important;
      }
      #add .dropdown-item:hover{
        color: #009688 !important;
      }
    </style>
@endsection
