@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-8 col-lg-9 shadow-sm">
         
                <div class="card-header bg-white font-weight-bolder">DOC VIEWER
                    
                        <div class="float-right">
                               
                                

                                <div class="btn-group">
                                    <button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      <i class="fa fa-fingerprint" aria-hidden="true"></i>  
                                      {{-- Authorize Document --}}
                                    </button>
                                    <div class="dropdown-menu shadow-lg border-0">
                                        <a class="dropdown-item" href="#"> <button data-toggle="modal" data-target="#requestSignature" class=" pl-0 btn btn-default btn-sm font-weight-bold"> <i class="fa fa-file-signature" aria-hidden="true"></i> Request signature</button></a>
                                        <a class="dropdown-item" href="#"> <button data-toggle="modal" data-target="#requestStamp" class="pl-0 btn btn-default btn-sm  font-weight-bold"> <i class="fa fa-stamp" aria-hidden="true"></i> Request stamp</button></a>
                                        <a class="dropdown-item" href="#"> <button data-toggle="modal" data-target="#requestSignatureStamp" class="pl-0 btn btn-default btn-sm font-weight-bold text-warning"> <i class="fa fa-stamp" aria-hidden="true"></i> Request Signature & Stamp</button></a>
                                        <div class="dropdown-divider"></div>
                                        <div class="dropdown-divider"></div>
                                            @can('manage-users')
                                            <a  href="{{route('comment.printComment',$file)}}" class="dropdown-item font-weight-bold "> <i class="fa fa-comment-alt" aria-hidden="true"></i> Comments </a>                                       
                                              <a  href="{{route('sign',$file)}}" class=" dropdown-item  font-weight-bold"> <i class="fa fa-file-signature" aria-hidden="true"></i> Sign this file </a>
                                              <a  href="{{route('stamp',$file)}}" class=" dropdown-item  font-weight-bold"> <i class="fa fa-stamp" aria-hidden="true"></i> Stamp this file</a>
                                              <a  href="{{route('signstamp',$file)}}" class=" dropdown-item  font-weight-bold"> <i class="fa fa-file-signature" aria-hidden="true"></i> Sign & <i class="fa fa-stamp" aria-hidden="true"></i> Stamp this file</a>
                                            @endcan
                                    </div>
                                  </div>




                                
                               
                                
                                
                                <a  href="#" class="btn btn-default text-white btn-sm pl-5 pr-5"></a>
                                <a  href="{{route('home')}}" class="btn btn-default text-right btn-sm font-weight-bold"> </i> <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</a>
                            </div>  

                </div> 
                  <div class="row justify-content-center"> 
                        <div class="col-md-12 pl-3 pr-3">

                        
                            <object data="{{url($response)}}" width="100%" style="height:100vh">
                                <p class="text-center"> Sorry! your browser is not supported to view this file, We recommend you use google chrome</p>
                                </object> 

                                {{-- <iframe src='{{url($response)}}' width='100%' height='1030' frameborder='0'> </iframe> --}}
                            
                        </div>
                    </div>
                </div>

                

                <div class="col-sm-12 col-md-4 col-lg-3 p-3 bg-white">
                    @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                        @php  
                                            Alert::error('No Reciepient', 'Please select a reciepient to sign/stamp the document!')->width('320px');
                                        @endphp
                            @endforeach
                    </div>
                @endif
                    <div class="collapse" id="collapseExample">
                        <div class="card mb-3 shadow" style="width: 18rem; border-radius:8px;">
                            <div class="card-body">
                                <img class="rounded-circle float-right" height="30px" width="30px" src="/storage/avatars/{{ Auth::user()->avatar }}" />
                              <h5 class="card-title font-weight-bold">{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</h5>
                              <h6 class="card-subtitle  text-muted small">{{ Auth::user()->updated_at }}</h6>

                            <form action="{{route('comment.create')}}" method="post">
                                @csrf
                                    {{ method_field('POST') }}
                                  <div class="form-group">
                                    <label for=""></label>
                                    <textarea id="txtarea" class="form-control border-1 border-primary" name="comment" id="" rows="1" required></textarea>
                                  </div>
                              <input type="hidden" name="file_id" value="{{$file->id}}">
                              <input type="hidden" name="filename" value="{{$file->filename}}">
                              <input type="hidden" name="folder_id" value="{{$file->folder_id}}">
                                  @if ($file->folder_id == "0")
                                    <input type="hidden" name="path" value="files/{{$file->filename}}">
                                  @else
                                    @php
                                        $path=DB::table('folders') ->where('id','=',$file->folder_id) ->first();
                                    @endphp
                                        <input type="hidden" name="path" value="{{$path->path}}/{{$file->filename}}">
                                  @endif
                                     
                              <input type="hidden" name="owner_id" value="{{$file->user_id}}">
                              <input type="hidden" name="commenter_id" value="{{ Auth::user()->id }}">
                              <input type="hidden" name="url" value="{{ url()->current() }}">

                              <button type="submit" class="btn btn-primary font-weight-bold">Comment</button> 
                              <a  class="card-link text-success font-weight-bold float-right pt-2" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" >
                                </i> <i class="fa fa-times" aria-hidden="true"></i> Cancel
                            </a>
                            </form>

                            </div>
                          </div>
                      </div>
                    @php
                        $comments=DB::table('comments') ->where('file_id','=',$file->id)->where('status','=','Open')->orderBy('id','desc')->get();
                    @endphp
                    @if (count($comments) > 0 )
                        @foreach ($comments as $comment)
                            <div class="card mb-3 shadow" style="width: 18rem; border-radius:8px;">
                                <div class="card-body">
                                    @php
                                        $commenter=DB::table('users') ->where('id','=',$comment->commenter_id) ->first();
                                    @endphp
                                    <img class="rounded-circle float-right border-2 border-primary" height="30px" width="30px" src="/storage/avatars/{{ $commenter->avatar }}" />
                                <h6 class="card-title font-weight-bold">{{ $commenter->firstname }} {{ $commenter->lastname }}</h6>
                                <h6 class="card-subtitle mb-2 text-muted small">{{ $comment->created_at }}</h6>
                                <p class="card-text font-weight-bold text-dark">{{$comment->comment}}.</p>






















                                @php
                                    $replies=DB::table('replies') ->where('comment_id','=',$comment->id)->orderBy('id','asc')->get();
                                @endphp
                                @if (count($replies) > 0 )
                                    @foreach ($replies as $reply)
                                        <div class="card mb-3 shadow" style="width: 18rem; border-radius:8px;">
                                            <div class="card-body">
                                                @php
                                                    $replyer = DB::table('users') ->where('id','=',$reply->replyer_id)->first();
                                                    //  dd($replyer->avatar)
                                                @endphp
                                                <img class="rounded-circle float-right border-2 border-primary" height="30px" width="30px" src="/storage/avatars/{{ $replyer->avatar }}" />
                                            <h6 class="card-title font-weight-bold">{{ $replyer->firstname }} {{ $replyer->lastname }}</h6>
                                            <p class="card-text small font-weight-bold text-dark">{{$reply->reply}}.</p>
            
            
            
                                            {{-- <a href="{{route('comment.closeComment',$comment->id)}}" class="card-link text-success small font-weight-bold"><i class="fa fa-times" aria-hidden="true"></i> Close</a> --}}
                                           
                                            </div>
                                        </div>
                                    @endforeach
                                @endif






















                                
                                <div class="collapse mb-2" id="collapseReply{{$comment->id}}">
                                    <hr>
                                    <div class="card card-body border-0 p-0">
                                        <form action="{{route('reply.create')}}" method="post">
                                            @csrf
                                                {{ method_field('POST') }}
                                          <div class="form-group">
                                            <textarea id="txtarea" class="form-control border-1 border-primary" name="reply" id="" rows="1" required></textarea>
                                          </div>
                                          <input type="hidden" name="file_id" value="{{$file->id}}">
                                          <input type="hidden" name="comment_id" value="{{$comment->id}}">
                                          <input type="hidden" name="replyer_id" value="{{ Auth::user()->id }}">
                                          <input type="hidden" name="url" value="{{ url()->current() }}">
                                          
                                          <input type="hidden" name="filename" value="{{$file->filename}}">
                                          <input type="hidden" name="owner" value="{{$file->user_id}}">
            
                                          <button type="submit" class="btn btn-primary btn-sm font-weight-bold">Reply This comment</button> 
                                          <a  class="card-link text-success font-weight-bold small float-right pt-2" type="button" data-toggle="collapse" data-target="#collapseReply{{$comment->id}}" aria-expanded="false" aria-controls="collapseExample" >
                                             <i class="fa fa-times" aria-hidden="true"></i> Cancel
                                        </a>
                                        </form>
            
                                    </div>
                                  </div>





                                {{-- <a href="{{route('comment.closeComment',$comment->id)}}" class="card-link text-success small font-weight-bold dis"><i class="fa fa-times" aria-hidden="true"></i> Close</a> --}}
                                <a href="#collapseReply{{$comment->id}}" class="card-link text-success small font-weight-bold" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample">Reply comment</a>
                                </div>
                            </div>
                        @endforeach
                    @endif
                    
                        <button id="myBtn1" class="btn btn-primary shadow-lg " type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" data-toggle="tooltip" data-placement="bottom" title="Add Comment">
                            </i> <i class="fa fa-comment-alt" aria-hidden="true"></i>
                        </button>
                      
                     
                </div>
          
        </div>
    </div>
  
  
                <!--Signature Modal -->
                <div class="modal fade bd-example-modal-lg" id="requestSignature" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title text-success h6 font-weight-bold" id="exampleModalCenterTitle"> <i class="fa fa-file-signature" aria-hidden="true"></i> Request a signature from</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('requestSignature')}}" method="post">
                                @csrf
                                {{ method_field('POST') }}
                              <table id="example" class="table table-borderless table-hover">
                                   
                                  <thead>
                                      <tr>
                                          <th></th>
                                          <th></th>
                                          <th></th>
                                      </tr>
                                  </thead>
                                  <tbody >
                                      @php
                                         $sender=Auth::user();
                                         $users = DB::table('users')->get();
                                        
                                      @endphp
                                      @foreach ($users as $user )
                                          <tr >
                                            <td class="text-primary justify-content-center"><img class="rounded-circle" height="50px" width="50px" src="/storage/avatars/{{ $user->avatar }}" /> </td>
                                            <td class="text-primary align-middle justify-content-center"> {{ $user->firstname }}   {{ $user->lastname }}</td>
                                            <td class="text-right align-end justify-content-end">
                                               
                                                    <div class="custom-control custom-switch  align-middle mt-3">
                                                        <input type="checkbox" name="reciepient_id[]" class="custom-control-input " id="{{$user->id}}" value="{{$user->id}}">                
                                                        <label class="custom-control-label" for="{{$user->id}}"></label>
                                                    </div>

                                                    <input type="hidden" name="sender" value="{{$sender->id}}">                             
                                                    <input type="hidden" name="filename" value="{{$file->filename}}">  
                                                    <input type="hidden" name="url" value="{{ url()->current() }}">                          
                                                   
                                              </td>
                                           
                                          </tr>
                                        
                                        @endforeach
                                  </tbody>
                              </table>
                        {{-- </div> --}}
                        <div class="modal-footer">
                        <button type="button" class="btn btn-default text-success font-weight-bold" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                        <button type="submit" class="btn btn-primary">Send request</button>
                        </div>
                            </form>
                    </div>
                    </div>
                </div>
            </div>



                <!--Sign and Stamp Modal -->
                <div class="modal fade bd-example-modal-lg" id="requestSignatureStamp" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title text-success h6 font-weight-bold" id="exampleModalCenterTitle"> <i class="fa fa-stamp" aria-hidden="true"></i> Request a signature and stamp from</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('requestSignatureStamp')}}" method="post">
                                @csrf
                                {{ method_field('POST') }}
                            {{-- <div class=" table-responsive"> --}}
                            <table id="example2" class="table table-borderless table-hover">
                                
                                <thead>
                                    <tr>
                                        
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        
                                        
                                    </tr>
                                </thead>
                                <tbody >
                                    @php
                                        $sender=Auth::user();
                                        $users = DB::table('users')->get();
                                        
                                    @endphp
                                    @foreach ($users as $user )

                                    @php
                                    @endphp

                                        <tr >
                                            <td class="text-primary justify-content-center"><img class="rounded-circle" height="50px" width="50px" src="/storage/avatars/{{ $user->avatar }}" /> </td>
                                            <td class="text-primary align-middle justify-content-center"> {{ $user->firstname }}   {{ $user->lastname }}</td>
                                            <td class="text-right align-end justify-content-end">
                                            
                                            

                                                    <div class="custom-control custom-switch  align-middle mt-3">
                                                        <input type="checkbox" name="reciepient_id[]" class="custom-control-input " id="{{$user->id}}3" value="{{$user->id}}">                
                                                        <label class="custom-control-label" for="{{$user->id}}3"></label>
                                                    </div>


                                                    <input type="hidden" name="sender" value="{{$sender->id}}">                             
                                                    <input type="hidden" name="filename" value="{{$file->filename}}">  
                                                    <input type="hidden" name="url" value="{{ url()->current() }}">                          
                                                    
                                                
                                                
                                            </td>
                                        
                                        </tr>
                                        
                                        @endforeach
                                </tbody>
                            </table>
                        {{-- </div> --}}
                        <div class="modal-footer">
                        <button type="button" class="btn btn-default text-success font-weight-bold" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                        <button type="submit" class="btn btn-primary">Send request</button>
                        </div>
                            </form>
                    </div>
                    </div>
                    </div>
                </div>

                <!--Stamp Modal -->
                <div class="modal fade" id="requestStamp" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title text-success h6 font-weight-bold" id="exampleModalCenterTitle"> <i class="fa fa-stamp" aria-hidden="true"></i> Request a stamp from</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('requestStamp')}}" method="post">
                                @csrf
                                {{ method_field('POST') }}
                            {{-- <div class=" table-responsive"> --}}
                            <table id="example3" class="table table-borderless table-hover">
                                
                                <thead>
                                    <tr>
                                        
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        
                                        
                                    </tr>
                                </thead>
                                <tbody >
                                    @php
                                        $sender=Auth::user();
                                        $users = DB::table('users')->get();
                                        
                                    @endphp
                                    @foreach ($users as $user )

                                    @php
                                    @endphp

                                        <tr >
                                            <td class="text-primary justify-content-center"><img class="rounded-circle" height="50px" width="50px" src="/storage/avatars/{{ $user->avatar }}" /> </td>
                                            <td class="text-primary align-middle justify-content-center"> {{ $user->firstname }}   {{ $user->lastname }}</td>
                                            <td class="text-right align-end justify-content-end">
                                            
                                            

                                                    <div class="custom-control custom-switch  align-middle mt-3">
                                                        <input type="checkbox" name="reciepient_id[]" class="custom-control-input " id="{{$user->id}}2" value="{{$user->id}}">                
                                                        <label class="custom-control-label" for="{{$user->id}}2"></label>
                                                    </div>


                                                    <input type="hidden" name="sender" value="{{$sender->id}}">                             
                                                    <input type="hidden" name="filename" value="{{$file->filename}}">  
                                                    <input type="hidden" name="url" value="{{ url()->current() }}">                          
                                                    
                                                
                                                
                                            </td>
                                        
                                        </tr>
                                        
                                        @endforeach
                                </tbody>
                            </table>
                        {{-- </div> --}}
                        <div class="modal-footer">
                        <button type="button" class="btn btn-default text-success font-weight-bold" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                        <button type="submit" class="btn btn-primary">Send request</button>
                        </div>
                            </form>
                    </div>
                    </div>
                </div>
                </div>



<script>
    $(document).ready(function() {
        $('#example').DataTable( {
            "paging":   false,
            "ordering": false,
            "info":     false
        } );
    } );
    $(document).ready(function() {
        $('#example2').DataTable( {
            "paging":   false,
            "ordering": false,
            "info":     false
        } );
    } );
    $(document).ready(function() {
        $('#example3').DataTable( {
            "paging":   false,
            "ordering": false,
            "info":     false
        } );
    } );
    function goBack() {
      window.history.back();
    };
    </script>
    <script>
        function expandTextarea(id) {
            document.getElementById(id).addEventListener('keyup', function() {
                this.style.overflow = 'hidden';
                this.style.height = 0;
                this.style.height = this.scrollHeight + 'px';
            }, false);
        }

        expandTextarea('txtarea');
    </script>
    <style>
        input{
            border:1px solid #00968859 !important;
            padding: 20px !important;
        }
        select{
            border:1px solid #00968859 !important;
            padding: 10px !important;
            min-height: 45px !important;
            min-width: 100px !important;
            color: black !important;
        }
         #example_filter, #example2_filter, #example3_filter > label > input{
            /* background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIgICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgICB2ZXJzaW9uPSIxLjEiICAgaWQ9InN2ZzQ0ODUiICAgdmlld0JveD0iMCAwIDIxLjk5OTk5OSAyMS45OTk5OTkiICAgaGVpZ2h0PSIyMiIgICB3aWR0aD0iMjIiPiAgPGRlZnMgICAgIGlkPSJkZWZzNDQ4NyIgLz4gIDxtZXRhZGF0YSAgICAgaWQ9Im1ldGFkYXRhNDQ5MCI+ICAgIDxyZGY6UkRGPiAgICAgIDxjYzpXb3JrICAgICAgICAgcmRmOmFib3V0PSIiPiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+ICAgICAgICA8ZGM6dHlwZSAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4gICAgICAgIDxkYzp0aXRsZT48L2RjOnRpdGxlPiAgICAgIDwvY2M6V29yaz4gICAgPC9yZGY6UkRGPiAgPC9tZXRhZGF0YT4gIDxnICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDMwLjM2MjIpIiAgICAgaWQ9ImxheWVyMSI+ICAgIDxnICAgICAgIHN0eWxlPSJvcGFjaXR5OjAuNSIgICAgICAgaWQ9ImcxNyIgICAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNjAuNCw4NjYuMjQxMzQpIj4gICAgICA8cGF0aCAgICAgICAgIGlkPSJwYXRoMTkiICAgICAgICAgZD0ibSAtNTAuNSwxNzkuMSBjIC0yLjcsMCAtNC45LC0yLjIgLTQuOSwtNC45IDAsLTIuNyAyLjIsLTQuOSA0LjksLTQuOSAyLjcsMCA0LjksMi4yIDQuOSw0LjkgMCwyLjcgLTIuMiw0LjkgLTQuOSw0LjkgeiBtIDAsLTguOCBjIC0yLjIsMCAtMy45LDEuNyAtMy45LDMuOSAwLDIuMiAxLjcsMy45IDMuOSwzLjkgMi4yLDAgMy45LC0xLjcgMy45LC0zLjkgMCwtMi4yIC0xLjcsLTMuOSAtMy45LC0zLjkgeiIgICAgICAgICBjbGFzcz0ic3Q0IiAvPiAgICAgIDxyZWN0ICAgICAgICAgaWQ9InJlY3QyMSIgICAgICAgICBoZWlnaHQ9IjUiICAgICAgICAgd2lkdGg9IjAuODk5OTk5OTgiICAgICAgICAgY2xhc3M9InN0NCIgICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjY5NjQsLTAuNzE3NiwwLjcxNzYsMC42OTY0LC0xNDIuMzkzOCwyMS41MDE1KSIgICAgICAgICB5PSIxNzYuNjAwMDEiICAgICAgICAgeD0iLTQ2LjIwMDAwMSIgLz4gICAgPC9nPiAgPC9nPjwvc3ZnPg==); */
            background-image: url(/images/icons/search.svg) !important;
            background-repeat: no-repeat !important;
            background-color: #fff !important;
            background-size: 18px;
            background-position: 300px 11px !important;
            font-weight: bold !important;
        }
        #example_filter, #example2_filter, #example3_filter > label{
            color: #fff !important;
        }

        #example tr:hover{
            background-color: #00968866 !important;
            color: white !important;
            border-radius: 4px !important;
            font-weight: bold;
        }
        #example2 tr:hover{
            background-color: #00968866 !important;
            color: white !important;
            border-radius: 4px !important;
            font-weight: bold;
        }
        #example3 tr:hover{
            background-color: #00968866 !important;
            color: white !important;
            border-radius: 4px !important;
            font-weight: bold;
        }
    .custom-control-input:checked ~ .custom-control-label::before {
        color: #fff;
        background-color: #009688 !important;
    }

        
    </style>

@endsection
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>