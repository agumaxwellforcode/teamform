@extends('layouts.app')

@section('content')

<div class="container-fluid mt-5">
    <div class="row justify-content-center align-content-center mt-5">
        <div class="col-md-4">
            <br><br>
            <div class="card mt-5 shadow-lg border-0">
                <div class="card border-0" >
                    <div class="m-5  ">
                      @if ($signature->action == "Signed a documented that you requested")
                        <div class="row justify-content-lg-center">
                          <div class="col-sm-6">
                            <img class="card-img-top img-fluid" src="{{$signature->signature}}" alt="Signature">
                          </div>
                        </div>
                      @elseif($signature->action == "Stamped a documented that you requested")
                        <div class="row justify-content-lg-center">
                          <div class="col-sm-6">
                            <img class="card-img-top img-fluid" src="{{$signature->stamp}}" alt="Stamp">
                          </div>
                        </div>
                      @else
                          <div class="row justify-content-lg-center">
                              <div class="col-sm-6">
                                <img class="card-img-top img-fluid" src="{{$signature->signature}}" alt="Signature">
                              </div>
                              <div class="col-sm-6">
                                <img class="card-img-top img-fluid" src="{{$signature->stamp}}" alt="Stamp">
                              </div>
                          </div>
                      @endif
                        
                    </div>
                    
                    <div class="card-body">
                      <h5 class="card-title">The Authorization is valid for the transaction below;</h5>
                      {{-- <p class="card-text">.</p> --}}
                    </div>
                    @php
                       $signer = DB::table('users')->where('id', '=', $signature->actioneer)->first();
                       $owner = DB::table('users')->where('id', '=', $signature->actioneer)->first();
                    @endphp
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item"> <b> File : </b> {{$signature->filename}}</li>
                      <li class="list-group-item"><b>Owner:</b> {{$owner->firstname}} {{$owner->lastname}}</li>
                      <li class="list-group-item"><b>Authorized by:</b> {{$signer->firstname}} {{$signer->lastname}}</li>
                      <li class="list-group-item"><b>Date:</b> {{$signature->created_at}}</li>
                    </ul>
                    <div class="card-body text-center">
                    <a href="{{route('home')}}" class="card-link btn btn-success"> <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</a>
                      {{-- <a href="#" class="card-link float-right btn btn-success">Another link</a> --}}
                    </div>
                  </div>
  </div>
  </div>
</div>

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  {{-- <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script> --}}

  
  
  
  
  