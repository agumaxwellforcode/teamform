@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">


            @if ($message = Session::get('success'))
                            
            <div class="alert alert-success alert-block">

                <button type="button" class="close" data-dismiss="alert">×</button>

                <strong>{{ $message }}</strong>

            </div>

       @endif



        <div class="col-md-8 ">
            @if(session()->get('message'))
            <div class="alert alert-success">
              {{ session()->get('message') }}
            </div>
            @endif
            <div class="card shadow-lg">
                <div class="card-header text-primary font-weight-bolder">File Upload
                    <div class="float-right">
                        <a href="{{ route('home') }}" class="btn btn-default text-danger"> <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</a>
                        <a href="{{ route('home') }}" class="btn btn-default text-primary"> <i class="fa fa-home" aria-hidden="true"></i> Home</a>
                    </div>   
                </div>
                <div class="card-body p-0">

                        <form action="{{ route('file.upload') }}" class="dropzone bg-white border-1 border-light text-primary justify-content-center mb-0" style="height:250px;" id="myDropzone" method="POST" name="myDropzone" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row justify-content-center mb-5">
                               
                                <input type="hidden" id="permision" name="permision" value="Private">
                                {{-- <select class="form-control col-sm-8 col-md-2 col-lg-2  text-danger border-1" id="permision" name="permision">
                                    <option value="#">Who has access to this file?</option>
                                    <option value="all">Permission</option>
                                    <option value="all">Public</option>
                                    <option value="me">Private</option>
                                    
                                </select> --}}
                            </div>
                            <div class="fallback">
                              <input name="file" type="file" multiple />
                              <div class="form-group mb-0">
                                    <div class=" ">
                                        <button type="submit" class="btn btn-primary btn-block">
                                            {{ __('Upload') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                          </form>

               
            </div>
        </div>
        
        
    </div>
</div>
{{-- {{dd($request);}} --}}



 

<script>

 
    //    Dropzone.autoDiscover = false;
    Dropzone.options.myAwesomeDropzone = {
                 url:"{{ route('file.upload') }}",
                 method:"POST",
                 paramName: "file", // The name that will be used to transfer the file
                 dictDefaultMessage: "Drag and drop files here or click to upload...",
                 acceptedFiles:"audio/*,image/*,.mp3,.wav,.mp4,.psd,.pdf,video/*,.xls,.xlsx,.csv,.txt,.docx,.tiff,.eps,.svg,.xd,.ai,.psd,.ppt,.pptx",
                 maxFilesize: 10240000, // MB
                 maxFiles: 1000,
                 uploadMultiple: true,
                 timeout:100000,
                 parallelUploads:10,
                 addRemoveLinks: true,
                 
                 init: function() {
                     var myDropzone = this;
                     this.on("sending", function(file, xhr, formData) {
                        // Will send the filesize along with the file as POST data.
                        formData.append("permission",  $('#permision').val());
                        });
                   
                     this.on("uploadprogress", function(file, progress) {
                         console.log("File progress", progress);
                     });

                     this.on('queuecomplete', function () {
                        location.reload();
                    });
                     },
                    
 
                 };
                 
                 
 
 
 
 
 
   </script>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<script>
    function goBack() {
      window.history.back();
    }
    </script>
@endsection

