@extends('layouts.app')

@section('content')





<div class="container-fluid">
    <div class="row justify-content-center m-0">

        <div class="col-md-12 p-0">
        
            {{-- breadcrumb --}}

            <div class="card border-0">
           

                <div class="card-header border-0 pt-0 pb-0 bg-white text-success font-weight-bolder">Notifications
                        <div class="float-right">
                            <a onclick="goBack()" class="btn btn-default text-danger hb"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</a>
                            <a href="{{ route('home') }}" class="btn btn-default text-primary hb"> <i class="fa fa-home" aria-hidden="true"></i> Home</a>
                        </div> 
                    

                </div>

                <div class="card-body ">
                    <div class="row justify-content-center">
                        <div class="col-sm-4 mb-4" style="max-height:80vh;">

                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                  <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><i class="fa fa-align-justify" aria-hidden="true"></i> Messages 
                                    
                                    @php
                                        $user = Auth::user();
                                        $alerts = DB::table('notifications')
                                                    ->where('recipient_user_id', '=',$user->id)
                                                    ->where('seen', '=','0')
                                                    ->get();
                                    @endphp

                                    @if (!$alerts->isEmpty())
                                    
                                    @php
                                        $count = $alerts->count();
                                    @endphp
                                    <span class="badge badge-success">{{$count}}</span>
                                    @endif
                                </a>
                                   

                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"><i class="fa fa-bell" aria-hidden="true"></i> File notifications 
                                         @php
            
                                            $alertsf = DB::table('file_notifications')
                                                        ->where('owner', '=', Auth::user()->id)
                                                        ->where('seen', '=','0')
                                                        ->get();
                                        @endphp

                                        @if (!$alertsf->isEmpty())
                                        
                                            @php
                                                $countf = $alertsf->count();
                                            @endphp
                                        <span class="badge badge-success">{{$countf}}</span>
                                        @endif
                                  </a>
                                </li>
                            </ul>
                              <div class="tab-content" id="pills-tabContent border border-dark">
                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <ul class="list-group">
                               
                                        @foreach ($notifications as $notification)
                                                    @php
        
                                                         $user = Auth::user();
                                                         $announcement = DB::table('announcements')
                                                                        ->where('id', '=',$notification->announcement_id)
                                                                        ->first();
                                                    @endphp
                                               
                                                @if ($notification->seen == '0')
                                                
                                                   
                                                </li>
                                                    <a  href="{{route('notificationview',$announcement->id)}}" class="text-dark font-weight-bold mb-1 text-decoration-none" > 
                                                        <li  class="list-group-item d-flex justify-content-between align-items-center" style="background-color:  #00968817">
                                                            {{ $announcement->tittle }}
                                                            <span class="badge badge-default badge"> {{ $announcement->sender }}</span>
                                                            <span class="badge badge-success badge-pill"> {{ $notification->time_sent }}</span>
                                                        </li>
                                                    </a>
                                                @elseif ($notification->seen == '1')
        
                                                
        
                                                    <a href="{{route('notificationview',$announcement->id)}}" class="text-dark mb-1 font-weight-normal text-decoration-none"> 
                                                        <li  class="list-group-item d-flex justify-content-between align-items-center">
                                                            {{ $announcement->tittle }}
                                                            <span class="badge badge-default badge"> {{ $announcement->sender }}</span>
                                                            <span class="badge badge-light badge-pill"> {{ $notification->time_sent }}</span>
                                                        </li>
                                                    </a>
                                                @endif
                                          @endforeach
                                      </ul>
                                </div>
                                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                    
                                    <ul class="list-group">
                                        @php
                                            $user = Auth::user();
                                            $filenotifications = DB::table('file_notifications')->where('owner', '=',$user->id)->orderBy('seen','asc')->get();
                                        @endphp
                               
                                        @foreach ($filenotifications as $filenotification)
                                             @php
                                                $actioneer = DB::table('users')->where('id', '=',$filenotification->actioneer)->first();
                                             @endphp
                                               
                                                @if ($filenotification->seen == '0')
                                                
                                                   
                                                </li>
                                                    <a  href="{{route('filenotificationview',$filenotification->id)}}" class="text-dark font-weight-bold mb-1 text-decoration-none" > 
                                                        <li  class="list-group-item d-flex justify-content-between align-items-center" style="background-color:  #00968817">
                                                            {{ $actioneer->firstname }} {{ $filenotification->action }} .
                                                            <span class="badge badge-success badge-pill"> {{ $filenotification->created_at }}</span>
                                                        </li>
                                                    </a>
                                                @elseif ($filenotification->seen == '1')
        
                                                
        
                                                    <a href="{{route('filenotificationview',$filenotification->id)}}" class="text-dark mb-1 font-weight-normal text-decoration-none"> 
                                                        <li  class="list-group-item d-flex justify-content-between align-items-center">
                                                            {{ $actioneer->firstname }} {{ $filenotification->action }}.
                                                            <span class="badge badge-light badge-pill"> {{ $filenotification->created_at }}</span>
                                                        </li>
                                                    </a>
                                                @endif
                                          @endforeach
                                      </ul>
                                </div>
                              </div>

                           
                        </div>
                            <div class="col-sm-8 bg-white">
                                            <div class="row justify-content-center bg-white shadow-lg ">
                                        
                                                <div class="col-sm-11 bg-white  m-0 p-4 " style="height:80vh; background:url('/images/opened.svg'); background-repeat:no-repeat; background-size:contain;">
                                                    
                                                    <p class=" text-center text-light font-weight-bolder mt-5 pt-5" style=" font-size:20px">Select an item to Read</p>
                                                    

                                                </div>
                                            </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>






  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<script>
    function goBack() {
      window.history.back();
    }
    </script>

    <style>
        .nav-pills{
            background-color:  #00968859 !important;
        }
        .nav-pills .nav-item .nav-link{
            font-weight: bold;
            color:  #000 !important;
            border-radius: 0px !important;
        }
        .nav-pills .nav-item.active{
            background-color:  #009688 !important;
        }
        .nav-pills .nav-link.active,
        .nav-pills .show > .nav-link {
            color: #fff !important;
            background-color: #009688;
        }
         /* Extra small devices (phones, 600px and down) */
         @media only screen and (max-width: 600px) {
        .header, .list-group-item, .message, .nav-link, .card-header, .hb {font-size: 10px;}
    }
    </style>
@endsection