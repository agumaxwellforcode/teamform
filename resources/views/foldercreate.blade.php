@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header font-weight-bolder">
                    <!-- Example single danger button -->
                        <div class="btn-group">
                                <button type="button" class="btn btn-primary dropdown-toggle font-weight-bolder" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add New
                                </button>
                                <div class="dropdown-menu">
                                <a class="dropdown-item font-weight-bolder text-primary" href="{{route('push')}}"> <i class="fa fa-file-alt" aria-hidden="true"></i> File</a>
                                <a href="{{ route('home') }}" class="btn btn-default text-primary"> <i class="fa fa-home" aria-hidden="true"></i> Home</a>
                                {{-- <a class="dropdown-item" href="#">Something else here</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Separated link</a> --}}
                                </div>
                            </div>
                        <div class="float-right">
                            <a onclick="goBack()" class="btn btn-default text-danger"> <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</a>
                                <a href="{{route('folder.index')}}" class="btn btn-default text-primary "> <i class="fa fa-folder" aria-hidden="true"></i> Folders</a>
                            </div>  

                </div>

                @if ($message = Session::get('success'))
    
                <div class="alert alert-success text-center alert-block">

                    <button type="button" class="close" data-dismiss="alert">×</button>

                    <strong>{{ $message }}</strong>

                </div>

            @endif
            @if ($message = Session::get('error'))
    
            <div class="alert alert-danger text-center alert-block">

                <button type="button" class="close" data-dismiss="alert">×</button>

                <strong>{{ $message }}</strong>

            </div>

        @endif
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{-- You are logged in!
                    <a href="{{route('push')}}">Uplaod</a> --}}

                    <div class="row justify-content-center">

                        <div class="col-sm-10 col-md-6 col-lg-4 mt-3">
                                <form action="{{ route('create') }}"  method="POST">
                                    @csrf
                                    {{ method_field('POST') }}
{{-- {{dd($user->id)}} --}}
                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1" class="font-weight-bold">Folder Name</label>
                                            <input type="text" name="name" class="form-control font-weight-bold" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter the folder name" required>
                                        
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1" class="font-weight-bold">Permission</label>
                                            <select class="form-control font-weight-bold" id="exampleFormControlSelect1" name="permision">
                                                {{-- <option value="#">Who has access to this file?</option> --}}
                                                <option value="all">Public</option>
                                                {{-- <option value="admin">Admin Only</option> --}}
                                                <option value="me">Private</option>
                                            </select>
                                        </div>
                                    <br>
                                        <button type="submit" class="btn btn-primary float-right mt-1 mb-4"> <i class="fa fa-plus" aria-hidden="true"></i> Create</button>
                                    </form>
                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script>
    function goBack() {
      window.history.back();
    }
    </script>
@endsection
