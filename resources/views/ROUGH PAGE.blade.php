<div class="row ">
    @foreach ($folders as $folder)
       <div class="col-sm-1 bg-light">
        <a href="{{route('subhome',$folder)}}"><img class="img img-fluid rounded" src="/images/icons/folder.svg" alt="" srcset=""></a>
       <p class="text-center" style="font-size:10px;">{{$folder->name}}</p>
       </div>
    @endforeach
    
</div>
<hr>










<div class="row ">
    @foreach ($files as $file)
    <div class="col-sm-1  m-2 p-3 bg-light text-center border">
        {{-- {{dd($extension)}}  $extension =='docx'--}}
        @if(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='docx' )
        <a href="{{route('text',$file)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%" src="/images/icons/docs.svg" alt="" srcset=""></a>
           {{--<p class="text-center">{{$file->filename}}</p>--}}
       
        @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='PNG')
        <a href="{{route('image',$file)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"  src="{{"storage/files/$file->filename"}}" alt="" srcset=""></a>
       {{--<p class="text-center">{{$file->filename}}</p>--}}

        @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='png')
        <a href="{{route('image',$file)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"  src="{{"storage/files/$file->filename"}}" alt="" srcset=""></a>
       {{--<p class="text-center">{{$file->filename}}</p>--}}

        @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='jpg')
        <a href="{{route('image',$file)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"  src="{{"storage/files/$file->filename"}}" alt="" srcset=""></a>
       {{--<p class="text-center">{{$file->filename}}</p>--}}

        @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='JPG')
        <a href="{{route('image',$file)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"   src="{{"storage/files/$file->filename"}}" alt="" srcset=""></a>
       {{--<p class="text-center">{{$file->filename}}</p>--}}

       @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='JPEG')
       <a href="{{route('image',$file)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"  src="{{"storage/files/$file->filename"}}" alt="" srcset=""></a>
       {{-- <p class="text-center">{{$file->filename}}</p> --}}

       @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='jpeg')
       <a href="{{route('image',$file)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"  src="{{"storage/files/$file->filename"}}" alt="" srcset=""></a>
       {{-- <p class="text-center">{{$file->filename}}</p> --}}


        @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='pdf')
        <a href="{{route('pdf',$file)}}"><img class="img img-fluid rounded" src="/images/icons/pdf.svg" alt="" srcset="" ></a>
       {{--<p class="text-center">{{$file->filename}}</p>--}}

        @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='mp3')
        <a href="{{route('audio',$file)}}"><img class="img img-fluid rounded" src="/images/icons/mp3.svg" alt="" srcset="" ></a>
       {{--<p class="text-center">{{$file->filename}}</p>--}}

        @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='csv')
        <a href="{{route('sheet',$file)}}"><img class="img img-fluid rounded" src="/images/icons/sheets.svg" alt="" srcset="" ></a>
       {{--<p class="text-center">{{$file->filename}}</p>--}}

        @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='xlsx')
        <a href="{{route('sheet',$file)}}"><img class="img img-fluid rounded" src="/images/icons/sheets.svg" alt="" srcset="" ></a>
       {{--<p class="text-center">{{$file->filename}}</p>--}}

        @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='html')
        <a href="{{route('sheet',$file)}}"><img class="img img-fluid rounded" src="/images/icons/html.svg" alt="" srcset="" ></a>
       {{--<p class="text-center">{{$file->filename}}</p>--}}
        

        @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='pptx')
        <a href="{{route('slide',$file)}}"><img class="img img-fluid rounded" src="/images/icons/slides.svg" alt="" srcset="" ></a>
       {{--<p class="text-center">{{$file->filename}}</p>--}}


       {{-- <a href="{{ route('customer.plans.index', ['customer' => $customer->id, 'plan' => $plan->id]) }}">{{ $plan->name }}</a> --}}

       {{-- <form action="{{route('admin.users.destroy',$user)}}" method="post" >
              @csrf
              {{ method_field('DELETE') }}
              <button type="submit" class="btn btn-outline-danger" style="padding:3px;"><i class="fa fa-trash" aria-hidden="true"></i></button>
          </form> --}}
        @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='mp4')
      
        <a href="{{route('video',$file)}}"><img class="img img-fluid rounded" src="/images/icons/cone.svg" alt="" srcset="" ></a>
       {{--<p class="text-center">{{$file->filename}}</p>--}}

        @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='zip')
        <a href="#"><img class="img img-fluid rounded" src="/images/icons/zip.svg" alt="" srcset=""  ></a>
       {{--<p class="text-center">{{$file->filename}}</p>--}}

        @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='rar')
        <a href="#"><img class="img img-fluid rounded" src="/images/icons/zip.svg" alt="" srcset=""  ></a>
       {{--<p class="text-center">{{$file->filename}}</p>--}}
        @endif
    </div>
    @endforeach
    
</div>

































<script>
    // "myAwesomeDropzone" is the camelized version of the HTML element's ID
    Dropzone.options.myAwesomeDropzone = {
    paramName: "file", // The name that will be used to transfer the file
    maxFilesize: 1024, // MB
    addRemoveLinks: true,
    init: function() {
        this.on("uploadprogress", function(file, progress) {
            console.log("File progress", progress);
        });
        }
    acceptedFiles:"audio/*,image/*,.psd,.pdf,video/*,.txt,.docx,.tiff,.eps,.svg,.xd,.ai,.psd"
    accept: function(file, done) {
        if (file.name == "justinbieber.jpg") {
        done("Naha, you don't.");
        }
        else { done(); }
    }
    };
</script>















































<div class="row p-1">
                       
    @foreach ($privateFiles as $privateFile)
      <div class="col-sm-1  m-2 p-3 bg-light text-center border">
      {{-- {{dd($extension)}}  $extension =='docx'--}}
      @if(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='docx' )
      <a href="{{route('text',$privateFile)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%" src="/images/icons/docs.svg" alt="" srcset=""></a>
         {{--<p class="text-center">{{$file->filename}}</p>--}}
     
      @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='PNG')
      <a href="{{route('image',$privateFile)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"  src="{{"storage/files/$privateFile->filename"}}" alt="" srcset=""></a>
     {{--<p class="text-center">{{$file->filename}}</p>--}}

      @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='png')
      <a href="{{route('image',$privateFile)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"  src="{{"storage/files/$privateFile->filename"}}" alt="" srcset=""></a>
     {{--<p class="text-center">{{$file->filename}}</p>--}}

      @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='jpg')
      <a href="{{route('image',$privateFile)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"  src="{{"storage/files/$privateFile->filename"}}" alt="" srcset=""></a>
     {{--<p class="text-center">{{$file->filename}}</p>--}}

      @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='JPG')
      <a href="{{route('image',$privateFile)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"   src="{{"storage/files/$privateFile->filename"}}" alt="" srcset=""></a>
     {{--<p class="text-center">{{$file->filename}}</p>--}}

     @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='JPEG')
     <a href="{{route('image',$privateFile)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"  src="{{"storage/files/$privateFile->filename"}}" alt="" srcset=""></a>
     {{-- <p class="text-center">{{$file->filename}}</p> --}}

     @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='jpeg')
     <a href="{{route('image',$privateFile)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"  src="{{"storage/files/$privateFile->filename"}}" alt="" srcset=""></a>
     {{-- <p class="text-center">{{$file->filename}}</p> --}}

      @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='pdf')
      <a href="{{route('pdf',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/pdf.svg" alt="" srcset="" ></a>
     {{--<p class="text-center">{{$file->filename}}</p>--}}

      @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='mp3')
      <a href="{{route('audio',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/mp3.svg" alt="" srcset="" ></a>
     {{--<p class="text-center">{{$file->filename}}</p>--}}

      @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='csv')
      <a href="{{route('sheet',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/sheets.svg" alt="" srcset="" ></a>
     {{--<p class="text-center">{{$file->filename}}</p>--}}

      @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='xlsx')
      <a href="{{route('sheet',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/sheets.svg" alt="" srcset="" ></a>
     {{--<p class="text-center">{{$file->filename}}</p>--}}

      @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='html')
      <a href="{{route('sheet',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/html.svg" alt="" srcset="" ></a>
     {{--<p class="text-center">{{$file->filename}}</p>--}}
      

      @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='pptx')
      <a href="{{route('slide',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/slides.svg" alt="" srcset="" ></a>
     {{--<p class="text-center">{{$file->filename}}</p>--}}


     {{-- <a href="{{ route('customer.plans.index', ['customer' => $customer->id, 'plan' => $plan->id]) }}">{{ $plan->name }}</a> --}}

     {{-- <form action="{{route('admin.users.destroy',$user)}}" method="post" >
            @csrf
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-outline-danger" style="padding:3px;"><i class="fa fa-trash" aria-hidden="true"></i></button>
        </form> --}}
      @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='mp4')
    
      <a href="{{route('video',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/cone.svg" alt="" srcset="" ></a>
     {{--<p class="text-center">{{$file->filename}}</p>--}}

      @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='zip')
      <a href="#"><img class="img img-fluid rounded" src="/images/icons/zip.svg" alt="" srcset=""  ></a>
     {{--<p class="text-center">{{$file->filename}}</p>--}}

      @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='rar')
      <a href="#"><img class="img img-fluid rounded" src="/images/icons/zip.svg" alt="" srcset=""  ></a>
     {{--<p class="text-center">{{$file->filename}}</p>--}}
      @endif
    </div>
    @endforeach
    
</div>












$fileName = str_replace(' ', '', $file->getClientOriginalName());































































































@@foreach ($files as $file)
<div class="col-sm-1  m-2 p-3 bg-light text-center border">
   


   {{ dd($file)}} --}}

    @if(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='docx' )
    <a href="{{route('subtext',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%" src="/images/icons/docs.svg" alt="" srcset=""></a>
    {{--<p class="text-center">{{$file->filename}}</p>--}}
    

    <div class="dropdown-menu dropdown-menu-sm m-0" id="{{$file->id}}">
        <a class="dropdown-item" href="#">Open</a>
        <a class="dropdown-item text-success" href="#">Share</a>
        <a class="dropdown-item text-danger" href="#">Delete</a>
      </div>
      <script>

        $('.{{$file->id}}').on('contextmenu', function(e) {
         
          $('#{{$file->id}}').css({
            display: "block",
           
          }).addClass("show");
          return false; //blocks default Webbrowser right click menu

        })
        $('.row').on("click", function() {
            $('#{{$file->id}}').removeClass("show").hide();
        });

        $('.row').on("contextmenu", function() {
            $('#{{$file->id}}').removeClass("show").hide();
        });
        
        
        $('#{{$file->id}} a').on("click", function() {
          $(this).parent().removeClass("show").hide();
        });
        
    </script>
  
   



    @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='PNG')
    <a href="{{route('subimage',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"   src="{{url("storage/$target->path/$file->filename")}}" alt="" srcset=""></a>
{{--<p class="text-center">{{$file->filename}}</p>--}}


<div class="dropdown-menu dropdown-menu-sm m-0" id="{{$file->id}}">
    <a class="dropdown-item" href="#">Open</a>
    <a class="dropdown-item text-success" href="#">Share</a>
    <a class="dropdown-item text-danger" href="#">Delete</a>
  </div>
  <script>

    $('.{{$file->id}}').on('contextmenu', function(e) {
     
      $('#{{$file->id}}').css({
        display: "block",
       
      }).addClass("show");
      return false; //blocks default Webbrowser right click menu

    })
    $('.row').on("click", function() {
        $('#{{$file->id}}').removeClass("show").hide();
    });
    $('.row').on("contextmenu", function() {
            $('#{{$file->id}}').removeClass("show").hide();
        });
    
    $('#{{$file->id}} a').on("click", function() {
      $(this).parent().removeClass("show").hide();
    });
    
</script>





    @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='png')
    <a href="{{route('subimage',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"   src="{{url("storage/$target->path/$file->filename")}}" alt="" srcset=""></a>
{{--<p class="text-center">{{$file->filename}}</p>--}}


<div class="dropdown-menu dropdown-menu-sm m-0" id="{{$file->id}}">
    <a class="dropdown-item" href="#">Open</a>
    <a class="dropdown-item text-success" href="#">Share</a>
    <a class="dropdown-item text-danger" href="#">Delete</a>
  </div>
  <script>

    $('.{{$file->id}}').on('contextmenu', function(e) {
     
      $('#{{$file->id}}').css({
        display: "block",
       
      }).addClass("show");
      return false; //blocks default Webbrowser right click menu

    })
    $('.row').on("click", function() {
        $('#{{$file->id}}').removeClass("show").hide();
    });
    $('.row').on("contextmenu", function() {
            $('#{{$file->id}}').removeClass("show").hide();
        });
    
    $('#{{$file->id}} a').on("click", function() {
      $(this).parent().removeClass("show").hide();
    });
    
</script>




    @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='jpg')
    <a href="{{route('subimage',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"  src="{{url("storage/$target->path/$file->filename")}}" alt="" srcset=""></a>
{{--<p class="text-center">{{$file->filename}}</p>--}}


<div class="dropdown-menu dropdown-menu-sm m-0" id="{{$file->id}}">
    <a class="dropdown-item" href="#">Open</a>
    <a class="dropdown-item text-success" href="#">Share</a>
    <a class="dropdown-item text-danger" href="#">Delete</a>
  </div>
  <script>

    $('.{{$file->id}}').on('contextmenu', function(e) {
     
      $('#{{$file->id}}').css({
        display: "block",
       
      }).addClass("show");
      return false; //blocks default Webbrowser right click menu

    })
    $('.row').on("click", function() {
        $('#{{$file->id}}').removeClass("show").hide();
    });
    $('.row').on("contextmenu", function() {
            $('#{{$file->id}}').removeClass("show").hide();
        });
    
    $('#{{$file->id}} a').on("click", function() {
      $(this).parent().removeClass("show").hide();
    });
    
</script>




    @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='JPG')
    <a href="{{route('subimage',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"    src="{{url("storage/$target->path/$file->filename")}}" alt="" srcset=""></a>
{{--<p class="text-center">{{$file->filename}}</p>--}}


<div class="dropdown-menu dropdown-menu-sm m-0" id="{{$file->id}}">
    <a class="dropdown-item" href="#">Open</a>
    <a class="dropdown-item text-success" href="#">Share</a>
    <a class="dropdown-item text-danger" href="#">Delete</a>
  </div>
  <script>

    $('.{{$file->id}}').on('contextmenu', function(e) {
     
      $('#{{$file->id}}').css({
        display: "block",
       
      }).addClass("show");
      return false; //blocks default Webbrowser right click menu

    })
    $('.row').on("click", function() {
        $('#{{$file->id}}').removeClass("show").hide();
    });
    $('.row').on("contextmenu", function() {
            $('#{{$file->id}}').removeClass("show").hide();
        });
    
    $('#{{$file->id}} a').on("click", function() {
      $(this).parent().removeClass("show").hide();
    });
    
</script>




@elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='JPEG')
<a href="{{route('subimage',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"   src="{{url("storage/$target->path/$file->filename")}}" alt="" srcset=""></a>
{{-- <p class="text-center">{{$file->filename}}</p> --}}


<div class="dropdown-menu dropdown-menu-sm m-0" id="{{$file->id}}">
    <a class="dropdown-item" href="#">Open</a>
    <a class="dropdown-item text-success" href="#">Share</a>
    <a class="dropdown-item text-danger" href="#">Delete</a>
  </div>
  <script>

    $('.{{$file->id}}').on('contextmenu', function(e) {
     
      $('#{{$file->id}}').css({
        display: "block",
       
      }).addClass("show");
      return false; //blocks default Webbrowser right click menu

    })
    $('.row').on("click", function() {
        $('#{{$file->id}}').removeClass("show").hide();
    });
    $('.row').on("contextmenu", function() {
            $('#{{$file->id}}').removeClass("show").hide();
        });
    
    $('#{{$file->id}} a').on("click", function() {
      $(this).parent().removeClass("show").hide();
    });
    
</script>




@elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='jpeg')
<a href="{{route('subimage',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"   src="{{url("storage/$target->path/$file->filename")}}" alt="" srcset=""></a>
{{-- <p class="text-center">{{$file->filename}}</p> --}}



<div class="dropdown-menu dropdown-menu-sm m-0" id="{{$file->id}}">
    <a class="dropdown-item" href="#">Open</a>
    <a class="dropdown-item text-success" href="#">Share</a>
    <a class="dropdown-item text-danger" href="#">Delete</a>
  </div>
  <script>

    $('.{{$file->id}}').on('contextmenu', function(e) {
     
      $('#{{$file->id}}').css({
        display: "block",
       
      }).addClass("show");
      return false; //blocks default Webbrowser right click menu

    })
    $('.row').on("click", function() {
        $('#{{$file->id}}').removeClass("show").hide();
    });
    $('.row').on("contextmenu", function() {
            $('#{{$file->id}}').removeClass("show").hide();
        });
    
    $('#{{$file->id}} a').on("click", function() {
      $(this).parent().removeClass("show").hide();
    });
    
</script>




@elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='pdf')
<a href="{{route('subpdf',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" src="/images/icons/pdf.svg" alt="" srcset="" ></a>
{{-- <p class="text-center">{{$file->filename}}</p> --}}


<div class="dropdown-menu dropdown-menu-sm m-0" id="{{$file->id}}">
    <a class="dropdown-item" href="#">Open</a>
    <a class="dropdown-item text-success" href="#">Share</a>
    <a class="dropdown-item text-danger" href="#">Delete</a>
  </div>
  <script>

    $('.{{$file->id}}').on('contextmenu', function(e) {
     
      $('#{{$file->id}}').css({
        display: "block",
       
      }).addClass("show");
      return false; //blocks default Webbrowser right click menu

    })
    $('.row').on("click", function() {
        $('#{{$file->id}}').removeClass("show").hide();
    });
    $('.row').on("contextmenu", function() {
            $('#{{$file->id}}').removeClass("show").hide();
        });
    
    $('#{{$file->id}} a').on("click", function() {
      $(this).parent().removeClass("show").hide();
    });
    
</script>




@elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='mp3')
<a href="{{route('subaudio',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" src="/images/icons/mp3.svg" alt="" srcset="" ></a>
{{-- <p class="text-center">{{$file->filename}}</p> --}}


<div class="dropdown-menu dropdown-menu-sm m-0" id="{{$file->id}}">
    <a class="dropdown-item" href="#">Open</a>
    <a class="dropdown-item text-success" href="#">Share</a>
    <a class="dropdown-item text-danger" href="#">Delete</a>
  </div>
  <script>

    $('.{{$file->id}}').on('contextmenu', function(e) {
     
      $('#{{$file->id}}').css({
        display: "block",
       
      }).addClass("show");
      return false; //blocks default Webbrowser right click menu

    })
    $('.row').on("click", function() {
        $('#{{$file->id}}').removeClass("show").hide();
    });
    $('.row').on("contextmenu", function() {
            $('#{{$file->id}}').removeClass("show").hide();
        });
    
    $('#{{$file->id}} a').on("click", function() {
      $(this).parent().removeClass("show").hide();
    });
    
</script>




@elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='csv')
<a href="{{route('subsheet',$file)}}"class="{{$file->id}}"><img class="img img-fluid rounded" src="/images/icons/sheets.svg" alt="" srcset="" ></a>
{{-- <p class="text-center">{{$file->filename}}</p> --}}


<div class="dropdown-menu dropdown-menu-sm m-0" id="{{$file->id}}">
    <a class="dropdown-item" href="#">Open</a>
    <a class="dropdown-item text-success" href="#">Share</a>
    <a class="dropdown-item text-danger" href="#">Delete</a>
  </div>
  <script>

    $('.{{$file->id}}').on('contextmenu', function(e) {
     
      $('#{{$file->id}}').css({
        display: "block",
       
      }).addClass("show");
      return false; //blocks default Webbrowser right click menu

    })
    $('.row').on("click", function() {
        $('#{{$file->id}}').removeClass("show").hide();
    });
    $('.row').on("contextmenu", function() {
            $('#{{$file->id}}').removeClass("show").hide();
        });
    
    $('#{{$file->id}} a').on("click", function() {
      $(this).parent().removeClass("show").hide();
    });
    
</script>




@elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='xlsx')
<a href="{{route('subsheet',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" src="/images/icons/sheets.svg" alt="" srcset="" ></a>
{{-- <p class="text-center">{{$file->filename}}</p> --}}


<div class="dropdown-menu dropdown-menu-sm m-0" id="{{$file->id}}">
    <a class="dropdown-item" href="#">Open</a>
    <a class="dropdown-item text-success" href="#">Share</a>
    <a class="dropdown-item text-danger" href="#">Delete</a>
  </div>
  <script>

    $('.{{$file->id}}').on('contextmenu', function(e) {
     
      $('#{{$file->id}}').css({
        display: "block",
       
      }).addClass("show");
      return false; //blocks default Webbrowser right click menu

    })
    $('.row').on("click", function() {
        $('#{{$file->id}}').removeClass("show").hide();
    });

    $('.row').on("contextmenu", function() {
            $('#{{$file->id}}').removeClass("show").hide();
        });
    
    $('#{{$file->id}} a').on("click", function() {
      $(this).parent().removeClass("show").hide();
    });
    
</script>




@elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='html')
<a href="{{route('subtext',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" src="/images/icons/html.svg" alt="" srcset="" ></a>
{{-- <p class="text-center">{{$file->filename}}</p> --}}


<div class="dropdown-menu dropdown-menu-sm m-0" id="{{$file->id}}">
    <a class="dropdown-item" href="#">Open</a>
    <a class="dropdown-item text-success" href="#">Share</a>
    <a class="dropdown-item text-danger" href="#">Delete</a>
  </div>
  <script>

    $('.{{$file->id}}').on('contextmenu', function(e) {
     
      $('#{{$file->id}}').css({
        display: "block",
       
      }).addClass("show");
      return false; //blocks default Webbrowser right click menu

    })
    $('.row').on("click", function() {
        $('#{{$file->id}}').removeClass("show").hide();
    });
    $('.row').on("contextmenu", function() {
            $('#{{$file->id}}').removeClass("show").hide();
        });
    
    $('#{{$file->id}} a').on("click", function() {
      $(this).parent().removeClass("show").hide();
    });
    
</script>





@elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='pptx')
<a href="{{route('subslide',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" src="/images/icons/slides.svg" alt="" srcset="" ></a>
{{-- <p class="text-center">{{$file->filename}}</p> --}}


<div class="dropdown-menu dropdown-menu-sm m-0" id="{{$file->id}}">
    <a class="dropdown-item" href="#">Open</a>
    <a class="dropdown-item text-success" href="#">Share</a>
    <a class="dropdown-item text-danger" href="#">Delete</a>
  </div>
  <script>

    $('.{{$file->id}}').on('contextmenu', function(e) {
     
      $('#{{$file->id}}').css({
        display: "block",
       
      }).addClass("show");
      return false; //blocks default Webbrowser right click menu

    })
    $('.row').on("click", function() {
        $('#{{$file->id}}').removeClass("show").hide();
    });
    $('.row').on("contextmenu", function() {
            $('#{{$file->id}}').removeClass("show").hide();
        });
    
    $('#{{$file->id}} a').on("click", function() {
      $(this).parent().removeClass("show").hide();
    });
    
</script>




@elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='mp4')
<a href="{{route('subvideo',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" src="/images/icons/cone.svg" alt="" srcset="" ></a>
{{-- <p class="text-center">{{$file->filename}}</p> --}}


<div class="dropdown-menu dropdown-menu-sm m-0" id="{{$file->id}}">
    <a class="dropdown-item" href="#">Open</a>
    <a class="dropdown-item text-success" href="#">Share</a>
    <a class="dropdown-item text-danger" href="#">Delete</a>
  </div>
  <script>

    $('.{{$file->id}}').on('contextmenu', function(e) {
     
      $('#{{$file->id}}').css({
        display: "block",
       
      }).addClass("show");
      return false; //blocks default Webbrowser right click menu

    })
    $('.row').on("click", function() {
        $('#{{$file->id}}').removeClass("show").hide();
    });
    $('.row').on("contextmenu", function() {
            $('#{{$file->id}}').removeClass("show").hide();
        });
    
    $('#{{$file->id}} a').on("click", function() {
      $(this).parent().removeClass("show").hide();
    });
    
</script>




@elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='zip')
  <a href="{{route('subpdf',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" src="/images/icons/zip.svg" alt="" srcset=""  ></a>
  {{-- <p class="text-center">{{$file->filename}}</p> --}}


  <div class="dropdown-menu dropdown-menu-sm m-0" id="{{$file->id}}">
    <a class="dropdown-item" href="#">Open</a>
    <a class="dropdown-item text-success" href="#">Share</a>
    <a class="dropdown-item text-danger" href="#">Delete</a>
  </div>
  <script>

    $('.{{$file->id}}').on('contextmenu', function(e) {
     
      $('#{{$file->id}}').css({
        display: "block",
       
      }).addClass("show");
      return false; //blocks default Webbrowser right click menu

    })
    $('.row').on("click", function() {
        $('#{{$file->id}}').removeClass("show").hide();
    });
    $('.row').on("contextmenu", function() {
            $('#{{$file->id}}').removeClass("show").hide();
        });
    
    $('#{{$file->id}} a').on("click", function() {
      $(this).parent().removeClass("show").hide();
    });
    
</script>




  @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='rar')
  <a href="{{route('subpdf',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" src="/images/icons/zip.svg" alt="" srcset=""  ></a>
  {{-- <p class="text-center">{{$file->filename}}</p> --}}


  <div class="dropdown-menu dropdown-menu-sm m-0 p-0" id="{{$file->id}}">
    <a class="dropdown-item" href="#">Open</a>
    <a class="dropdown-item text-success" href="#">Share</a>
    <a class="dropdown-item text-danger" href="#">Delete</a>
  </div>
  <script>

    $('.{{$file->id}}').on('contextmenu', function(e) {
     
      $('#{{$file->id}}').css({
        display: "block",
       
      }).addClass("show");
      return false; //blocks default Webbrowser right click menu

    })
    $('.row').on("click", function() {
        $('#{{$file->id}}').removeClass("show").hide();
    });
    $('.row').on("contextmenu", function() {
            $('#{{$file->id}}').removeClass("show").hide();
        });
    
    $('#{{$file->id}} a').on("click", function() {
      $(this).parent().removeClass("show").hide();
    });
    
</script>


  @endif




















  
</div>
@endforeach



































































@foreach ($notifications as $notification)
<div class="col-sm-4">
<div class="card">
    
    <div class="card-body">

            @php

                    $user = Auth::user();
                    $announcement = DB::table('announcements')
                                ->where('id', '=',$notification->announcement_id)
                                ->first();
            @endphp

    <h5 class="card-title font-weight-bolder">{{ $announcement->tittle }}</h5>
    <h6 class="card-subtitle mb-2 text-muted text-info" style="font-size:10px;">Created by  {{ $announcement->sender }} <br> {{ $announcement->created_at }}</h6> <hr>
    <p class="card-text">{{ $announcement->message }}</p> <hr>

    <a href="{{route('admin.announcement.edit',$announcement->id)}}" class="card-link">
        <button type="button" class="btn btn-outline-primary float-left mr-1" style="padding:3px;"> 
        <i class="fas fa-edit "></i>
        </button>
    </a>
    <a href="#" class="card-link"> 
    <form action="{{route('admin.announcement.destroy',$announcement->id)}}" method="post" class="float-left">
        @csrf
        {{ method_field('DELETE') }}
        <button type="submit" class="btn btn-outline-danger" style="padding:3px;"><i class="fa fa-trash" aria-hidden="true"></i></button>
    </form>
</a>



<a href="{{route('admin.notification.show',$announcement->id)}}" class="card-link float-right">
    <button type="button" class="btn btn-outline-success float-left mr-1" style="padding:3px;"> 
        <i class="fas fa-share-alt "></i>
    </button>
    </a>
</div>
</div>
</div>
@endforeach