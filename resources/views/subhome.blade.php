@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header font-weight-bolder">
                    <!-- Example single danger button -->
                        <div class="btn-group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-align-justify" aria-hidden="true"></i>
                                </button>
                                <div class="dropdown-menu">
                                <a class="dropdown-item font-weight-bolder text-primary" href="{{route('home')}}"> <i class="fa fa-file-alt" aria-hidden="true"></i> File</a>
                                <a class="dropdown-item font-weight-bolder text-primary" href="{{route('folder.index')}}"> <i class="fa fa-folder" aria-hidden="true"></i> Folder</a>
                                {{-- <a class="dropdown-item" href="#">Something else here</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Separated link</a> --}}
                                </div>
                            </div>
                        <div class="float-right">
                                <a href="{{route('push')}}" class="btn btn-primary "> <i class="fa fa-cloud-upload-alt" aria-hidden="true"></i> Upload</a>
                            </div>  

                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{-- You are logged in!
                    <a href="{{route('push')}}">Uplaod</a> --}}

                    <div class="row ">
                        @foreach ($files as $file)
                        <div class="col-sm-1  m-2 p-3 bg-light text-center border">
                            {{-- {{dd($extension)}}  $extension =='docx'--}}
                            @if(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='docx' )
                            <a href="{{route('text',$file)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%" src="/images/icons/docs.svg" alt="" srcset=""></a>
                            {{--<p class="text-center">{{$file->filename}}</p>--}}
                        
                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='PNG')
                            <a href="{{route('image',$file)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"  src="{{url("$file->path/$file->filename")}}" alt="" srcset=""></a>
                        {{--<p class="text-center">{{$file->filename}}</p>--}}

                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='png')
                            <a href="{{route('image',$file)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"  src="{{url("storage/$file->path/$file->filename")}}" alt="" srcset=""></a>
                        {{--<p class="text-center">{{$file->filename}}</p>--}}

                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='jpg')
                            <a href="{{route('image',$file)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"  src="{{url("storage/$file->path/$file->filename")}}" alt="" srcset=""></a>
                        {{--<p class="text-center">{{$file->filename}}</p>--}}

                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='JPG')
                            <a href="{{route('image',$file)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"   src="{{url("storage/$file->path/$file->filename")}}" alt="" srcset=""></a>
                        {{--<p class="text-center">{{$file->filename}}</p>--}}

                        @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='JPEG')
                        <a href="{{route('image',$file)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"  src="{{url("storage/$file->path/$file->filename")}}" alt="" srcset=""></a>
                        {{-- <p class="text-center">{{$file->filename}}</p> --}}

                        @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='jpeg')
                        <a href="{{route('image',$file)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"  src="{{url("storage/$file->path/$file->filename")}}" alt="" srcset=""></a>
                        {{-- <p class="text-center">{{$file->filename}}</p> --}}


                        @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='pdf')
                        <a href="#"><img class="img img-fluid rounded" src="/images/icons/pdf.svg" alt="" srcset="" ></a>
                        {{-- <p class="text-center">{{$file->filename}}</p> --}}

                        @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='mp3')
                        <a href="#"><img class="img img-fluid rounded" src="/images/icons/mp3.svg" alt="" srcset="" ></a>
                        {{-- <p class="text-center">{{$file->filename}}</p> --}}

                        @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='csv')
                        <a href="#"><img class="img img-fluid rounded" src="/images/icons/sheets.svg" alt="" srcset="" ></a>
                        {{-- <p class="text-center">{{$file->filename}}</p> --}}

                        @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='xlsx')
                        <a href="#"><img class="img img-fluid rounded" src="/images/icons/sheets.svg" alt="" srcset="" ></a>
                        {{-- <p class="text-center">{{$file->filename}}</p> --}}

                        @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='html')
                        <a href="#"><img class="img img-fluid rounded" src="/images/icons/html.svg" alt="" srcset="" ></a>
                        {{-- <p class="text-center">{{$file->filename}}</p> --}}
                        

                        @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='pptx' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='PPTX' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='PPT' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='ppt')
                        <a href="#"><img class="img img-fluid rounded" src="/images/icons/slides.svg" alt="" srcset="" ></a>
                        {{-- <p class="text-center">{{$file->filename}}</p> --}}

                        @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='mp4')
                        <a href="#"><img class="img img-fluid rounded" src="/images/icons/cone.svg" alt="" srcset="" ></a>
                        {{-- <p class="text-center">{{$file->filename}}</p> --}}

                        @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='zip')
                          <a href="#"><img class="img img-fluid rounded" src="/images/icons/zip.svg" alt="" srcset=""  ></a>
                          {{-- <p class="text-center">{{$file->filename}}</p> --}}

                          @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='rar')
                          <a href="#"><img class="img img-fluid rounded" src="/images/icons/zip.svg" alt="" srcset=""  ></a>
                          {{-- <p class="text-center">{{$file->filename}}</p> --}}
                        @endif
            </div>
            @endforeach
                            
                        </div>
                </div>

                {{-- <div class="container">
                    <div class="row my-3">
                      <div class="col">
                        <div class="jumbotron bg-danger">
                          <h1>Right click menu for Bootstrap 4 - Advanced Components</h1>
                          <p class="lead">by djibe.</p>
                          <h2>
                          Demo
                          </h2>
                            <p>Just right click in this Jumbotron to test.</p>
                          <div class="dropdown-menu dropdown-menu-sm" id="context-menu">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div> --}}
                  
            </div>
        </div>
    </div>
</div>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

@endsection
