@extends('layouts.app')

@section('content')





<div class="container-fluid">
    <div class="row justify-content-center m-0">
        @can('manage-users')
        <div class="col-sm-2 p-0">
        
             <!-- Sidebar -->
             <div class="bg-light border" id="sidebar-wrapper">
                  
                <div class="list-group list-group-flush">
                  <a href="#" class="list-group-item list-group-item-action bg-dark text-white">ADMIN</a>
                  <a href="{{ route('admin.users.index') }}" class="list-group-item list-group-item-action  font-weight-bold">USERS</a>
                  {{-- <a href="{{ route('admin.group.index') }}" class="list-group-item list-group-item-action bg-light font-weight-bold">GROUPS</a> --}}
                  <a href="{{ route('admin.announcement.index') }}" class="list-group-item list-group-item-action bg-light font-weight-bold">ANNOUNCEMENT</a>
                  <a href="{{ route('admin.registration.index') }}" class="list-group-item list-group-item-action bg-light font-weight-bold">REGISTRATION</a>
                  <a href="{{ route('inbox') }}" class="list-group-item list-group-item-action bg-light font-weight-bold">INBOX
                    @php
                      $count = DB::table('contacts')->where('status','=','Not Treated')->count();
                    @endphp
                    @if ($count !== 0)
                      <span class="badge badge-pill badge-danger ml-3">
                        {{ $count }}
                      </span>
                    @endif
                  </a>
                  <a href="{{ route('quota') }}" class="list-group-item list-group-item-action bg-success text-white  font-weight-bold">QUOTA</a>
                 
                </div>
              </div>
              <!-- /#sidebar-wrapper -->


        </div>

       @endcan

        <div class="col-md-10">
            <div class="row mb-0">
                @can('manage-users')
           {{-- breadcrumb --}}

                  <nav aria-label="breadcrumb col-sm-5">
                      <ol class="breadcrumb ">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                        <li class="breadcrumb-item"><a href="#">Configuration</a></li>
                        <li class="breadcrumb-item "><a href="#">Admin</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Quota</li>
                      </ol>
                    </nav>
                   
                    <div class="col-sm-3"> 
                       

                          <div class="btn-group" role="group" aria-label="Third group">
                              <button type="button" onClick="history.go(0)" class="btn btn-default bg-light p-2"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                            </div>
                    </div>
                    @endcan

            </div>
            {{-- breadcrumb --}}

            <div class="card mt-3">

                    {{-- @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                    @endif --}}

                <div class="card-header bg-light text-dark font-weight-bolder"> <h3 class="font-weight-bolder float-left">Quota</h3>
                    
                </div>

                <div class="card-body ">
                 
                    <div class="row justify-content-center">
                      @can('manage-users')
                        <div class="col-sm-4">
                            <p class="text-center font-weight-bolder"> System Statistics</p>
                        </div>

                        <div class="col-sm-4">

                            <p class="text-center font-weight-bolder">Usage Statistics</p>
                         
                         </div>
                        @endcan
                        <div class="col-sm-4">
                          <p class="text-center font-weight-bolder"> My Usage</p>
                        </div>
                    </div> <br>
                    <div class="row justify-content-center">
                      @can('manage-users')
                        <div class="col-sm-4 pr-5 pl-5 pt-0 pb-0">
                            {!! $chart->container() !!}
                        </div>

                        <div class="col-sm-4 pr-5 pl-5 pt-0 pb-0">
                            {!! $bar->container() !!}
                        </div>
                        @endcan
                        <div class="col-sm-4 pr-5 pl-5 pt-0 pb-0">
                           {!! $userChart->container() !!}
                        </div>
                    </div>
            
<br><br>
                 
                   

              </div>
            </div>
        </div>
    </div>
</div>


{{-- <script>
  $(document).ready(function() {
 $('#example').DataTable( {
     "paging":   true,
     "ordering": true,
     "info":     false
 } );
} );
  </script> --}}

{{-- <style>
  input{
      border:1px solid #3490dc !important;
      padding: 20px !important;
  }
  select{
      border:1px solid #3490dc !important;
      padding: 10px !important;
      min-height: 45px !important;
      min-width: 100px !important;
      color: black !important;
  }
</style> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
{!! $chart->script() !!}
{!! $bar->script() !!}
{!! $userChart->script() !!}
@endsection


 
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> 

<script>
    function goBack() {
      window.history.back();
    }
</script>