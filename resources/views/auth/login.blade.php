@extends('layouts.head')

@section('content')
  <div class="container-fluid">
    <div class="row justify-content-center" id="row" >

      <div class="col-sm-12 " id="board" style="height:100vh">

        <div class="row justify-content-end  m-0 mr-lg-5">
            <div class="col-sm-11 col-md-6 col-lg-4 col-xl-3 p-3 shadow pt-5" style="margin-top:25vh; background-color:rgba(255, 255, 255, 0.61); border-radius:10px;">
                
                <p class=" font-weight-bolder text-center text-success"> <i class="fa fa-user-circle" aria-hidden="true"></i> Login</p>

               <form method="POST" action="{{ route('login') }}">
                       {{ csrf_field() }}

                       <div class="form-group">
                         <div class="col-md-12">
                           @if(session()->has('login_error'))
                               <div class="alert alert-danger">
                                       <button type="button" class="close" data-dismiss="alert">×</button>
                               {{ session()->get('login_error') }}
                               </div>
                       </div>
                   </div>
                     @endif

                       <div class="form-group{{ $errors->has('identity') ? ' has-error' : '' }} mt-2 mb-4">
                        

                           <div class="col-md-12">
                               <input id="identity" type="identity" class="form-control @error('identity') is-invalid @enderror" name="identity" placeholder="Enter your username or email" value="{{ old('identity') }}"  autocomplete="email" autofocus>

                               @if ($errors->has('identity'))
                                   <span class="help-block text-danger" role="alert">
                                       <strong class="text-danger">{{ $errors->first('identity') }}</strong>
                                   </span>
                               @endif

                           </div>
                       </div>

                       <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                      
                           <div class="col-md-12">
                               <input id="password" type="password" class="form-control border-right-0 @error('password') is-invalid @enderror" name="password" placeholder="Enter your Password"  autocomplete="current-password" data-toggle="password">

                               @if ($errors->has('password'))
                                   <span class="help-block text-danger" role="alert">
                                       <strong class="htext-danger">{{ $errors->first('password') }}</strong>
                                   </span>
                                @endif

                           </div>
                       </div>

                       <div class="form-group">
                           <div class="col-md-12 offset-md-0">
                               <div class="form-check">
                                   <input class="form-check-input mt-2" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                   <label class="form-check-label muli small" for="remember">
                                       {{ __('Keep me logged in') }}
                                   </label>
                                   @if (Route::has('password.request'))
                                       <a class="btn btn-link text-success muli small float-right font-weight-bold"  href="{{ route('register') }}">
                                           {{ __('Register') }}
                                       </a>
                       @endif
                               </div>
                               
                           </div>
                           
                           
                       </div>

                       <div class="form-group mb-5">
                           <div class="col-md-12 offset-md-0">
                               <button type="submit" class="btn btn-primary muli btn-block">
                                   {{ __('Signin') }}
                               </button>

                           </div>
                           
                       </div>
                      
                   </form>
               </div>
              
           </div>
        <a class="text-center" href="{{route('contact')}}" id="myBtn" title="Contact admin"><i class="fa fa-comment" aria-hidden="true"></i></a>
         
      </div>

    
    </div>
</div >
@endsection
             