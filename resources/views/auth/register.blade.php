@extends('layouts.head')

@section('content')
  <div class="container-fluid">
    <div class="row justify-content-center" id="row">

      <div class="col-sm-12" id="board" style="height:100vh">
        <div class="row justify-content-end  m-0 mr-lg-5">
          <div class="col-sm-11 col-md-6 col-lg-4 col-xl-3 p-3 shadow pt-5" style="margin-top:15vh; background-color:rgba(255, 255, 255, 0.61); border-radius:10px;">    
              <p class=" font-weight-bolder text-center text-success"> <i class="fa fa-user-circle" aria-hidden="true"></i> Register</p>

              <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}
              
              
                <div class="form-group ">
                    <div class="col-md-12">
                    @if(session()->has('message'))
                      <div class="alert alert-success">
                          <button type="button" class="close" data-dismiss="alert">×</button>
                        {{ session()->get('message') }}
                      </div>
                    @endif
              </div>
            </div>

                <div class="form-group mb-3{{ $errors->has('firstname') ? ' has-error' : '' }}">
                  {{-- <label for="name" class="col-md-4 control-label">Name</label> --}}

                  <div class="col-md-12">
                    <input id="firstname" type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" required
                          autofocus placeholder="First Name">

                    @if ($errors->has('firstname'))
                      <span class="help-block text-dark">
                            <strong>{{ $errors->first('firstname') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>

                <div class="form-group mb-3{{ $errors->has('lastname') ? ' has-error' : '' }}">
                    {{-- <label for="name" class="col-md-4 control-label">Name</label> --}}

                    <div class="col-md-12">
                      <input id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" required
                        placeholder="Last Name"   >

                      @if ($errors->has('lastname'))
                        <span class="help-block text-danger">
                            <strong>{{ $errors->first('lastname') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>

                  <div class="form-group mb-3{{ $errors->has('email') ? ' has-error' : '' }}" >
                      {{-- <label for="email" class="col-md-4 control-label">E-Mail Address</label> --}}
  
                      <div class="col-md-12">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="email@email.com">
  
                        @if ($errors->has('email'))
                          <span class="help-block text-danger">
                               <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                      </div>
                    </div>
  

                <div class="form-group mb-3{{ $errors->has('username') ? ' has-error' : '' }}">
                  {{-- <label for="name" class="col-md-4 control-label">Username</label> --}}

                  <div class="col-md-12">
                    <input id="name" type="text" class="form-control " name="username" value="{{ old('username') }}" required
                      placeholder="Username" >

                    @if ($errors->has('username'))
                      <span class="help-block text-danger">
                          <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>

               
                <div class="form-group mb-3{{ $errors->has('password') ? ' has-error' : '' }}">
                  {{-- <label for="password" class="col-md-4 control-label">Password</label> --}}

                  <div class="col-md-12">
                    <input id="password" type="password" class="form-control border-right-0" name="password" required placeholder="Password" data-toggle="password">
                    {{-- <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span> --}}
                    @if ($errors->has('password'))
                      <span class="help-block text-danger">
                          <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>

                <div class="form-group mb-3">
                  {{-- <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label> --}}

                  <div class="col-md-12">
                    <input id="password-confirm" type="password" class="form-control " name="password_confirmation"
                          required placeholder="Confirm Password" >
                  </div>
                </div>
               

                <div class="form-group mb-3">
                  <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary btn-block">
                      Signup
                    </button>
                  </div>
                </div>
                
              <div class="form-group mb-4 text-center">
                <h6 class="font-weight-normal muli font-weight-bold">Already have an account? <a class="muli font-weight-bold text-success" href="{{ route('login') }}"> Log In</a></h6>
              </div>
          </form>
          </div>
        </div>
        <a class="text-center" href="{{route('contact')}}" id="myBtn" title="Contact admin"><i class="fa fa-comment" aria-hidden="true"></i></a>
      </div>
      </div>
    </div>

      
  @endsection