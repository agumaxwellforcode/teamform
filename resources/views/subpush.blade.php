@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">


            @if ($message = Session::get('success'))
                            
            <div class="alert alert-success col-sm-12 alert-block">

              <button type="button" class="close" data-dismiss="alert">×</button>

                <strong>{{ $message }}</strong>

            </div>

       @endif



        <div class="col-md-12">
            @if(session()->get('message'))
            <div class="alert alert-success">
              {{ session()->get('message') }}
            </div>
            @endif
            <div class="card">
                <div class="card-header font-weight-lighter"> 

                  <ul class="nav nav-pills float-left">
                    {{-- <li class="nav-item">
                      <a class="nav-link active" data-toggle="pill" href="#home">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " data-toggle="pill" href="#file">File</a>
                    </li> --}}
                    <li class="nav-item dropdown font-weight-bolder">
                      <a class="nav-link dropdown-toggle btn-primary font-weight-bolder" data-toggle="dropdown" href="#"> Add New <i class="fa fa-plus" aria-hidden="true"></i> &nbsp;</a>
                      <div class="dropdown-menu border border-primary">
                        {{-- <a class="dropdown-item" data-toggle="pill" href="#">Link 1</a> --}}
                        <a class="dropdown-item text-primary font-weight-bolder" data-toggle="pill" href="#file"> <i class="fa fa-pus" aria-hidden="true"></i>  File</a>
                        <a class="dropdown-item text-primary font-weight-bolder" href="{{route('subFolderLoad',$target->id)}}"><i class="fa fa-pus" aria-hidden="true"></i>  Folder</a>
                      </div>
                    </li>
                    {{-- <li class="nav-item">
                      <a class="btn btn-primary font-weight-bolder text-white" href="{{route('subFolderLoad',$target->id)}}"> <i class="fa fa-plus" aria-hidden="true"></i> Create Folder</a>

                    </li> --}}
                  </ul>

                   
                        <div class="float-right">
                        <a onclick="goBack()" class="btn btn-default text-danger"> Back</a>
                        <a href="{{ route('home') }}" class="btn btn-default text-primary"> <i class="fa fa-home" aria-hidden="true"></i> Home</a>
                    </div>  
                </div>









            <div class="tab-content ">
              <div class="tab-pane container-fluid p-0" id="file">
                <div class="card-body p-0">
                                  {{-- {{dd($target->path)}} --}}

                        @php
                           $folder= $target->id;
                           $path= $target->path;
                        @endphp

                        <form action="{{ route('subfile.upload') }}" class="dropzone  bg-white border-1 border-light text-primary justify-content-center mb-0" style="border-radius:6px; height:400px;" id="myDropzone" method="POST" name="myDropzone" enctype="multipart/form-data">
                            @csrf

                            <input type="hidden" id="permision" name="permision" value="all">
                            <input type="hidden" id="folder" name="folder" value="{{$target->id}}">
                            <input type="hidden" id="path" name="path" value="{{$target->path}}">
                            <div class="fallback">
                           
                              <input name="file" type="file" multiple />
                              <div class="form-group mb-0">
                                    <div class=" ">
                                        <button type="submit" id="submit-all" class="btn btn-primary btn-block">
                                            {{ __('Upload') }}
                                        </button>
                                    </div>
                                </div>
                            </div>

                           


                            {{-- <div class="form-group row justify-content-center mt-5">
                               
                              <select class="form-control col-sm-8 col-md-2 col-lg-2 font-weight-light text-danger text-center border-0" id="permision" name="permision">
                                  <option value="#">Who has access to this file?</option>
                                  <option value="all">Who has access to this file</option>
                                  <option value="all">Everyone</option>
                                  <option value="me">Me Only</option>
                                  <option value="admin">Admin Only</option>
                              </select>
                          </div> --}}
                          </form>

            </div>
          </div>
       
       
          <div class="tab-pane container-fluid active" id="home">




           
        
        {{-- <small class=" text-primary "> {{$target->path}} </small> --}}
        <br>
        <div class="row  p-1">
            @foreach ($afolders as $afolder)
               <div class="col-sm-1 m-2 pt-3 bg-white text-center ">
                <a href="{{route('subhome',$afolder)}}" class="{{$afolder->id}}"><img class="img img-fluid rounded" src="/images/icons/folder.svg" alt="" srcset=""></a>
               <p class="text-center" style="font-size:10px; margin-top:5px;">{{$afolder->name}}</p>


                      <div class="dropdown-menu dropdown-menu-sm bg-light" id="{{$afolder->id}}" style="min-width:auto !important;">
                        <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('subhome',$afolder)}}">Open </a>
                        {{-- <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('share',$privateFolder)}}">Share <i class="fa fa-share-alt" aria-hidden="true"></i></a> --}}
                        <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-danger" href="{{route('deleteFolder',$afolder)}}">Delete <i class="fa fa-trash" aria-hidden="true"></i></a></a>
                      </div>
                      <script>

                        $('.{{$afolder->id}}').on('contextmenu', function(e) {
                        
                          $('#{{$afolder->id}}').css({
                            display: "block",
                          
                          }).addClass("show");
                          return false; //blocks default Webbrowser right click menu

                        })
                        $('.row').on("click", function() {
                            $('#{{$afolder->id}}').removeClass("show").hide();
                        });
                        $('.row').on("contextmenu", function() {
                                $('#{{$afolder->id}}').removeClass("show").hide();
                            });
                        
                        $('#{{$afolder->id}} a').on("click", function() {
                          $(this).parent().removeClass("show").hide();
                        });
                        
                    </script>


               </div>
            @endforeach
            
        </div> <hr>
        <div class="row  p-1 mt-4 mb-4">
          
                @foreach ($files as $file)
                            <div class="col-sm-1 pt-2 pb-2  m-2   text-center ">
                               


                               {{-- {{ dd($file)}} --}} 

                                @if(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='docx' || pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='DOCX' )
                                {{-- {{dd($file)}} --}}
                                <a href="{{route('subtext',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" style="object-fit: cover; height:auto; width:auto" src="/images/icons/docs.svg" alt="" srcset=""></a>
                                <p class="text-center" style="font-size:10px; margin-top:5px;">{{str_limit($file->filename, '17')}}</p>
                                

                                <div class="dropdown-menu dropdown-menu-sm m-0 mb-3" id="{{$file->id}}">
                                    <a class="dropdown-item pl-3 pr-3 font-weight-bolder" href="{{route('subtext',$file)}}">Open</a>
                                    <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('subshare',$file)}}">Share <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                    <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark"  href="{{route('deleteSubFile',$file)}}">Delete <i class="fa fa-trash" aria-hidden="true"></i></a>
                                  </div>
                                  <script>
                
                                    $('.{{$file->id}}').on('contextmenu', function(e) {
                                     
                                      $('#{{$file->id}}').css({
                                        display: "block",
                                       
                                      }).addClass("show");
                                      return false; //blocks default Webbrowser right click menu
                
                                    })
                                    $('.row').on("click", function() {
                                        $('#{{$file->id}}').removeClass("show").hide();
                                    });

                                    $('.row').on("contextmenu", function() {
                                        $('#{{$file->id}}').removeClass("show").hide();
                                    });
                                    
                                    
                                    $('#{{$file->id}} a').on("click", function() {
                                      $(this).parent().removeClass("show").hide();
                                    });
                                    
                                </script>


                                        @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='doc' || pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='DOC')
                                        {{-- {{dd($file)}} --}}
                                        <a href="{{route('subtext',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" style="object-fit: cover; height:auto; width:auto" src="/images/icons/docs.svg" alt="" srcset=""></a>
                                        <p class="text-center" style="font-size:10px; margin-top:5px;">{{str_limit($file->filename, '17')}}</p>


                                        <div class="dropdown-menu dropdown-menu-sm m-0 mb-3" id="{{$file->id}}">
                                            <a class="dropdown-item pl-3 pr-3 font-weight-bolder" href="{{route('subtext',$file)}}">Open</a>
                                            <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('subshare',$file)}}">Share <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                            <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark"  href="{{route('deleteSubFile',$file)}}">Delete <i class="fa fa-trash" aria-hidden="true"></i></a>
                                          </div>
                                          <script>

                                            $('.{{$file->id}}').on('contextmenu', function(e) {
                                            
                                              $('#{{$file->id}}').css({
                                                display: "block",
                                              
                                              }).addClass("show");
                                              return false; //blocks default Webbrowser right click menu

                                            })
                                            $('.row').on("click", function() {
                                                $('#{{$file->id}}').removeClass("show").hide();
                                            });

                                            $('.row').on("contextmenu", function() {
                                                $('#{{$file->id}}').removeClass("show").hide();
                                            });
                                            
                                            
                                            $('#{{$file->id}} a').on("click", function() {
                                              $(this).parent().removeClass("show").hide();
                                            });
                                            
                                        </script>
                              
                               



                                @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='PNG')
                                <a href="{{route('subimage',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" style="object-fit: cover; height:auto; width:auto"   src="{{url("storage/$target->path/$file->filename")}}" alt="" srcset=""></a>
                          
                                <p class="text-center" style="font-size:10px; margin-top:5px;">{{str_limit($file->filename, '17')}}</p>

                           

                            <div class="dropdown-menu dropdown-menu-sm m-0 mb-3" id="{{$file->id}}">
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder" href="{{route('subimage',$file)}}">Open</a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('subshare',$file)}}">Share <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                    <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('deleteSubFile',$file)}}">Delete  <i class="fa fa-trash" aria-hidden="true"></i></a>
                              </div>
                              <script>
            
                                $('.{{$file->id}}').on('contextmenu', function(e) {
                                 
                                  $('#{{$file->id}}').css({
                                    display: "block",
                                   
                                  }).addClass("show");
                                  return false; //blocks default Webbrowser right click menu
            
                                })
                                $('.row').on("click", function() {
                                    $('#{{$file->id}}').removeClass("show").hide();
                                });
                                $('.row').on("contextmenu", function() {
                                        $('#{{$file->id}}').removeClass("show").hide();
                                    });
                                
                                $('#{{$file->id}} a').on("click", function() {
                                  $(this).parent().removeClass("show").hide();
                                });
                                
                            </script>
                          
                           



                                @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='png')
                                <a href="{{route('subimage',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" style="object-fit: cover; height:auto; width:auto"   src="{{url("storage/$target->path/$file->filename")}}" alt="" srcset=""></a>
                           <p class="text-center" style="font-size:10px; margin-top:5px;">{{str_limit($file->filename, '17')}}</p>


                            <div class="dropdown-menu dropdown-menu-sm m-0 mb-3" id="{{$file->id}}">
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder" href="{{route('subimage',$file)}}">Open</a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('subshare',$file)}}">Share <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('deleteSubFile',$file)}}">Delete  <i class="fa fa-trash" aria-hidden="true"></i></a>
                              </div>
                              <script>
            
                                $('.{{$file->id}}').on('contextmenu', function(e) {
                                 
                                  $('#{{$file->id}}').css({
                                    display: "block",
                                   
                                  }).addClass("show");
                                  return false; //blocks default Webbrowser right click menu
            
                                })
                                $('.row').on("click", function() {
                                    $('#{{$file->id}}').removeClass("show").hide();
                                });
                                $('.row').on("contextmenu", function() {
                                        $('#{{$file->id}}').removeClass("show").hide();
                                    });
                                
                                $('#{{$file->id}} a').on("click", function() {
                                  $(this).parent().removeClass("show").hide();
                                });
                                
                            </script>
                          
                           


                                @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='jpg')
                                <a href="{{route('subimage',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" style="object-fit: cover;height:auto; width:auto"  src="{{url("storage/$target->path/$file->filename")}}" alt="" srcset=""></a>
                           <p class="text-center" style="font-size:10px; margin-top:5px;">{{str_limit($file->filename, '17')}}</p>


                            <div class="dropdown-menu dropdown-menu-sm m-0 mb-3" id="{{$file->id}}">
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder" href="{{route('subimage',$file)}}">Open</a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('subshare',$file)}}">Share <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                    <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('deleteSubFile',$file)}}">Delete  <i class="fa fa-trash" aria-hidden="true"></i></a>
                              </div>
                              <script>
            
                                $('.{{$file->id}}').on('contextmenu', function(e) {
                                 
                                  $('#{{$file->id}}').css({
                                    display: "block",
                                   
                                  }).addClass("show");
                                  return false; //blocks default Webbrowser right click menu
            
                                })
                                $('.row').on("click", function() {
                                    $('#{{$file->id}}').removeClass("show").hide();
                                });
                                $('.row').on("contextmenu", function() {
                                        $('#{{$file->id}}').removeClass("show").hide();
                                    });
                                
                                $('#{{$file->id}} a').on("click", function() {
                                  $(this).parent().removeClass("show").hide();
                                });
                                
                            </script>
                          
                           


                                @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='JPG')
                                <a href="{{route('subimage',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" style="object-fit: cover; height:auto; width:auto"    src="{{url("storage/$target->path/$file->filename")}}" alt="" srcset=""></a>
                           <p class="text-center" style="font-size:10px; margin-top:5px;">{{str_limit($file->filename, '17')}}</p>


                            <div class="dropdown-menu dropdown-menu-sm m-0 mb-3" id="{{$file->id}}">
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder" href="{{route('subimage',$file)}}">Open</a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('subshare',$file)}}">Share <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('deleteSubFile',$file)}}">Delete  <i class="fa fa-trash" aria-hidden="true"></i></a>
                              </div>
                              <script>
            
                                $('.{{$file->id}}').on('contextmenu', function(e) {
                                 
                                  $('#{{$file->id}}').css({
                                    display: "block",
                                   
                                  }).addClass("show");
                                  return false; //blocks default Webbrowser right click menu
            
                                })
                                $('.row').on("click", function() {
                                    $('#{{$file->id}}').removeClass("show").hide();
                                });
                                $('.row').on("contextmenu", function() {
                                        $('#{{$file->id}}').removeClass("show").hide();
                                    });
                                
                                $('#{{$file->id}} a').on("click", function() {
                                  $(this).parent().removeClass("show").hide();
                                });
                                
                            </script>
                          
                           


                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='JPEG')
                            <a href="{{route('subimage',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" style="object-fit: cover; height:auto; width:auto"   src="{{url("storage/$target->path/$file->filename")}}" alt="" srcset=""></a>
                            <p class="text-center" style="font-size:10px; margin-top:5px;">{{str_limit($file->filename, '17')}}</p>


                            <div class="dropdown-menu dropdown-menu-sm m-0 mb-3" id="{{$file->id}}">
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder" href="{{route('subimage',$file)}}">Open</a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('subshare',$file)}}">Share <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('deleteSubFile',$file)}}">Delete  <i class="fa fa-trash" aria-hidden="true"></i></a>
                              </div>
                              <script>
            
                                $('.{{$file->id}}').on('contextmenu', function(e) {
                                 
                                  $('#{{$file->id}}').css({
                                    display: "block",
                                   
                                  }).addClass("show");
                                  return false; //blocks default Webbrowser right click menu
            
                                })
                                $('.row').on("click", function() {
                                    $('#{{$file->id}}').removeClass("show").hide();
                                });
                                $('.row').on("contextmenu", function() {
                                        $('#{{$file->id}}').removeClass("show").hide();
                                    });
                                
                                $('#{{$file->id}} a').on("click", function() {
                                  $(this).parent().removeClass("show").hide();
                                });
                                
                            </script>
                          
                           


                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='jpeg')
                            <a href="{{route('subimage',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" style="object-fit: cover; height:auto; width:auto"   src="{{url("storage/$target->path/$file->filename")}}" alt="" srcset=""></a>
                            <p class="text-center" style="font-size:10px; margin-top:5px;">{{str_limit($file->filename, '17')}}</p>



                            <div class="dropdown-menu dropdown-menu-sm m-0 mb-3" id="{{$file->id}}">
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder" href="{{route('subimage',$file)}}">Open</a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('subshare',$file)}}">Share <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('deleteSubFile',$file)}}">Delete  <i class="fa fa-trash" aria-hidden="true"></i></a>
                              </div>
                              <script>
            
                                $('.{{$file->id}}').on('contextmenu', function(e) {
                                 
                                  $('#{{$file->id}}').css({
                                    display: "block",
                                   
                                  }).addClass("show");
                                  return false; //blocks default Webbrowser right click menu
            
                                })
                                $('.row').on("click", function() {
                                    $('#{{$file->id}}').removeClass("show").hide();
                                });
                                $('.row').on("contextmenu", function() {
                                        $('#{{$file->id}}').removeClass("show").hide();
                                    });
                                
                                $('#{{$file->id}} a').on("click", function() {
                                  $(this).parent().removeClass("show").hide();
                                });
                                
                            </script>
                          
                           


                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='pdf' || pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='PDF')
                            <a href="{{route('subpdf',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" src="/images/icons/pdf.svg" alt="" srcset="" ></a>
                            @php 
                              //  $value = $file->filename;
                            @endphp
                            {{-- <p class="text-center" style="font-size:10px; margin-top:5px;">{{str_limit($value, '16')}}</p> --}}
                            <p class="text-center" style="font-size:10px; margin-top:5px;">{{str_limit($file->filename, '17')}}</p>


                            <div class="dropdown-menu dropdown-menu-sm m-0 mb-3" id="{{$file->id}}">
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder" href="{{route('subpdf',$file)}}">Open</a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('subshare',$file)}}">Share <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                    <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('deleteSubFile',$file)}}">Delete  <i class="fa fa-trash" aria-hidden="true"></i></a>
                              </div>
                              <script>
            
                                $('.{{$file->id}}').on('contextmenu', function(e) {
                                 
                                  $('#{{$file->id}}').css({
                                    display: "block",
                                   
                                  }).addClass("show");
                                  return false; //blocks default Webbrowser right click menu
            
                                })
                                $('.row').on("click", function() {
                                    $('#{{$file->id}}').removeClass("show").hide();
                                });
                                $('.row').on("contextmenu", function() {
                                        $('#{{$file->id}}').removeClass("show").hide();
                                    });
                                
                                $('#{{$file->id}} a').on("click", function() {
                                  $(this).parent().removeClass("show").hide();
                                });
                                
                            </script>
                          
                           


                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='mp3' || pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='MP3')
                            <a href="{{route('subaudio',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" src="/images/icons/mp3.svg" alt="" srcset="" ></a>
                            <p class="text-center" style="font-size:10px; margin-top:5px;">{{str_limit($file->filename, '17')}}</p>


                            <div class="dropdown-menu dropdown-menu-sm m-0 mb-3" id="{{$file->id}}">
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder" href="{{route('subaudio',$file)}}">Open</a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('subshare',$file)}}">Share <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                    <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('deleteSubFile',$file)}}">Delete  <i class="fa fa-trash" aria-hidden="true"></i></a>
                              </div>
                              <script>
            
                                $('.{{$file->id}}').on('contextmenu', function(e) {
                                 
                                  $('#{{$file->id}}').css({
                                    display: "block",
                                   
                                  }).addClass("show");
                                  return false; //blocks default Webbrowser right click menu
            
                                })
                                $('.row').on("click", function() {
                                    $('#{{$file->id}}').removeClass("show").hide();
                                });
                                $('.row').on("contextmenu", function() {
                                        $('#{{$file->id}}').removeClass("show").hide();
                                    });
                                
                                $('#{{$file->id}} a').on("click", function() {
                                  $(this).parent().removeClass("show").hide();
                                });
                                
                            </script>
                          
                           


                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='csv' || pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='CSV')
                            <a href="{{route('subsheet',$file)}}"class="{{$file->id}}"><img class="img img-fluid rounded" src="/images/icons/sheets.svg" alt="" srcset="" ></a>
                            <p class="text-center" style="font-size:10px; margin-top:5px;">{{str_limit($file->filename, '17')}}</p>


                            <div class="dropdown-menu dropdown-menu-sm m-0 mb-3" id="{{$file->id}}">
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder" href="{{route('subsheet',$file)}}">Open</a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('share',$file)}}">Share <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('deleteSubFile',$file)}}">Delete  <i class="fa fa-trash" aria-hidden="true"></i></a>
                              </div>
                              <script>
            
                                $('.{{$file->id}}').on('contextmenu', function(e) {
                                 
                                  $('#{{$file->id}}').css({
                                    display: "block",
                                   
                                  }).addClass("show");
                                  return false; //blocks default Webbrowser right click menu
            
                                })
                                $('.row').on("click", function() {
                                    $('#{{$file->id}}').removeClass("show").hide();
                                });
                                $('.row').on("contextmenu", function() {
                                        $('#{{$file->id}}').removeClass("show").hide();
                                    });
                                
                                $('#{{$file->id}} a').on("click", function() {
                                  $(this).parent().removeClass("show").hide();
                                });
                                
                            </script>
                          
                           


                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='xlsx' || pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='XLSX')
                            <a href="{{route('subsheet',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" src="/images/icons/sheets.svg" alt="" srcset="" ></a>
                            <p class="text-center" style="font-size:10px; margin-top:5px;">{{str_limit($file->filename, '17')}}</p>


                            <div class="dropdown-menu dropdown-menu-sm m-0 mb-3" id="{{$file->id}}">
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder" href="{{route('subsheet',$file)}}">Open</a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('share',$file)}}">Share <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('deleteSubFile',$file)}}">Delete  <i class="fa fa-trash" aria-hidden="true"></i></a>
                              </div>
                              <script>
            
                                $('.{{$file->id}}').on('contextmenu', function(e) {
                                 
                                  $('#{{$file->id}}').css({
                                    display: "block",
                                   
                                  }).addClass("show");
                                  return false; //blocks default Webbrowser right click menu
            
                                })
                                $('.row').on("click", function() {
                                    $('#{{$file->id}}').removeClass("show").hide();
                                });

                                $('.row').on("contextmenu", function() {
                                        $('#{{$file->id}}').removeClass("show").hide();
                                    });
                                
                                $('#{{$file->id}} a').on("click", function() {
                                  $(this).parent().removeClass("show").hide();
                                });
                                
                            </script>
                          
                           


                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='html' || pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='HTML')
                            <a href="{{route('subtext',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" src="/images/icons/html.svg" alt="" srcset="" ></a>
                            <p class="text-center" style="font-size:10px; margin-top:5px;">{{str_limit($file->filename, '17')}}</p>
                            

                            <div class="dropdown-menu dropdown-menu-sm m-0 mb-3" id="{{$file->id}}">
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder" href="{{route('subtext',$file)}}">Open</a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('share',$file)}}">Share <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('deleteSubFile',$file)}}">Delete  <i class="fa fa-trash" aria-hidden="true"></i></a>
                              </div>
                              <script>
            
                                $('.{{$file->id}}').on('contextmenu', function(e) {
                                 
                                  $('#{{$file->id}}').css({
                                    display: "block",
                                   
                                  }).addClass("show");
                                  return false; //blocks default Webbrowser right click menu
            
                                })
                                $('.row').on("click", function() {
                                    $('#{{$file->id}}').removeClass("show").hide();
                                });
                                $('.row').on("contextmenu", function() {
                                        $('#{{$file->id}}').removeClass("show").hide();
                                    });
                                
                                $('#{{$file->id}} a').on("click", function() {
                                  $(this).parent().removeClass("show").hide();
                                });
                                
                            </script>
                          
                           



                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='pptx' || pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='PPTX' || pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='ppt' || pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='PPT')
                            <a href="{{route('subslide',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" src="/images/icons/slides.svg" alt="" srcset="" ></a>
                            <p class="text-center" style="font-size:10px; margin-top:5px;">{{str_limit($file->filename, '17')}}</p>


                            <div class="dropdown-menu dropdown-menu-sm m-0 mb-3" id="{{$file->id}}">
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder" href="{{route('subslide',$file)}}">Open</a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('share',$file)}}">Share <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('deleteSubFile',$file)}}">Delete  <i class="fa fa-trash" aria-hidden="true"></i></a>
                              </div>
                              <script>
            
                                $('.{{$file->id}}').on('contextmenu', function(e) {
                                 
                                  $('#{{$file->id}}').css({
                                    display: "block",
                                   
                                  }).addClass("show");
                                  return false; //blocks default Webbrowser right click menu
            
                                })
                                $('.row').on("click", function() {
                                    $('#{{$file->id}}').removeClass("show").hide();
                                });
                                $('.row').on("contextmenu", function() {
                                        $('#{{$file->id}}').removeClass("show").hide();
                                    });
                                
                                $('#{{$file->id}} a').on("click", function() {
                                  $(this).parent().removeClass("show").hide();
                                });
                                
                            </script>
                          
                           


                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='mp4' || pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='MP4')
                            <a href="{{route('subvideo',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" src="/images/icons/cone.svg" alt="" srcset="" ></a>
                            <p class="text-center" style="font-size:10px; margin-top:5px;">{{str_limit($file->filename, '17')}}</p>


                            <div class="dropdown-menu dropdown-menu-sm m-0 mb-3" id="{{$file->id}}">
                                <a class="dropdown-item  pl-3 pr-3 font-weight-bolder" href="{{route('subvideo',$file)}}">Open</a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('subshare',$file)}}">Share <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                    <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('deleteSubFile',$file)}}">Delete  <i class="fa fa-trash" aria-hidden="true"></i></a>
                              </div>
                              <script>
            
                                $('.{{$file->id}}').on('contextmenu', function(e) {
                                 
                                  $('#{{$file->id}}').css({
                                    display: "block",
                                   
                                  }).addClass("show");
                                  return false; //blocks default Webbrowser right click menu
            
                                })
                                $('.row').on("click", function() {
                                    $('#{{$file->id}}').removeClass("show").hide();
                                });
                                $('.row').on("contextmenu", function() {
                                        $('#{{$file->id}}').removeClass("show").hide();
                                    });
                                
                                $('#{{$file->id}} a').on("click", function() {
                                  $(this).parent().removeClass("show").hide();
                                });
                                
                            </script>
                          
                           


                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='zip' || pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='ZIP')
                              <a href="{{route('subpdf',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" src="/images/icons/zip.svg" alt="" srcset=""  ></a>
                              <p class="text-center" style="font-size:10px; margin-top:5px;">{{str_limit($file->filename, '17')}}</p>


                              <div class="dropdown-menu dropdown-menu-sm m-0 mb-3" id="{{$file->id}}">
                                <a class="dropdown-item  pl-3 pr-3 font-weight-bolder" href="{{route('subpdf',$file)}}">Open</a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('share',$file)}}">Share <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('deleteSubFile',$file)}}">Delete  <i class="fa fa-trash" aria-hidden="true"></i></a>
                              </div>
                              <script>
            
                                $('.{{$file->id}}').on('contextmenu', function(e) {
                                 
                                  $('#{{$file->id}}').css({
                                    display: "block",
                                   
                                  }).addClass("show");
                                  return false; //blocks default Webbrowser right click menu
            
                                })
                                $('.row').on("click", function() {
                                    $('#{{$file->id}}').removeClass("show").hide();
                                });
                                $('.row').on("contextmenu", function() {
                                        $('#{{$file->id}}').removeClass("show").hide();
                                    });
                                
                                $('#{{$file->id}} a').on("click", function() {
                                  $(this).parent().removeClass("show").hide();
                                });
                                
                            </script>
                          
                           


                              @elseif(pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='rar' || pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION) =='RAR')
                              <a href="{{route('subpdf',$file)}}" class="{{$file->id}}"><img class="img img-fluid rounded" src="/images/icons/zip.svg" alt="" srcset=""  ></a>
                              <p class="text-center" style="font-size:10px; margin-top:5px;">{{str_limit($file->filename, '17')}}</p>
                           

                              <div class="dropdown-menu dropdown-menu-sm m-0 p-0" id="{{$file->id}}">
                                <a class="dropdown-item  pl-3 pr-3 font-weight-bolder" href="{{route('subpdf',$file)}}">Open</a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('share',$file)}}">Share <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('deleteSubFile',$file)}}">Delete  <i class="fa fa-trash" aria-hidden="true"></i></a>
                              </div>
                              <script>
            
                                $('.{{$file->id}}').on('contextmenu', function(e) {
                                 
                                  $('#{{$file->id}}').css({
                                    display: "block",
                                   
                                  }).addClass("show");
                                  return false; //blocks default Webbrowser right click menu
            
                                })
                                $('.row').on("click", function() {
                                    $('#{{$file->id}}').removeClass("show").hide();
                                });
                                $('.row').on("contextmenu", function() {
                                        $('#{{$file->id}}').removeClass("show").hide();
                                    });
                                
                                $('#{{$file->id}} a').on("click", function() {
                                  $(this).parent().removeClass("show").hide();
                                });
                                
                            </script>
                          
                           
                              @endif




















                              
                </div>
                @endforeach
                
               
              </div>
          </div>
    </div>
</div>
{{-- {{dd($target->path)}} --}}

<script>

 
    //    Dropzone.autoDiscover = false;
    Dropzone.options.myDropzone = {
                 url:"{{ route('subfile.upload') }}",
                 method:"POST",
                 paramName: "file", // The name that will be used to transfer the file
                 dictDefaultMessage: "Drag and drop files here or click to upload...",
                 acceptedFiles:"audio/*,image/*,.mp3,.wav,.mp4,.psd,.pdf,video/*,.xls,.xlsx,.csv,.txt,.doc,.docx,.tiff,.eps,.svg,.xd,.ai,.psd,.ppt,.pptx",
                 maxFilesize: 10240000, // MB
                 addRemoveLinks: true,
                 
                 init: function() {
                     var myDropzone = this;
                     this.on("sending", function(file, xhr, formData) {
                        // Will send the filesize along with the file as POST data.
                        formData.append("folder", {{$target->id}});
                        formData.append("path",  '{{$target->path}}');
                        formData.append("permission",  $('#permision').val());
                        });
                    //  this.on('sending', function(file, xhr, formData){
                                
                    //                  formData.append('folder', {{$target->id}});
                    //                  formData.append('path', '{{$target->path}}');
                                   
                    //           });
                    //           console.log("formData")
                     this.on("uploadprogress", function(file, progress) {
                         console.log("File progress", progress);
                     });

                     this.on('queuecomplete', function () {
                        location.reload();
                     });
                     },
                    

                
                             
                 };
                 
                 
 
 
 
 
 
   </script>
 
 <script>
  function goBack() {
    window.history.back();
  }
  </script>
 
 <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
@endsection

{{-- @section('scripts')
  

@endsection --}}




{{-- {{dd($exts)}} --}}
                        {{-- <embed name="plugin" src="{{url('')}}{{ Storage::disk('public')->url("files/$file->filename")}}" type="application/pdf"> --}}
                        {{-- <iframe src="{{Storage::url("files/$file->filename")}}" width="100%" height="600"></iframe> --}}