<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Team form</title>


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/css/bootstrap.min.css" integrity="sha384-SI27wrMjH3ZZ89r4o+fGIJtnzkAnFs3E4qz9DIYioCQ5l9Rd/7UAa8DHcaL8jkWt" crossorigin="anonymous">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
	 <script src="https://unpkg.com/bootstrap-show-password@1.2.1/dist/bootstrap-show-password.min.js"></script> 
    <script src="https://kit.fontawesome.com/bf6c353baa.js" crossorigin="anonymous"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('fontawesome/js/all.min.js') }}" crossorigin="anonymous"></script>


    <!-- MDB core JavaScript -->
    {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.11/js/mdb.min.js"></script> --}}
         <script>
            $("#password").password('toggle');
        </script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link href="{{ asset('fontawesome/css/all.min.css') }}" rel="stylesheet">
    <!-- Styles -->
    <!-- Material Design Bootstrap -->
    {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.11/css/mdb.min.css" rel="stylesheet"> --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        .text-success{
            color: #009688 !important;
        }
        .bg-success  {
            background-color: #009688 !important;
        }
        .btn-success  {
            background-color: #009688 !important;
        }
        .btn-primary  {
            background-color: #009688 !important;
        }
        .btn-outline-primary  {
            border-color: #009688 !important;
        }
        .border-primary, .border-success  {
            border-color: #009688 !important;
        }
        #board{
            background-image:url("/images/medicine.svg");
            background-repeat: no-repeat, repeat;
            background-color: #fff;
            bottom: 0%;
          
        }
        #boardc{
            background-image:url("/images/contact.svg");
            background-repeat: repeat;
            background-color: #fff;
            bottom: 0%;
          
        }
        #img{
            position: relative;
            top: 50%;
            transform: translateY(-50%);

        }
        html, body, {
            font-weight: bold !important;
            font-family: 'Poppins', sans-serif !important;
            height: 100vh;
            overflow: auto;
            bottom:0;
            background-color: #fff !important;
        }
        #row{
            bottom:0;
        }
        .nav-link{
            font-weight:normal !important;
            color: black !important;
            font-family: 'Poppins', sans-serif;
        }
        .bold{
            font-weight: bolder !important;
        }
        ul{
            list-style-type: none;
            padding: 0px;
            margin: 0px;
        }
        .lst{
            float: left !important;
        }
        .Active-tab{
            border-color:green !important;;
        }
        .tab-link{
            text-decoration: none;
            padding: 10px;
            padding-bottom: 5px;
            color: black;
            border-bottom: 4px solid transparent;
            border-radius: 1px;
            font-family: 'Poppins', sans-serif;
            width: auto !important;
        }
        .tab-link:hover{
            color: green;
            text-decoration: none;
        }
        .nav-link:hover{
            color: green!important;
        }
        .form-control{
            border-width: 2px !important;
            border-radius: 2px !important;
            padding: 8px !important;
            font-family: 'Roboto', sans-serif;
            color: black;
            font-weight: normal;

        }
        .roboto{
            font-family: 'Roboto', sans-serif !important;
        }
        ..poppins{
            font-family: 'Poppins', sans-serif !important;
        }
        .small{
            font-size: 12px !important;
        }

        .field-icon {
            float: right;
            margin-left: -25px;
            margin-top: -25px;
            position: relative;
            z-index: 2;
            }
            #board > div > div > form > div > div > div:nth-child(2) > div > div > span{
                color: black;
                font-weight: bolder !important;
                position: relative;
                padding-top: 10px;
                padding-left: 5px;
                border-right: 2px solid #ced4da !important;;
                border-left: 0px !important;
                border-top: 2px solid #ced4da !important;;
                border-bottom: 2px solid #ced4da !important;;
                border-top-right-radius: 2px;
                border-bottom-right-radius: 2px;
                padding-right: 8px !important;
                border: 1px solid #ced4da;
            }
            #board > div > div > form > div:nth-child(7) > div > div > span{
         color: black;
         font-weight: bolder !important;
         position: relative;
         padding-top: 10px;
         padding-left: 5px;
         border-right: 2px solid #ced4da !important;;
         border-left: 0px !important;
         border-top: 2px solid #ced4da !important;;
         border-bottom: 2px solid #ced4da !important;;
         border-top-right-radius: 2px;
         border-bottom-right-radius: 2px;
         padding-right: 8px !important;
         border: 1px solid #ced4da;
     }

     #row > div.col-sm-7.pr-0.pl-0 > div > div > form > div:nth-child(8) > div > div > span{
         color: black;
         font-weight: bolder !important;
         position: relative;
         padding-top: 11px;
         padding-left: 5px;
         border-right: 2px solid #ced4da !important;;
         border-left: 0px !important;
         border-top: 2px solid #ced4da !important;;
         border-bottom: 2px solid #ced4da !important;;
         border-top-right-radius: 2px;
         border-bottom-right-radius: 2px;
         padding-right: 8px !important;
         border: 1px solid #ced4da;
         
     }

     #row > div.col-sm-7.pr-0.pl-0 > div > div > form > div > div > div:nth-child(2) > div > div > span{
        color: black;
         font-weight: bolder !important;
         position: relative;
         padding-top: 11px;
         padding-left: 5px;
         border-right: 2px solid #ced4da !important;;
         border-left: 0px !important;
         border-top: 2px solid #ced4da !important;;
         border-bottom: 2px solid #ced4da !important;;
         border-top-right-radius: 2px;
         border-bottom-right-radius: 2px;
         padding-right: 8px !important;
         border: 1px solid #ced4da;
         
         
     }

     #row > div.col-sm-7.pr-0.pl-0 > div > div > form > div:nth-child(4) > div > div > span{
        color: black;
         font-weight: bolder !important;
         position: relative;
         padding-top: 11px;
         padding-left: 5px;
         border-right: 2px solid #ced4da !important;;
         border-left: 0px !important;
         border-top: 2px solid #ced4da !important;;
         border-bottom: 2px solid #ced4da !important;;
         border-top-right-radius: 2px;
         border-bottom-right-radius: 2px;
         padding-right: 8px !important;
         border: 1px solid #ced4da;

         
     }
     #myBtn {
        display: block; /* Hidden by default */
        position: fixed; /* Fixed/sticky position */
        bottom: 20px; /* Place the button at the bottom of the page */
        right: 20px; /* Place the button 30px from the right */
        z-index: 99; /* Make sure it does not overlap */
        border: none; /* Remove borders */
        outline: none; /* Remove outline */
        background-color: #009688; /* Set a background color */
        color: white; /* Text color */
        cursor: pointer; /* Add a mouse pointer on hover */
        padding: 15px; /* Some padding */
        border-radius: 50%; /* Rounded corners */
        font-size: 15px; /* Increase font size */
        text-decoration: none;
        
        border: 2px solid #009688;
        }

        #myBtn:hover {
        background-color: #fff; /* Add a dark-grey background on hover */
        border: 2px solid #009688;
        color: #009688;
       
        }
        
    </style>
</head>
<body >
    @include('sweetalert::alert')

            <div id="app" class="bg-white">


            
                    @yield('content')
     
            </div>


          
        </body>
        </html>
        