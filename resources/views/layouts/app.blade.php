<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Team form</title>

    <!-- Scripts -->
  
    <script src="https://code.jquery.com/jquery-3.3.1.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
  

    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.js"></script>
    {{-- <script src="https://kit.fontawesome.com/bf6c353baa.js" crossorigin="anonymous"></script> --}}

    <script src="{{ asset('fontawesome/js/all.min.js') }}" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    
 
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href=https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css >
    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> --}}
     {{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css"> --}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css">
 
    
    
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('fontawesome/css/all.min.css') }}" rel="stylesheet">
    
    <style>
      html, 
        body {
            height: 90% !important;
            font-family: 'Muli', sans-serif !important;
        }
        .text-success,.text-primary, .btn-default{
            color: #009688 !important;
        }
        .bg-success  {
            background-color: #009688 !important;
        }
        .btn-success  {
            background-color: #009688 !important;
        }
        .btn-primary  {
            background-color: #009688 !important;
        }
        .btn-outline-primary  {
            border-color: #009688 !important;
        }
        .border-primary, .border-success  {
            border-color: #009688 !important;
        }
        .slink:hover{
            text-decoration: none !important;
        }
        #searchTable{
                display: none;
                position: absolute;
                z-index: 1;
            }
        .notification {
            background-color: transparent;
            color: white;
            text-decoration: none;
            /* padding: 1px 1px; */
            /* margin-top: 5px; */
            font-size: 25px;
            position: relative;
            display: inline-block;
            border-radius: 2px;
            }

        .notification:hover {
            /* background: grey; */
            color: #00cab6;
            }

            #navbarSupportedContent > ul:nth-child(2) > li.nav-item.dropdown.pt-0.show > div > a:hover{
                color: #009688 !important;
                font-weight: bold;
            }

        .notification .badge {
            position: absolute;
            top: -1px;
            right: -8px;
            padding: 2px 5px;
            font-size: 10px;
            border-radius: 70%;
            background: red;
            color: white;
            }

            #example_filter > label{
                /* color: transparent !important; */
               
            }
            #myBtn1 {
                display: block; /* Hidden by default */
                position: fixed; /* Fixed/sticky position */
                bottom: 50%; /* Place the button at the bottom of the page */
                right: 20px; /* Place the button 30px from the right */
                z-index: 99; /* Make sure it does not overlap */
                border: none; /* Remove borders */
                outline: none; /* Remove outline */
                background-color: #009688; /* Set a background color */
                color: white; /* Text color */
                cursor: pointer; /* Add a mouse pointer on hover */
                padding: 15px; /* Some padding */
                padding-top: 13px !important; /* Some padding */
                padding-bottom: 13px !important; /* Some padding */
                border-radius: 50%; /* Rounded corners */
                font-size: 20px; /* Increase font size */
                text-decoration: none;
                
                border: 2px solid #009688;
                }

                #myBtn:hover {
                background-color: #fff; /* Add a dark-grey background on hover */
                border: 2px solid #009688;
                color: #009688;
            
                }

                #back {
                    display: block; /* Hidden by default */
                    position: fixed; /* Fixed/sticky position */
                    top: 90px; /* Place the button at the bottom of the page */
                    left: 20px; /* Place the button 30px from the right */
                    z-index: 99; /* Make sure it does not overlap */
                    border: none; /* Remove borders */
                    outline: none; /* Remove outline */
                    background-color: #009688; /* Set a background color */
                    color: white; /* Text color */
                    cursor: pointer; /* Add a mouse pointer on hover */
                    padding-top: 15px; /* Some padding */
                    padding-bottom: 15px; /* Some padding */
                    padding-left: 20px; /* Some padding */
                    padding-right: 20px; /* Some padding */
                    border-radius: 50%; /* Rounded corners */
                    font-size: 15px; /* Increase font size */
                    text-decoration: none;
                    
                    border: 2px solid #009688;
                    }

                    #back:hover {
                    background-color: #fff !important; /* Add a dark-grey background on hover */
                    border: 2px solid #009688 !important;
                    color: #009688 !important;
       
                    }
           
    </style>
</head>
<body class="w-100 bg-white " style="">
    @include('sweetalert::alert')


    <div id="app" class="bg-white" >
        <nav class="navbar navbar-expand-md navbar-dark fixed-top" style=" background-color:#009688;">
            <div class="container-fluid  ml-0 mr-0">
                <a class="navbar-brand p-0" href="{{ url('/') }}">
                    <img class="rounded-circle"  src="/images/logo1.jpeg" height="40px" width="50px" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        
                        
                            <li class="nav-item ">
                              <a class="nav-link {{ (request()->is('home','homelist','push','folder.create','folder.index','text/*','slide/*','pdf/*','audio/*','video/*','home/*','image/*','subhome/*','subtext/*','subslide/*','subpdf/* ','share/* ','exist/* ','subaudio/*','subvideo/*','subhome/*','subimage/*')) ? 'active' : '' }}" href="{{ route('home') }}"> <i class="fa fa-file" aria-hidden="true"></i> Files </a>
                            </li>
                        @can('manage-users')
                            <li class="nav-item">
                              <a class="nav-link {{ (request()->is('admin/*','inbox','quota','inbox','profile')) ? 'active' : '' }}" href="{{ route('admin.users.index') }}"> <i class="fa fa-cog" aria-hidden="true"></i> Settings/Management <span class="sr-only">(current)</span></a>
                            </li>
                        @endcan
                           
                          </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ">
                        <li class="nav-item">
                                
                         </li>
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link text-white" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                        



                            @php

                                $user = Auth::user();
                                $alerts = DB::table('notifications')
                                            ->where('recipient_user_id', '=',$user->id)
                                            ->where('seen', '=','0')
                                            ->get();

                                $alertf = DB::table('file_notifications')
                                            ->where('owner', '=',$user->id)
                                            ->where('seen', '=','0')
                                            ->get();
                            @endphp

                            @if (!$alerts->isEmpty() || !$alertf->isEmpty() )
                            
                            @php
                                $count = $alerts->count() + $alertf->count();
                             @endphp


                                <li  class="nav-item dropdown  pt-0 " >

                                    <a id="dropdownMenu2" href="#" class="notification mr-3 mt-1" href="#"role="button" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false" v-pre><span><i class="fa fa-bell" aria-hidden="true"></i></span><span class="badge">{{$count}}</span> </a>
                                   
                                    </a>
                                        <div class="dropdown-menu dropdown-menu-right border-white bg-success" aria-labelledby="dropdownMenu2">
                                          
                                        <a class="dropdown-item text-white" href="{{route('admin.notification.index')}}">                                                   
                                                   
                                                    <span class="badge badge-default ">{{$count}}</span> new notification
                                                </a>
                                           
                                        </div>
                                     </li>

                          @else
                          <li  class="nav-item dropdown  pt-1 " >

                            <a id="dropdownMenu2" href="#" class="notification mr-3" href="#"role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre><span><i class="fa fa-bell" aria-hidden="true"></i></span> </a>
                           
                            {{-- <a id="dropdownMenu2" class="nav-link text-danger dropdown-" href="#"role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre> <i class="fa fa-bell" aria-hidden="true"></i> --}}
                            </a>
                                <div class="dropdown-menu dropdown-menu-right border-primary" aria-labelledby="dropdownMenu2">
                                   
                                <a class="dropdown-item text-primary" href="{{route('admin.notification.index')}}">                                                   
                                            <i class="fa fa-bell" aria-hidden="true"></i> 
                                            No new notification
                                        </a>
                                   
                                </div>
                             </li>
                        @endif












                        
                        <li class="nav-item">
                                <a class="nav-link text-white" href="/profile"> <img class="rounded-circle" height="30px" width="30px" src="/storage/avatars/{{ Auth::user()->avatar }}" /></a>
                        </li>

                        <li class="nav-item dropdown">
                               
                                <a id="navbarDropdown" class="nav-link dropdown-toggle mt-1 text-white" href="/profile" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->lastname }}<span class="caret"></span>
                                </a>

                               
                               
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                   
                                    <a class="dropdown-item text-primary" href="/profile">
                                         
                                         <i class="fa fa-user" aria-hidden="true"></i> {{ __('Profile') }}
                                     </a>
                        
                                    <a class="dropdown-item text-primary" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                         <i class="fa fa-power-off" aria-hidden="true"></i> {{ __('Logout') }}
                                    </a>


                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>


                                    <a class="dropdown-item text-primary" href="{{ route('quota') }}">
                                        <i class="fa fa-chart-pie" aria-hidden="true"></i> My Quota
                                    </a>

                                    @can('manage-users')
                                        <a class="dropdown-item text-primary" href="{{ route('admin.users.index') }}">
                                                <i class="fa fa-cogs" aria-hidden="true"></i> Configuration
                                        </a>
                                    @endcan
                                </div>
                                
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        
        <main class="py-3" style=" margin-top:80px">
            @yield('content')
        </main>
    </div>
   
    </body>
</html>
