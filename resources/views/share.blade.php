@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card shadow-lg">
             
                <div class="card-header font-weight-bold bg-light text-success">Share With <i class="fa fa-share-alt" aria-hidden="true"></i> 
                    <div class="float-right">
                        <a onclick="goBack()" class="btn btn-default text-danger btn-sm">  <i class="fa fa-times" aria-hidden="true"></i> Cancel</a>
                    </div>
                </div>

                <div class="card-body pt-0">

                      <div class="row justify-content-center">
                          
                        @if ($message = Session::get('success'))
                              
                        <div class="alert alert-success alert-block">
        
                            <button type="button" class="close" data-dismiss="alert">×</button>
        
                            <strong>{{ $message }}</strong>
        
                        </div>

                  @endif
                        
                          <div class="custom-control custom-switch col-sm-11  m-4 text-center bg-white pl-1">

                            <form action="{{route('shareFile')}}" method="post">
                                @csrf
                                {{ method_field('POST') }}
                            <div class="" id="style-2">
                              <table id="example" class="table table-borderless table-hover">
                                   
                                  <thead>
                                      <tr class="border-bottom border-primary">
                                          
                                          <th class="border-bottom small text-success font-weight-bold"> Select All</th>
                                          <th class="border-bottom text-right"></th>
                                          <th class="text-right  pr-0 border-bottom ">
                                            <div class="custom-control custom-checkbox align-middle">
                                                <input type="checkbox" class="custom-control-input" id="select_all">
                                                <label class="custom-control-label" for="select_all"></label>
                                             </div>
                                          </th>
                                         
                                          
                                      </tr>
                                  </thead>
                                  <tbody >
                                      @php
                                         $sharer=Auth::user();
                                        //  dd($sharer->id);
                                      @endphp
                                      @foreach ($users as $user )

                                      @php
                                        // $sharer=Auth::user();
                                        //  dd($path);
                                     @endphp

                                          <tr >
                                  
                                                {{-- {{ implode(', ', $user->roles()->get()->pluck('avatar','firstname','lastname')->toArray()) }} --}}


                                            <td class="text-primary justify-content-center"><img class="rounded-circle" height="50px" width="50px" src="/storage/avatars/{{ $user->avatar }}" />   {{ implode(', ', $user->roles()->get()->pluck('firstname','lastname')->toArray()) }}</td>
                                            <td class="text-primary text-left align-middle justify-content-center"> {{ $user->firstname }}   {{ $user->lastname }}</td>
                                            <td class="text-right  pr-0 others">
                                               
                                              

                                                    <div class="custom-control custom-switch  align-middle mt-3">
                                                        <input type="checkbox" name="reciepient_id[]" class="custom-control-input others" id="{{$user->id}}" value="{{$user->id}}">                
                                                        <label class="custom-control-label" for="{{$user->id}}"></label>
                                                    </div>


                                                    <input type="hidden" name="sharer_id" value="{{$sharer->id}}">                            
                                                    <input type="hidden" name="file_id" value="{{$file_id}}">                            
                                                    <input type="hidden" name="filename" value="{{$filename}}">                            
                                                    <input type="hidden" name="folder_id" value="{{$folder_id}}">                            
                                                    <input type="hidden" name="path" value="{{$path}}">                            
                                                 
                                              </td>
                                           
                                          </tr>
                                        
                                        @endforeach
                                  </tbody>
                              </table>
                            </div>
                              <div class="row justify-content-center mt-4 ">
                                <button type="submit" class="btn btn-success p-2 text-center shadow" ><i class="fa fa-share-alt" aria-hidden="true"></i> Share file</button>
                               
                                {{-- <a href="{{route('admin.users.index')}}" class="btn btn-outline-default text-danger float-right">Cancel</a> --}}
                             
                              </div>
                            </form>
                              
                            </div>
                             
                        
                      </div>

                     
                </div>

                
            </div>
        </div>
    </div>
</div>

    <script>

$('.your-checkbox').prop('indeterminate', true)


     $(document).ready(function() {
    $('#example').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
     </script>

     <script>
     $(function() {
          $('#favoritesModal').on("show.bs.modal", function (e) {
              $("#favoritesModalLabel").html($(e.relatedTarget).data('title'));
              $("#fav-title").html($(e.relatedTarget).data('title'));
          });
      });

     
     </script>
     
<style>
    input{
        border:1px solid #00968859 !important;
        padding: 20px !important;
    }
    select{
        border:1px solid #00968859 !important;
        padding: 10px !important;
        min-height: 45px !important;
        min-width: 100px !important;
        color: black !important;
    }
    #example_filter > label > input{
      /* background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIgICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgICB2ZXJzaW9uPSIxLjEiICAgaWQ9InN2ZzQ0ODUiICAgdmlld0JveD0iMCAwIDIxLjk5OTk5OSAyMS45OTk5OTkiICAgaGVpZ2h0PSIyMiIgICB3aWR0aD0iMjIiPiAgPGRlZnMgICAgIGlkPSJkZWZzNDQ4NyIgLz4gIDxtZXRhZGF0YSAgICAgaWQ9Im1ldGFkYXRhNDQ5MCI+ICAgIDxyZGY6UkRGPiAgICAgIDxjYzpXb3JrICAgICAgICAgcmRmOmFib3V0PSIiPiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+ICAgICAgICA8ZGM6dHlwZSAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4gICAgICAgIDxkYzp0aXRsZT48L2RjOnRpdGxlPiAgICAgIDwvY2M6V29yaz4gICAgPC9yZGY6UkRGPiAgPC9tZXRhZGF0YT4gIDxnICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDMwLjM2MjIpIiAgICAgaWQ9ImxheWVyMSI+ICAgIDxnICAgICAgIHN0eWxlPSJvcGFjaXR5OjAuNSIgICAgICAgaWQ9ImcxNyIgICAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNjAuNCw4NjYuMjQxMzQpIj4gICAgICA8cGF0aCAgICAgICAgIGlkPSJwYXRoMTkiICAgICAgICAgZD0ibSAtNTAuNSwxNzkuMSBjIC0yLjcsMCAtNC45LC0yLjIgLTQuOSwtNC45IDAsLTIuNyAyLjIsLTQuOSA0LjksLTQuOSAyLjcsMCA0LjksMi4yIDQuOSw0LjkgMCwyLjcgLTIuMiw0LjkgLTQuOSw0LjkgeiBtIDAsLTguOCBjIC0yLjIsMCAtMy45LDEuNyAtMy45LDMuOSAwLDIuMiAxLjcsMy45IDMuOSwzLjkgMi4yLDAgMy45LC0xLjcgMy45LC0zLjkgMCwtMi4yIC0xLjcsLTMuOSAtMy45LC0zLjkgeiIgICAgICAgICBjbGFzcz0ic3Q0IiAvPiAgICAgIDxyZWN0ICAgICAgICAgaWQ9InJlY3QyMSIgICAgICAgICBoZWlnaHQ9IjUiICAgICAgICAgd2lkdGg9IjAuODk5OTk5OTgiICAgICAgICAgY2xhc3M9InN0NCIgICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjY5NjQsLTAuNzE3NiwwLjcxNzYsMC42OTY0LC0xNDIuMzkzOCwyMS41MDE1KSIgICAgICAgICB5PSIxNzYuNjAwMDEiICAgICAgICAgeD0iLTQ2LjIwMDAwMSIgLz4gICAgPC9nPiAgPC9nPjwvc3ZnPg==); */
      background-image: url(/images/icons/search.svg) !important;
      background-repeat: no-repeat !important;
      background-color: #fff !important;
      background-size: 18px;
      background-position: 167px 11px !important;
      font-weight: bold !important;
  }
  #example_filter > label{
    color: #fff !important;
  }
  #style-2::-webkit-scrollbar {
    height: 6px;
    background-color: #F5F5F5;
    
    } 
    #style-2::-webkit-scrollbar-thumb {
        background-color: #009688;
    }
    #style-2::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
    }
    #example tr:hover{
        background-color: #00968866 !important;
        color: white !important;
        border-radius: 4px !important;
        font-weight: bold;
    }
    /* .custom-checkbox .custom-control-input:checked ~ .custom-control-label::before {
        background-color: #000 !important;
    } */
    .custom-control-input:checked ~ .custom-control-label::before {
        color: #fff;
        background-color: #009688 !important;
    }
    
  </style>
 
  <script>
    function goBack() {
      window.history.back();
    }
    </script>
    <script>
        $('#select_all').change(function() {
            $('.others').prop("checked", $(this).is(':checked'));
        });

        $('.others').change(function() {
        if($(this).is(':checked')) 
            {     
            // check if all days checkbox checked then check select_all
            if($('.others').is(':checked').length == $('.others').length)
                $('#select_all').prop("checked", true); 
            }
            else
            {
                $('#select_all').prop("checked", false); 
            } 
        });

    
        
    </script>
@endsection
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

</body>
</html>