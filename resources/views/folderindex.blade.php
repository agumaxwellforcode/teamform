@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header font-weight-bold">
                    <!-- Example single danger button -->
                    <a class="btn btn-primary font-weight-bolder text-white" href="{{route('folder.create')}}"> <i class="fa fa-plus" aria-hidden="true"></i> Create Folder</a>

                        
                        <div class="float-right">
                            <a href="{{ route('home') }}" class="btn btn-default text-primary"> <i class="fa fa-home" aria-hidden="true"></i> Home</a>
                                {{-- <a href="{{route('folder.create')}}" class="btn btn-default text-success "> <i class="fa fa-plus" aria-hidden="true"></i> New</a> --}}
                            </div>  

                </div>

                <div class="card-body">
                    @if ($message = Session::get('success'))
                            
                    <div class="alert alert-success col-sm-12 alert-block">
        
                      <button type="button" class="close" data-dismiss="alert">×</button>
        
                        <strong>{{ $message }}</strong>
        
                    </div>
        
               @endif
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h6 class="font-weight-bolder">FOLDERS</h6>

                    <div class="row bg-light">
            {{-- PUBLIC FOLDERS --}}
                     <div class="col-sm-7 m-2">
                         <h6  class="font-weight-bolder " style="font-size:10px">PUBLIC FOLDERS</h6>
                        <div class="row p-1 border">
                            
                            @foreach ($publicFolders as $publicFolder)
                            <div class="col-sm-2 m-2 pt-3 bg-light text-center ">
                                <a href="{{route('subhome',$publicFolder)}}"><img class="img img-fluid rounded" src="/images/icons/folder.svg" alt="" srcset=""></a>
                            <p class="text-center" style="font-size:10px;">{{$publicFolder->name}}</p>
                            </div>
                            @endforeach
                            
                        </div>
                    </div>

            {{-- SHARED FOLDERS --}}
                    <div class="col-sm-4 m-2">
                        <h6  class="font-weight-bolder text-danger" style="font-size:10px">MY PRIVATE FOLDERS</h6>
                        <div class="row p-1 ">
                            
                            @foreach ($privateFolders as $privateFolder)
                            <div class="col-sm-2 m-2 pt-3 p-0 bg-light text-center ">
                                <a href="{{route('subhome',$privateFolder)}}"><img class="img img-fluid rounded" src="/images/icons/folder.svg" alt="" srcset=""></a>
                            <p class="text-center" style="font-size:10px;">{{$privateFolder->name}}</p>
                            </div>
                            @endforeach
                            
                        </div>
                    </div>
                    
            {{-- PRIVATE FOLDERS
                    <div class="col-sm-3 m-2">
                        <h6  class="font-weight-bolder " style="font-size:10px">MY PRIVATE FOLDERS</h6>
                        <div class="row p-1 border">
                            
                            @foreach ($folders as $folder)
                            <div class="col-sm-2 m-2 pt-3 bg-light text-center border">
                                <a href="{{route('subhome',$folder)}}"><img class="img img-fluid rounded" src="/images/icons/folder.svg" alt="" srcset=""></a>
                            <p class="text-center" style="font-size:10px;">{{$folder->name}}</p>
                            </div>
                            @endforeach
                            
                        </div>
                    </div> --}}
                    
                </div>








                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

@endsection
