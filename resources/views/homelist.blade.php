@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header font-weight-bolder">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Add New <i class="fa fa-plus" aria-hidden="true"></i>
                        </button>
                        <div class="dropdown-menu">
                        {{-- <a class="dropdown-item font-weight-bolder" href="{{route('home')}}"> <i class="fa fa-file-alt" aria-hidden="true"></i> All</a> --}}
                        <a class="dropdown-item font-weight-bold text-primary" href="{{route('push')}}"> <i class="fa fa" aria-hidden="true"></i> File</a>
                        <a class="dropdown-item font-weight-bold text-primary" href="{{route('folder.create')}}"> <i class="fa fa" aria-hidden="true"></i> Folder</a>
                        {{-- <a class="dropdown-item" href="#">Something else here</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Separated link</a> --}}
                        </div>
                    </div>
                    <div class="float-right">
                        {{-- <form class="form-inline float-left mr-2">
                         
                          <div class="input-group ">
                              <input type="search" class="form-control border-primary" placeholder="Search" aria-label="Search" aria-describedby="button-addon2">
                              <div class="input-group-append">
                                <button class="btn btn-outline-primary bg-white" style="border-left:0px; " type="submit" id="button-addon2"><i class="fa fa-search" aria-hidden="true"></i></button>
                              </div>
                            </div>
                      
                      </form> --}}
                        <div class="btn-group " role="group" aria-label="Basic example">
                          <button type="button" class="btn btn-primary text-white"><a href="{{ route('homelist') }}"><i class="fa fa-list text-white" aria-hidden="true"></i></a></button>
                          {{-- <button type="button" class="btn btn-secondary bg-dark"><a href="{{ route('home') }}"><i class="fa fa-th" aria-hidden="true"></i></a></button> --}}
                          <button type="button" class="btn btn-primary text-white"><a href="{{ route('home') }}"><i class="fa fa-th text-white" aria-hidden="true"></i></a></button>
                        </div>
                        
                              {{-- <a href="{{route('push')}}" class="btn btn-primary "> <i class="fa fa-file-medical" aria-hidden="true"></i> </a> --}}
                          </div>   

                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{-- You are logged in!
                    <a href="{{route('push')}}">Uplaod</a> --}}

                    <div class="row justify-content-center">
                        <div class="col-sm-12 table-responsive">
                        <table id="example" class="table table-light table-condensed  table-striped table-hover ">
                            <thead class="">
                                <tr>
                                    <th class=" ">Name</th>
                                    <th></th>
                                    <th class="">Created by</th>
                                    <th class="">Permision</th>
                                    <th>Size</th>
                                    <th class=" text-right">Date Created</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($publicFolders as $publicFolder)
                                <tr class="border-bottom clickable-row" data-href="{{route('subhome',$publicFolder)}}">
                                    <td class="align-middle" style="border-left:1px solid green">   
                                       <div class="p-1">
                                        <a href="{{route('subhome',$publicFolder)}}"><img class="img img-fluid rounded" src="/images/icons/folder.svg" alt="" srcset="" height="20" width="20" ></a>
                                       </div>
                                     </td>
                                     <td class="align-middle">   
                                        {{$publicFolder->name}}
                                     </td>
                                     <td class="align-middle "> 
                                        @php
                                            $user= Auth::user();
                                            $owner=DB::table('folders')->where('id','=',$publicFolder->id)->first();
                                        @endphp
                                    
                                        @if(($owner->user_id == $user->id ))
                                            {{ "Me" }}                            
                                        @else
                                            @php
                                                $another=DB::table('users')->where('id','=',$owner->user_id)->first();
                                            @endphp
                                                {{$another->firstname}} {{$another->lastname}}
                                        @endif
                                    </td>
                                    <td class="text-uppercase">
                                        {{$publicFolder->permision}}
                                    </td>
                                    <td>
                                        ---
                                    </td>
                                    </td>
                                        
                                    <td class=" text-right">
                                        {{$publicFolder->created_at}}
                                    </td>

                                </tr>
                                @endforeach

                                @foreach ($privateFolders as $privateFolder)
                                    <tr class="border-bottom clickable-row" data-href="{{route('subhome',$privateFolder)}}">
                                        <td class="align-middle" style="border-left:1px solid red">   
                                        <div class="p-1">
                                            <a href="{{route('subhome',$privateFolder)}}"><img class="img img-fluid rounded" src="/images/icons/folder.svg" alt="" srcset="" height="20" width="20" ></a>
                                        </div>
                                        </td>
                                        <td class="align-middle">   
                                            {{$privateFolder->name}}
                                        </td>
                                        <td class="align-middle"> 
                                            @php
                                                $user= Auth::user();
                                                $owner=DB::table('folders')->where('id','=',$privateFolder->id)->first();
                                            @endphp
                                        
                                            @if(($owner->user_id == $user->id ))
                                                {{ "Me" }}                            
                                            @else
                                            @php
                                                $another=DB::table('users')->where('id','=',$owner->user_id)->first();
                                            @endphp
                                                    {{$another->firstname}} {{$another->lastname}}
                                            @endif
                                        </td>
                                        <td class="text-uppercase">
                                            {{$privateFolder->permision}}
                                        </td>
                                        <td>
                                            ---
                                        </td>
                                        </td>
                                            
                                        <td class=" text-right">
                                            {{$privateFolder->created_at}}
                                        </td>

                                    </tr>
                                @endforeach
                                @foreach ($files as $file)

                                        @if(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='docx' || pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='DOCX' )
                                    
                                            <tr class="border-bottom clickable-row" data-href="{{route('text',$file)}}">

                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='doc'|| pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='DOC' )
                                    
                                            <tr class="border-bottom clickable-row" data-href="{{route('text',$file)}}">
                                        
                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='PNG' )
                                    
                                            <tr class="border-bottom clickable-row" data-href="{{route('image',$file)}}">

                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='png' )
                                    
                                            <tr class="border-bottom clickable-row" data-href="{{route('image',$file)}}">

                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='jpeg' )
                                    
                                            <tr class="border-bottom clickable-row" data-href="{{route('image',$file)}}">

                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='JPEG' )
                                    
                                            <tr class="border-bottom clickable-row" data-href="{{route('image',$file)}}">

                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='JPG' )
                                    
                                            <tr class="border-bottom clickable-row" data-href="{{route('image',$file)}}">

                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='jpg' )
                                    
                                            <tr class="border-bottom clickable-row" data-href="{{route('image',$file)}}">

                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='pdf' )
                                    
                                            <tr class="border-bottom clickable-row" data-href="{{route('pdf',$file)}}">

                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='PDF' )
                                    
                                            <tr class="border-bottom clickable-row" data-href="{{route('pdf',$file)}}">

                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='mp3' || pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='MP3' )
                                    
                                            <tr class="border-bottom clickable-row" data-href="{{route('audio',$file)}}">

                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='csv' || pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='CSV' )
                                    
                                            <tr class="border-bottom clickable-row" data-href="{{route('sheet',$file)}}">

                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='xlsx' || pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='XLSX' )
                                    
                                            <tr class="border-bottom clickable-row" data-href="{{route('sheet',$file)}}">

                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='html' || pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='HTML' )
                                    
                                            <tr class="border-bottom clickable-row" data-href="#">

                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='pptx' || pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='PPTX' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='PPT' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='ppt' )
                                    
                                            <tr class="border-bottom clickable-row" data-href="{{route('slide',$file)}}">

                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='mp4' || pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='MP4' )
                                    
                                            <tr class="border-bottom clickable-row" data-href="{{route('video',$file)}}">

                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='zip' || pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='ZIP' )
                                    
                                            <tr class="border-bottom clickable-row" data-href="#">
            
                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='rar' || pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='RAR' )
                                        
                                            <tr class="border-bottom clickable-row" data-href="#">
                                        @endif
                                                <td style="border-left:1px solid green">
                                                        <div class=" p-1 ">
                                                                {{-- {{dd($extension)}}  $extension =='docx'--}}
                                                                @if(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='docx' || pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='DOCX' )
                                                                <a href="{{route('text',$file)}}"><img class="img img-fluid rounded" src="/images/icons/docs.svg" alt="" srcset="" height="20" width="20" ></a>
                                                                    {{-- <p class="text-center">{{$file->filename}}</p> --}}
                                                            
                                                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='doc' || pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='DOC' )
                                                                <a href="{{route('text',$file)}}"><img class="img img-fluid rounded" src="/images/icons/docs.svg" alt="" srcset="" height="20" width="20" ></a>
                                                                        {{-- <p class="text-center">{{$file->filename}}</p> --}}
                                                                
                                                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='PNG')
                                                                <a href="{{route('image',$file)}}"><img class="img img-fluid rounded" src="{{"storage/files/$file->filename"}}" alt="" srcset="" height="20" width="20"></a>
                                                                {{-- <p class="text-center">{{$file->filename}}</p> --}}
                                    
                                                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='png')
                                                                <a href="{{route('image',$file)}}"><img class="img img-fluid rounded" src="{{"storage/files/$file->filename"}}" alt="" srcset="" height="20" width="20"></a>
                                                                {{-- <p class="text-center">{{$file->filename}}</p> --}}
                                    
                                                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='jpeg')
                                                                <a href="{{route('image',$file)}}"><img class="img img-fluid rounded" src="{{"storage/files/$file->filename"}}" alt="" srcset="" height="20" width="20"></a>
                                                                {{-- <p class="text-center">{{$file->filename}}</p> --}}
                                    
                                                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='JPEG')
                                                                <a href="{{route('image',$file)}}"><img class="img img-fluid rounded" src="{{"storage/files/$file->filename"}}" alt="" srcset="" height="20" width="20"></a>
                                                                {{-- <p class="text-center">{{$file->filename}}</p> --}}
                                    
                                                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='JPG')
                                                                <a href="{{route('image',$file)}}"><img class="img img-fluid rounded" src="{{"storage/files/$file->filename"}}" alt="" srcset="" height="20" width="20"></a>
                                                                {{-- <p class="text-center">{{$file->filename}}</p> --}}
                                    
                                                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='jpg')
                                                                <a href="{{route('image',$file)}}"><img class="img img-fluid rounded" src="{{"storage/files/$file->filename"}}" alt="" srcset="" height="20" width="20"></a>
                                                                {{-- <p class="text-center">{{$file->filename}}</p> --}}
                                    
                                                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='pdf')
                                                                <a href="{{route('pdf',$file)}}"><img class="img img-fluid rounded" src="/images/icons/pdf.svg" alt="" srcset="" height="20" width="20" ></a>
                                                                {{-- <p class="text-center">{{$file->filename}}</p> --}}

                                                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='PDF')
                                                                <a href="{{route('pdf',$file)}}"><img class="img img-fluid rounded" src="/images/icons/pdf.svg" alt="" srcset="" height="20" width="20" ></a>
                                                                {{-- <p class="text-center">{{$file->filename}}</p> --}}
                                    
                                                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='mp3' || pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='MP3' )
                                                                <a href="{{route('audio',$file)}}"><img class="img img-fluid rounded" src="/images/icons/mp3.svg" alt="" srcset="" height="20" width="20" ></a>
                                                                {{-- <p class="text-center">{{$file->filename}}</p> --}}
                                    
                                                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='csv' || pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='CSV' )
                                                                <a href="{{route('sheet',$file)}}"><img class="img img-fluid rounded" src="/images/icons/sheets.svg" alt="" srcset="" height="20" width="20" ></a>
                                                                {{-- <p class="text-center">{{$file->filename}}</p> --}}
                                    
                                                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='xlsx' || pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='XLSX' )
                                                                <a href="{{route('sheet',$file)}}"><img class="img img-fluid rounded" src="/images/icons/sheets.svg" alt="" srcset="" height="20" width="20" ></a>
                                                                {{-- <p class="text-center">{{$file->filename}}</p> --}}
                                    
                                                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='html' || pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='HTML' )
                                                                <a href="#"><img class="img img-fluid rounded" src="/images/icons/html.svg" alt="" srcset="" height="20" width="20" ></a>
                                                                {{-- <p class="text-center">{{$file->filename}}</p> --}}
                                                                
                                    
                                                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='pptx' || pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='PPTX' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='PPT' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='ppt' )
                                                                <a href="{{route('slide',$file)}}"><img class="img img-fluid rounded" src="/images/icons/slides.svg" alt="" srcset="" height="20" width="20" ></a>
                                                                {{-- <p class="text-center">{{$file->filename}}</p> --}}
                                
                                                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='mp4' || pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='MP4' )
                                                                <a href="{{route('video',$file)}}"><img class="img img-fluid rounded" src="/images/icons/cone.svg" alt="" srcset="" height="20" width="20" ></a>
                                                                {{-- <p class="text-center">{{$file->filename}}</p> --}}

                                                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='zip' || pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='ZIP' )
                                                                <a href="#"><img class="img img-fluid rounded" src="/images/icons/zip.svg" alt="" srcset="" height="20" width="20" ></a>
                                                                {{-- <p class="text-center">{{$file->filename}}</p> --}}

                                                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='rar' || pathinfo(storage_path(Storage::disk('local')->url("files/$file->filename")), PATHINFO_EXTENSION) =='RAR' )
                                                                <a href="#"><img class="img img-fluid rounded" src="/images/icons/zip.svg" alt="" srcset="" height="20" width="20" ></a>
                                                                {{-- <p class="text-center">{{$file->filename}}</p> --}}
                                                                @endif
                                                            </div>
                                                </td>
                                                <td class="align-middle">
                                                    
                                                {{$file->filename}}
                                                
                                                </td>
                                            
                                                <td class="align-middle "> 
                                                    @php
                                                        $user= Auth::user();
                                                    $owner=DB::table('users')->where('id','=',$file->user_id)->first();
                                                @endphp
                                                
                                                    @if(($owner->firstname == $user->firstname ) && ($owner->lastname == $user->lastname ))
                                                        {{ "Me" }}
                                                                                        
                                                    @else
                                                    
                                                        {{$owner->firstname}}
                                                    @endif
                                            </td>
                                            <td class="text-uppercase">
                                                {{$file->permision}}
                                            </td>
                                            <td class="align-middle">
                                                @php
                                                    $size = Storage::disk('public')->size("files/$file->filename");
                                                    $kb = 1024;
                                                    $mb = $kb * 1024;
                                                    $gb = $mb * 1024;
                                                    $tb = $gb * 1024;
                                                
                                                     if (($size >= $kb) && ($size < $mb)) {
                                                        $sizem = round(($size / $kb)) . ' KB';
                                                    } elseif (($size >= $mb) && ($size < $gb)) {
                                                        $sizem = round(($size / $mb)) . ' MB';
                                                    } elseif (($size >= $gb) && ($size < $tb)) {
                                                        $sizem = round(($size / $gb)) . ' GB';
                                                    } elseif ($size >= $tb) {
                                                        $sizem = round(($size / $tb)) . ' TB';
                                                    } else {
                                                        $sizem = round($size) . ' B';
                                                    }
                                                @endphp
                                                {{$sizem}}
                                            </td>
                                            
                                                <td class=" text-right">{{$file->created_at}}</td>

                                            </tr>
                              
                                @endforeach



                                @foreach ($privateFiles as $privateFile)

                                @if(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='docx' || pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='DOCX' )
                            
                                    <tr class="border-bottom clickable-row" data-href="{{route('text',$privateFile)}}">

                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='doc'|| pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='DOC' )
                            
                                    <tr class="border-bottom clickable-row" data-href="{{route('text',$privateFile)}}">
                                
                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='PNG' )
                            
                                    <tr class="border-bottom clickable-row" data-href="{{route('image',$privateFile)}}">

                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='png' )
                            
                                    <tr class="border-bottom clickable-row" data-href="{{route('image',$privateFile)}}">

                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='jpeg' )
                            
                                    <tr class="border-bottom clickable-row" data-href="{{route('image',$privateFile)}}">

                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='JPEG' )
                            
                                    <tr class="border-bottom clickable-row" data-href="{{route('image',$privateFile)}}">

                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='JPG' )
                            
                                    <tr class="border-bottom clickable-row" data-href="{{route('image',$privateFile)}}">

                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='jpg' )
                            
                                    <tr class="border-bottom clickable-row" data-href="{{route('image',$privateFile)}}">

                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='pdf' )
                            
                                    <tr class="border-bottom clickable-row" data-href="{{route('pdf',$privateFile)}}">

                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='PDF' )
                            
                                    <tr class="border-bottom clickable-row" data-href="{{route('pdf',$privateFile)}}">

                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='mp3' || pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='MP3' )
                            
                                    <tr class="border-bottom clickable-row" data-href="{{route('audio',$privateFile)}}">

                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='csv' || pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='CSV' )
                            
                                    <tr class="border-bottom clickable-row" data-href="{{route('sheet',$privateFile)}}">

                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='xlsx' || pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='XLSX' )
                            
                                    <tr class="border-bottom clickable-row" data-href="{{route('sheet',$privateFile)}}">

                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='html' || pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='HTML' )
                            
                                    <tr class="border-bottom clickable-row" data-href="#">

                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='pptx' || pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='PPTX' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='PPT' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='ppt' )
                            
                                    <tr class="border-bottom clickable-row" data-href="{{route('slide',$privateFile)}}">

                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='mp4' || pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='MP4' )
                            
                                    <tr class="border-bottom clickable-row" data-href="{{route('video',$privateFile)}}">

                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='zip' || pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='ZIP' )
                            
                                    <tr class="border-bottom clickable-row" data-href="#">
    
                                @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='rar' || pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='RAR' )
                                
                                    <tr class="border-bottom clickable-row" data-href="#">
                                @endif
                                        <td style="border-left:1px solid red">
                                                <div class=" p-1 ">
                                                        {{-- {{dd($extension)}}  $extension =='docx'--}}
                                                        @if(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='docx' || pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='DOCX' )
                                                        <a href="{{route('text',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/docs.svg" alt="" srcset="" height="20" width="20" ></a>
                                                            {{-- <p class="text-center">{{$privateFile->filename}}</p> --}}
                                                    
                                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='doc' || pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='DOC' )
                                                        <a href="{{route('text',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/docs.svg" alt="" srcset="" height="20" width="20" ></a>
                                                                {{-- <p class="text-center">{{$privateFile->filename}}</p> --}}
                                                        
                                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='PNG')
                                                        <a href="{{route('image',$privateFile)}}"><img class="img img-fluid rounded" src="{{"storage/files/$privateFile->filename"}}" alt="" srcset="" height="20" width="20"></a>
                                                        {{-- <p class="text-center">{{$privateFile->filename}}</p> --}}
                            
                                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='png')
                                                        <a href="{{route('image',$privateFile)}}"><img class="img img-fluid rounded" src="{{"storage/files/$privateFile->filename"}}" alt="" srcset="" height="20" width="20"></a>
                                                        {{-- <p class="text-center">{{$privateFile->filename}}</p> --}}
                            
                                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='jpeg')
                                                        <a href="{{route('image',$privateFile)}}"><img class="img img-fluid rounded" src="{{"storage/files/$privateFile->filename"}}" alt="" srcset="" height="20" width="20"></a>
                                                        {{-- <p class="text-center">{{$privateFile->filename}}</p> --}}
                            
                                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='JPEG')
                                                        <a href="{{route('image',$privateFile)}}"><img class="img img-fluid rounded" src="{{"storage/files/$privateFile->filename"}}" alt="" srcset="" height="20" width="20"></a>
                                                        {{-- <p class="text-center">{{$privateFile->filename}}</p> --}}
                            
                                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='JPG')
                                                        <a href="{{route('image',$privateFile)}}"><img class="img img-fluid rounded" src="{{"storage/files/$privateFile->filename"}}" alt="" srcset="" height="20" width="20"></a>
                                                        {{-- <p class="text-center">{{$privateFile->filename}}</p> --}}
                            
                                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='jpg')
                                                        <a href="{{route('image',$privateFile)}}"><img class="img img-fluid rounded" src="{{"storage/files/$privateFile->filename"}}" alt="" srcset="" height="20" width="20"></a>
                                                        {{-- <p class="text-center">{{$privateFile->filename}}</p> --}}
                            
                                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='pdf')
                                                        <a href="{{route('pdf',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/pdf.svg" alt="" srcset="" height="20" width="20" ></a>
                                                        {{-- <p class="text-center">{{$privateFile->filename}}</p> --}}

                                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='PDF')
                                                        <a href="{{route('pdf',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/pdf.svg" alt="" srcset="" height="20" width="20" ></a>
                                                        {{-- <p class="text-center">{{$privateFile->filename}}</p> --}}
                            
                                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='mp3' || pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='MP3' )
                                                        <a href="{{route('audio',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/mp3.svg" alt="" srcset="" height="20" width="20" ></a>
                                                        {{-- <p class="text-center">{{$privateFile->filename}}</p> --}}
                            
                                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='csv' || pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='CSV' )
                                                        <a href="{{route('sheet',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/sheets.svg" alt="" srcset="" height="20" width="20" ></a>
                                                        {{-- <p class="text-center">{{$privateFile->filename}}</p> --}}
                            
                                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='xlsx' || pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='XLSX' )
                                                        <a href="{{route('sheet',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/sheets.svg" alt="" srcset="" height="20" width="20" ></a>
                                                        {{-- <p class="text-center">{{$privateFile->filename}}</p> --}}
                            
                                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='html' || pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='HTML' )
                                                        <a href="#"><img class="img img-fluid rounded" src="/images/icons/html.svg" alt="" srcset="" height="20" width="20" ></a>
                                                        {{-- <p class="text-center">{{$privateFile->filename}}</p> --}}
                                                        
                            
                                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='pptx' || pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='PPTX' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='PPT' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='ppt' )
                                                        <a href="{{route('slide',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/slides.svg" alt="" srcset="" height="20" width="20" ></a>
                                                        {{-- <p class="text-center">{{$privateFile->filename}}</p> --}}
                        
                                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='mp4' || pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='MP4' )
                                                        <a href="{{route('video',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/cone.svg" alt="" srcset="" height="20" width="20" ></a>
                                                        {{-- <p class="text-center">{{$privateFile->filename}}</p> --}}

                                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='zip' || pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='ZIP' )
                                                        <a href="#"><img class="img img-fluid rounded" src="/images/icons/zip.svg" alt="" srcset="" height="20" width="20" ></a>
                                                        {{-- <p class="text-center">{{$privateFile->filename}}</p> --}}

                                                        @elseif(pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='rar' || pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='RAR' )
                                                        <a href="#"><img class="img img-fluid rounded" src="/images/icons/zip.svg" alt="" srcset="" height="20" width="20" ></a>
                                                        {{-- <p class="text-center">{{$privateFile->filename}}</p> --}}
                                                        @endif
                                                    </div>
                                        </td>
                                        <td class="align-middle">
                                            
                                        {{$privateFile->filename}}
                                        
                                        </td>
                                    
                                        <td class="align-middle "> 
                                            @php
                                                $user= Auth::user();
                                            $owner=DB::table('users')->where('id','=',$privateFile->user_id)->first();
                                        @endphp
                                        
                                            @if(($owner->firstname == $user->firstname ) && ($owner->lastname == $user->lastname ))
                                                {{ "Me" }}
                                                                                
                                            @else
                                            
                                                {{$owner->firstname}}
                                            @endif
                                    </td>
                                    <td class="text-uppercase">
                                        {{$privateFile->permision}}
                                    </td>
                                    <td class="align-middle">
                                        @php
                                    $size = Storage::disk('public')->size("files/$privateFile->filename");
                                        //    $size = Storage::disk('local')->url("files/$privateFile->filename")->size();
                                        @endphp
                                        {{$size}}
                                    </td>
                                    
                                        <td class=" text-right">{{$privateFile->created_at}}</td>

                                    </tr>
                      
                        @endforeach
                            </tbody>

                            <script>
                                jQuery(document).ready(function($) {
                                    $(".clickable-row").click(function() {
                                        window.location = $(this).data("href");
                                    });
                                 });
                            </script>
                           
                        </table>
                          
                            
                        </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
      $('#example').DataTable( {
          "paging":   true,
          "ordering": true,
          "info":     false,
          language: {
            searchPlaceholder: "Search"
        } 
      },
     );
  
    });
  
    </script>

<style>
    input{
        border:1px solid #009688 !important;
        padding: 20px !important;
    }

    select{
        border:1px solid #009688 !important;
        padding: 10px !important;
        width: auto !important;
        color: #000 !important;
        height: 42px !important;
        padding-right: 30px !important;
        padding-left: 20px !important;
       

    }
</style>
  
@endsection
 
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> 