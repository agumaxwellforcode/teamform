@extends('layouts.head')

@section('content')
<div class="container-fluid p-0">
   
        <nav class="navbar navbar-expand-md navbar-dark bg-white text shadow-sm">
                <div class="container">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <a class="nav-link text-white" href="/"> <img class="" height="70px" width="70px" src="/images/logo1.jpeg" /></a>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>
    
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">
    
                        </ul>
    
                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                           
                            <li class="nav-item">
                                    {{-- <a class="nav-link text-white" href="">{{ __('Contact Admin') }}</a> --}}
                                </li>
                               
                                {{-- <li class="nav-item">
                                        <a class="nav-link text-white" href=""></a>
                                    </li>
                                    <li class="nav-item">
                                            <a class="nav-link text-white" href=""></a>
                                        </li> --}}
                           
                                    {{-- <li class="nav-item">
                                            <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
                                        </li> --}}
                               
                            
                            <li class="nav-item">
                                   
                            </li>
    
                          
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="row justify-content-center p-0 m-0">
                    <div class="col-md-6" style="height: 90vh">
                        <br>
                        <br>
                        <br>
                            <div class="card">
                                <div class="card-header bg-success text-white">Account Activation</div>
                                    <div class="card-body">
                                       Congratulations, Account creation successfull but yet to be approved or is disabled. Please contact Admin for complaints and more information.   <br><br> Thank you. <br> <br>
                                        
                                        <a  class="btn btn-success" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                      document.getElementById('logout-form').submit();">
                                           {{ __('Proceed') }} <i class="fa fa-angle-right" aria-hidden="true"></i>
                                     </a>  
                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                        <br>
                                    </div>
                            </div>
                    </div>

        </div>




        
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  
@endsection