@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-7">
            <div class="card">
             
                <div class="card-header  bg-light text-primary font-weight-bold">Assign Users to : {{ $group->name }} Group
                    <div class="float-right">
                        <a href="{{ route('admin.group.index') }}" class="btn btn-default btn-sm"> < Back </a>
                    </div>
                </div>

                <div class="card-body pt-0">

                   
                        {{-- <div class="row justify-content-center mb-3 text-primary font-weight-bolder">
                          {{ implode(', ', $user->groups()->get()->pluck('name')->toArray()) }}
                        </div> --}}
                      <div class="row justify-content-center">
                        
                        
                          <div class="custom-control custom-switch col-sm-12  m-4 text-center bg-light pl-1">
                            @if ($message = Session::get('success'))
                              
                            <div class="alert alert-success alert-block">
            
                                <button type="button" class="close" data-dismiss="alert">×</button>
            
                                <strong>{{ $message }}</strong>
            
                            </div>
    
                      @endif

                              <table id="example" class="table table-striped table-borderless">
                                    
                                  <thead>
                                      <tr>
                                          
                                          <th></th>
                                          <th></th>
                                         
                                          
                                      </tr>
                                  </thead>
                                  <tbody >
                                      @foreach ($users as $user)
                                          <tr >
                                  
                                            {{-- <td class="text-primary"></td> --}}
                                            <td class="text-primary"><img class="rounded-circle" height="50px" width="50px" src="/storage/avatars/{{ $user->avatar }}" /> {{  $user->firstname }} {{  $user->lastname }}</td>
                                            <td class="">
                                                <form action="{{route('admin.group.update',$group)}}" method="post">
                                                    @csrf
                                                    {{ method_field('PUT') }}
                                                    <input type="hidden" name="users" value="{{$user->id}}">                            
                                                    <button type="submit" class="btn btn-outline-success float-right mr-1 mt-3" style="padding:3px;"> <i class="fas fa-user-plus "></i></button>                                                     
                                                  </form>
                                              </td>
                                           
                                          </tr>
                                        
                                        @endforeach
                                  </tbody>
                              </table>
                            
                              {{-- <input type="checkbox" name="groups[]" class="custom-control-input" id="{{$group->id}}" value="{{$group->id}}">
                              <label class="custom-control-label" for="{{$group->id}}">{{$group->name}}</label> --}}
                            </div>
                              {{-- <div class="form-check col-sm-4 border m-3 text-center pt-3 pb-2 bg-light">
                              <input type="checkbox" name="roles[]" id="" value="{{$role->id}}">
                              <label for="checkbox" class="font-weight-bold">{{$role->name}}</label>
                              </div> --}}
                          
                        
                      </div>

                      {{-- <div class="row justify-content-center mt-3 mb-4">
                        <button type="submit" class="btn btn-outline-success">Update</button>
                       
                        <a href="{{route('admin.users.index')}}" class="btn btn-outline-default text-danger float-right">Cancel</a>
                     
                      </div> --}}

                </div>

                
            </div>
        </div>
    </div>
</div>

{{-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script> --}}


    <script>
     $(document).ready(function() {
    $('#example').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
     </script>
<style>
    input{
        border:1px solid #3490dc !important;
        padding: 20px !important;
    }
</style>
    
{{-- 
     
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
   --}}
@endsection

</body>
</html>