@extends('layouts.app')

@section('content')





<div class="container-fluid">
    <div class="row justify-content-center m-0">
        <div class="col-sm-2 p-0">
        
             <!-- Sidebar -->
             <div class="bg-light border" id="sidebar-wrapper">
                  
                <div class="list-group list-group-flush">
                  <a href="#" class="list-group-item list-group-item-action bg-dark text-white">ADMIN</a>
                <a href="{{ route('admin.users.index') }}" class="list-group-item list-group-item-action font-weight-bold">USERS</a>
                  {{-- <a href="{{ route('admin.group.index') }}" class="list-group-item list-group-item-action  font-weight-bold bg-success ">GROUPS</a> --}}
                  <a href="{{ route('admin.announcement.index') }}" class="list-group-item list-group-item-action bg-light font-weight-bold">ANNOUNCEMENT</a>
                  <a href="{{ route('admin.registration.index') }}" class="list-group-item list-group-item-action bg-light font-weight-bold">REGISTRATION</a>
                  <a href="{{ route('inbox') }}" class="list-group-item list-group-item-action bg-light font-weight-bold">INBOX
                    @php
                      $count = DB::table('contacts')->where('status','=','Not Treated')->count();
                    @endphp
                    @if ($count !== 0)
                      <span class="badge badge-pill badge-danger ml-3">
                        {{ $count }}
                      </span>
                    @endif
                  </a>
                  <a href="{{ route('quota') }}" class="list-group-item list-group-item-action bg-light font-weight-bold">QUOTA</a>
                  
                </div>
              </div>
              <!-- /#sidebar-wrapper -->


        </div>

       

        <div class="col-md-10">
            <div class="row mb-0">


                {{-- <div class="col-sm-1">  --}}

                    {{-- <div class="btn-group" role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-default bg-light btn-lg  p-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-users  pr-1 "></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                        <a class="dropdown-item font-weight-bolder" href="{{ route('admin.group.create') }}"> <i class="fa fa-plus" aria-hidden="true"></i> Create new group</a>
                        </div>
                      </div> --}}

                {{-- </div> --}}

           {{-- breadcrumb --}}

                  <nav aria-label="breadcrumb col-sm-5">
                      <ol class="breadcrumb ">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                        <li class="breadcrumb-item"><a href="#">Configuration</a></li>
                        <li class="breadcrumb-item "><a href="#">Admin</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Groups</li>
                      </ol>
                    </nav>
                    <div class="col-sm-3"> 
                       

                          <div class="btn-group" role="group" aria-label="Third group">
                              <button type="button" class="btn btn-default bg-light p-2"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                            </div>
                    </div>

            </div>
            {{-- breadcrumb --}}

            <div class="card mt-3">

                    {{-- @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                    @endif --}}

                <div class="card-header bg-light text-primary font-weight-bolder"><h3 class="font-weight-bolder float-left"> Groups</h3>
                    {{-- <div class="float-right">
                        <a href="{{ route('admin.users.index') }}" class="btn btn-outline-danger btn-sm"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                    </div> --}}
                    <div class="float-right">
                    <a class="btn btn-primary font-weight-bolder float-right" href="{{ route('admin.group.create') }}"> <i class="fa fa-plus" aria-hidden="true"></i> Create new group</a>
                    </div>

                </div>

                <div class="card-body">

                    @if ($message = Session::get('success'))
                            
                        <div class="alert alert-success alert-block">
        
                            <button type="button" class="close" data-dismiss="alert">×</button>
        
                            <strong>{{ $message }}</strong>
        
                        </div>
    
                   @endif
                       
                 
                  <div class="table-responsive">
                   <table id="example" class="table table-borderless">
                    <thead>
                      <tr>
                        {{-- <th scope="col">ID</th> --}}
                        <th scope="col">Name</th>
                        <th class="text-center" scope="col">Members</th>
                        <th scope="col">Description</th>
                        <th scope="col">Last Modified</th>
                       
                        <th class="float-right" scope="col">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($groups as $group)

                        @php
                         $count = DB::table('group_user')->where('group_id','=',$group->id)->count();
                        @endphp
                            <tr>
                                {{-- <th class="text-center" scope="row"> {{ $group->id }}</th> --}}
                                <td class="font-weight-bol"> {{ $group->name }}</td>
                                <td  class="font-weight-bold text-center">  <a href="{{route('admin.group.index',$group->id)}}"><i class="fas fa-users mr-2"></i>{{ $count }}</a></td>
                                <td  class="font-weight-bol"> {{ $group->description }}</td>

                                <td  class="font-weight-bol"> {{ $group->updated_at }}</td>

                              
                                
                               
                                <td class="text-center align-content-end">
                                <a href="{{route('admin.group.edit',$group->id)}}" class="float-left"><button type="button" class="btn btn-primary float-right " style="padding:5px;"> <i class="fas fa-user-plus "></i> Add Users</button></a>
                                       
                                    <form action="{{route('admin.group.destroy',$group)}}" method="post" class="float-right">
                                        @csrf
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="btn btn-danger" style="padding:5px;"><i class="fa fa-trash" aria-hidden="true"></i> Delete Group</button>
                                    </form>
                            </td>

                            </tr>
                      @endforeach

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>

{{-- 
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> --}}

<script>
  $(document).ready(function() {
    $('#example').DataTable( {
        "paging":   true,
        "ordering": true,
        "info":     false,
        language: {
          searchPlaceholder: "Search"
      } 
    },
   );

  });

  </script>
<style>
  input{
      border:1px solid #3490dc !important;
      padding: 20px !important;
  }
  select{
      border:1px solid #3490dc !important;
      padding: 10px !important;
      min-height: 45px !important;
      min-width: 100px !important;
      color: black !important;
  }
  #example_filter > label > input{
      /* background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIgICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgICB2ZXJzaW9uPSIxLjEiICAgaWQ9InN2ZzQ0ODUiICAgdmlld0JveD0iMCAwIDIxLjk5OTk5OSAyMS45OTk5OTkiICAgaGVpZ2h0PSIyMiIgICB3aWR0aD0iMjIiPiAgPGRlZnMgICAgIGlkPSJkZWZzNDQ4NyIgLz4gIDxtZXRhZGF0YSAgICAgaWQ9Im1ldGFkYXRhNDQ5MCI+ICAgIDxyZGY6UkRGPiAgICAgIDxjYzpXb3JrICAgICAgICAgcmRmOmFib3V0PSIiPiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+ICAgICAgICA8ZGM6dHlwZSAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4gICAgICAgIDxkYzp0aXRsZT48L2RjOnRpdGxlPiAgICAgIDwvY2M6V29yaz4gICAgPC9yZGY6UkRGPiAgPC9tZXRhZGF0YT4gIDxnICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDMwLjM2MjIpIiAgICAgaWQ9ImxheWVyMSI+ICAgIDxnICAgICAgIHN0eWxlPSJvcGFjaXR5OjAuNSIgICAgICAgaWQ9ImcxNyIgICAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNjAuNCw4NjYuMjQxMzQpIj4gICAgICA8cGF0aCAgICAgICAgIGlkPSJwYXRoMTkiICAgICAgICAgZD0ibSAtNTAuNSwxNzkuMSBjIC0yLjcsMCAtNC45LC0yLjIgLTQuOSwtNC45IDAsLTIuNyAyLjIsLTQuOSA0LjksLTQuOSAyLjcsMCA0LjksMi4yIDQuOSw0LjkgMCwyLjcgLTIuMiw0LjkgLTQuOSw0LjkgeiBtIDAsLTguOCBjIC0yLjIsMCAtMy45LDEuNyAtMy45LDMuOSAwLDIuMiAxLjcsMy45IDMuOSwzLjkgMi4yLDAgMy45LC0xLjcgMy45LC0zLjkgMCwtMi4yIC0xLjcsLTMuOSAtMy45LC0zLjkgeiIgICAgICAgICBjbGFzcz0ic3Q0IiAvPiAgICAgIDxyZWN0ICAgICAgICAgaWQ9InJlY3QyMSIgICAgICAgICBoZWlnaHQ9IjUiICAgICAgICAgd2lkdGg9IjAuODk5OTk5OTgiICAgICAgICAgY2xhc3M9InN0NCIgICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjY5NjQsLTAuNzE3NiwwLjcxNzYsMC42OTY0LC0xNDIuMzkzOCwyMS41MDE1KSIgICAgICAgICB5PSIxNzYuNjAwMDEiICAgICAgICAgeD0iLTQ2LjIwMDAwMSIgLz4gICAgPC9nPiAgPC9nPjwvc3ZnPg==); */
      background-image: url(/images/icons/search.svg);
      background-repeat: no-repeat;
      background-color: #fff;
      background-size: 18px;
      background-position: 167px 11px !important;
      font-weight: bold;
  }
  #example_filter > label{
    color: #fff;
  }
</style>

@endsection