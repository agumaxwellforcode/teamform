@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-4">
              
            <div class="card-header bg-light text-primary font-weight-bold">Create Group:
                <div class="float-right">
                    <a href="{{ route('admin.group.index') }}" class="btn btn-default btn-sm"> < Back </a>
                </div>  
            </div>

                <div class="card-body">

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <p>
                            <strong>Input Error!</strong>
                        </p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
               
                   
               
                    <form action="{{route('admin.group.store')}}" method="POST">
                        {{ csrf_field() }}

                        {{ method_field('POST') }}
                        

                        <div class="form-group">
                            <label for="name">Group Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                        </div>
                 
                         <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" id="description" name="description" rows="3">{{ old('description') }}</textarea>
                        </div>
                 
                        <button type="submit" class="btn btn-success float-right"><i class="fa fa-plus" aria-hidden="true"> </i> Add group</button>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>


  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

@endsection