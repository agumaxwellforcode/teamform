@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-7">
            <div class="card">
             
                <div class="card-header bg-light text-primary font-weight-bold">Assign Users to : {{ $group->name }} Group
                    <div class="float-right">
                        <a href="{{ route('admin.group.index') }}" class="btn btn-default btn-sm"> < Back </a>
                    </div>
                </div>

                <div class="card-body pt-0">

                   
                        {{-- <div class="row justify-content-center mb-3 text-primary font-weight-bolder">
                          {{ implode(', ', $user->groups()->get()->pluck('name')->toArray()) }}
                        </div> --}}
                      <div class="row justify-content-center">
                        
                        
                          <div class="custom-control custom-switch col-sm-12  m-4 text-center bg-light pl-1">


                              <table id="example" class="table table-striped table-borderless">
                                    @if ($message = Session::get('success'))
                              
                                        <div class="alert alert-success alert-block">
                        
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                        
                                            <strong>{{ $message }}</strong>
                        
                                        </div>
                
                                  @endif
                                  <thead>
                                      <tr>
                                          
                                          <th></th>
                                          <th></th>
                                         
                                          
                                      </tr>
                                  </thead>
                                  <tbody >
                                      @php
                                          $groupUsers= DB::table('group_user')->where('group_id', '=',$group->id)
                                      @endphp
                                      @foreach ($groupUsers as $groupUser)
                                          <tr >
                                  
                                                {{-- {{ implode(', ', $user->roles()->get()->pluck('avatar','firstname','lastname')->toArray()) }} --}}


                                            <td class="text-primary"><img class="rounded-circle" height="50px" width="50px" src="/storage/avatars/{{ $user->avatar }}" />   {{ implode(', ', $user->roles()->get()->pluck('firstname','lastname')->toArray()) }}</td>
                                            <td class="">
                                                <form action="{{route('admin.group.update',$group)}}" method="post">
                                                    @csrf
                                                    {{ method_field('PUT') }}
                                                    <input type="hidden" name="users" value="{{$user->id}}">                            
                                                    <button type="submit" class="btn btn-outline-success float-right mr-1 mt-3" style="padding:3px;"> <i class="fas fa-user-plus "></i></button>                                                     
                                                  </form>
                                              </td>
                                           
                                          </tr>
                                        
                                        @endforeach
                                  </tbody>
                              </table>
                            
                              {{-- <input type="checkbox" name="groups[]" class="custom-control-input" id="{{$group->id}}" value="{{$group->id}}">
                              <label class="custom-control-label" for="{{$group->id}}">{{$group->name}}</label> --}}
                            </div>
                              {{-- <div class="form-check col-sm-4 border m-3 text-center pt-3 pb-2 bg-light">
                              <input type="checkbox" name="roles[]" id="" value="{{$role->id}}">
                              <label for="checkbox" class="font-weight-bold">{{$role->name}}</label>
                              </div> --}}
                          
                        
                      </div>

                      {{-- <div class="row justify-content-center mt-3 mb-4">
                        <button type="submit" class="btn btn-outline-success">Update</button>
                       
                        <a href="{{route('admin.users.index')}}" class="btn btn-outline-default text-danger float-right">Cancel</a>
                     
                      </div> --}}

                </div>

                
            </div>
        </div>
    </div>
</div>

    <script>
     $(document).ready(function() {
    $('#example').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
     </script>

     <script>
     $(function() {
          $('#favoritesModal').on("show.bs.modal", function (e) {
              $("#favoritesModalLabel").html($(e.relatedTarget).data('title'));
              $("#fav-title").html($(e.relatedTarget).data('title'));
          });
      });
     </script>
     
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  
@endsection

</body>
</html>