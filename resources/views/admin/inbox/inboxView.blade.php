@extends('layouts.app')

@section('content')





<div class="container-fluid">
    <div class="row justify-content-center m-0">

        <div class="col-md-12 p-0">
        
            {{-- breadcrumb --}}
           

            <div class="card border-0">

             
                @if ($message = Session::get('success'))
    
                <div class="alert alert-success col-sm-12 alert-block">
        
                    <button type="button" class="close" data-dismiss="alert">×</button>
      
                      <strong>{{ $message }}</strong>
      
                  </div>

                 
                

            @endif
           

                <div class="card-header bg- text- font-weight-bolder">Inbox
                    <div class="float-right">
                        <a onclick="goBack()" class="btn btn-default text-danger"> Back</a>
                        <a href="{{ route('home') }}" class="btn btn-default text-primary"> <i class="fa fa-home" aria-hidden="true"></i> Home</a>
                    </div> 

                </div>

                <div class="card-body">
                    <div class="row justify-content-center">
                        
                            <div class="col-sm-10 bg-light">
                                            <div class="row justify-content-center bg-light">
                                        
                                                <div class="col-sm-11  bg-white pt-4 m-3 pl-4">
                                                    <div class="float-left">
                                                    
                                                        <h6 class="card-subtitle mb-2 text-muted text-info" style="font-size:14px;">  {{ $contact->name }}</h6>
                                                        <h6 class="card-subtitle mb-2 text-muted text-info" style="font-size:9px;">  {{ $contact->designation }}</h6> 
                                                        <p class="h4 font-weight-bold">{{$contact->subject}}</p>  
                                                    </div>
                                                    <div class="float-right">

                                                    {{-- <a href="{{route('inboxTreated',$contact->id)}}" class="btn btn-default text-check "><i class="fa fa-check" aria-hidden="true"></i></a>
                                                    <a href="{{route('inboxTreated',$contact->id)}}" class="btn btn-default text-warning "><i class="fa fa-spinner" aria-hidden="true"></i></a> --}}
                                                        @if ($contact->status == 'Not Treated')
                                                            {{-- <form action="{{route('inboxNotTreated',$contact->id)}}" method="POST" class="float-right ">
                                                                @csrf
                                                                {{ method_field('POST') }} --}}
                                                                <button type="submit" class="btn btn-danger disabled float-right " data-toggle="tooltip" data-placement="bottom" title="Not Treated"><i class="fa fa-times" aria-hidden="true"></i></button>
                                                            {{-- </form> --}}
                                                        @else
                                                            <form action="{{route('inboxNotTreated',$contact->id)}}" method="POST" class="float-right ">
                                                                @csrf
                                                                {{ method_field('POST') }}
                                                                <button type="submit" class="btn btn-default text-danger float-right " data-toggle="tooltip" data-placement="bottom" title=" Set to Not Treated"><i class="fa fa-times" aria-hidden="true"></i></button>
                                                            </form>
                                                        @endif
                                                       
                                                        @if ($contact->status == 'In Progress')
                                                            {{-- <form action="{{route('inboxProgress',$contact->id)}}" method="POST" class="float-right ">
                                                                @csrf
                                                                {{ method_field('POST') }} --}}
                                                                <button type="submit" class="btn btn-warning disabled float-right "data-toggle="tooltip" data-placement="bottom" title="In Progress"><i class="fa fa-tools" aria-hidden="true"></i></button>
                                                            {{-- </form> --}}
                                                           
                                                        @else
                                                            <form action="{{route('inboxProgress',$contact->id)}}" method="POST" class="float-right ">
                                                                @csrf
                                                                {{ method_field('POST') }}
                                                                <button type="submit" class="btn btn-default text-warning float-right " data-toggle="tooltip" data-placement="bottom" title="Set to Progress"><i class="fa fa-tools" aria-hidden="true"></i></button>
                                                            </form>
                                                        @endif

                                                        @if ($contact->status == 'Treated')
                                                        
                                                            <button type="submit" class="btn btn-success disabled float-right" data-toggle="tooltip" data-placement="bottom" title="Treated"><i class="fa fa-check" aria-hidden="true"></i></button>
                                                       
                                                        @else
                                                        <form action="{{route('inboxTreated',$contact->id)}}" method="POST" class="float-right ">
                                                            @csrf
                                                            {{ method_field('POST') }}
                                                            <button type="submit" class="btn btn-default text-success float-right " data-toggle="tooltip" data-placement="bottom" title="Set to Treated"><i class="fa fa-check" aria-hidden="true"></i></button>
                                                        </form>
                                                        @endif
                                                    </div> 
                                                  
                                                   <br>
                                                   <br>
                                                   <hr>            
                                                   <br>
                                                    {{$contact->comment}}
                                                     <br>
                                                     <br>
                                                     <br>
                                                     <br>
                                                    
                                                     <h6 class="card-subtitle mb-2  text-primary float-left" style="font-size:12px;">  {{ $contact->email }}</h6> 
                                                     <h6 class="card-subtitle mb-2 text-muted text-info float-right" style="font-size:10px;">  {{ $contact->created_at }}</h6> 
                                                     <br>
                                                </div>
                                            </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>






  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<script>
    function goBack() {
      window.history.back();
    }
    </script>

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection