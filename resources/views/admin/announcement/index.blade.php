@extends('layouts.app')

@section('content')





<div class="container-fluid">
    <div class="row justify-content-center m-0">
        <div class="col-sm-2 p-0">
        
             <!-- Sidebar -->
             <div class="bg-light border" id="sidebar-wrapper">
                  
                <div class="list-group list-group-flush">
                  <a href="#" class="list-group-item list-group-item-action bg-dark text-white">ADMIN</a>
                <a href="{{ route('admin.users.index') }}" class="list-group-item list-group-item-action bg-light font-weight-bold">USERS</a>
                  {{-- <a href="{{ route('admin.group.index') }}" class="list-group-item list-group-item-action bg-light font-weight-bold">GROUPS</a> --}}
                  <a href="{{ route('admin.announcement.index') }}" class="list-group-item list-group-item-action bg-success text-white font-weight-bold">ANNOUNCEMENT</a>
                  <a href="{{ route('admin.registration.index') }}" class="list-group-item list-group-item-action bg-light font-weight-bold">REGISTRATION</a>
                  <a href="{{ route('inbox') }}" class="list-group-item list-group-item-action bg-light font-weight-bold">INBOX
                    @php
                      $count = DB::table('contacts')->where('status','=','Not Treated')->count();
                    @endphp
                    @if ($count !== 0)
                      <span class="badge badge-pill badge-danger ml-3">
                        {{ $count }}
                      </span>
                    @endif
                  </a>
                  <a href="{{ route('quota') }}" class="list-group-item list-group-item-action bg-light font-weight-bold">QUOTA</a>
                 
                </div>
              </div>
              <!-- /#sidebar-wrapper -->


        </div>

       

        <div class="col-md-10">
            <div class="row mb-0">
           {{-- breadcrumb --}}

                  <nav aria-label="breadcrumb col-sm-5">
                      <ol class="breadcrumb ">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                        <li class="breadcrumb-item"><a href="#">Configuration</a></li>
                        <li class="breadcrumb-item "><a href="#">Admin</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Announcements</li>
                      </ol>
                    </nav>
                    <div class="col-sm-3"> 
                        {{-- <div class="btn-group" role="group">
                            <button id="btnGroupDrop1" type="button" class="btn btn-default bg-light dropdown-toggle  p-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-cog    "></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                              <a class="dropdown-item" href="#">Dropdown link</a>
                              <a class="dropdown-item" href="#">Dropdown link</a>
                            </div>
                          </div> --}}

                          <div class="btn-group" role="group" aria-label="Third group">
                              <button type="button" class="btn btn-default bg-light p-2"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                            </div>
                    </div>

            </div>

            <div class="card mt-3">

             

                <div class="card-header bg-light text-dark font-weight-bolder"><h3 class="font-weight-bolder float-left"> Announcements </h3>
                    <div class="float-right">
                        <a href="{{ route('admin.announcement.create') }}" class="btn btn-primary ">  <i class="fa fa-plus" aria-hidden="true"></i> Create announcement</a>
                    </div>

                </div>

                <div class="card-body">
                    @if ($message = Session::get('success'))
                            
                    <div class="alert alert-success alert-block">
    
                        <button type="button" class="close" data-dismiss="alert">×</button>
    
                        <strong>{{ $message }}</strong>
    
                    </div>
    
                    @endif
                       <div class="row ">
                        
                            @foreach ($announcements as $announcement)
                            <div class="col-sm-4">
                              <div class="card shadow hover">
                                  
                                  <div class="card-body">
                                     
                                    <h5 class="card-title font-weight-bolder small">{{ $announcement->tittle }}</h5>
                                    <h6 class="card-subtitle mb-2 text-muted text-info" style="font-size:10px;">Created On <br> {{ $announcement->created_at }}</h6> <hr>
                                    <p class="card-text">{{ str_limit($announcement->message, '41') }}</p> <hr>
                                      <a href="{{route('admin.announcement.edit',$announcement->id)}}" class="card-link">
                                        <button type="button" class="btn btn-default text-dark float-left mr-1" style="padding:5px;"> 
                                          <i class="fas fa-edit "></i> Edit
                                        </button>
                                      </a>
                                    <a href="#" class="card-link"> 
                                      <form action="{{route('admin.announcement.destroy',$announcement)}}" method="post" class="float-left">
                                        @csrf
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="btn btn-default text-danger" style="padding:5px;"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                    </form>
                                  </a>
                                 

  
                                  <a href="{{route('admin.notification.show',$announcement->id)}}" class="card-link float-right">
                                      <button type="button" class="btn btn-primary float-left mr-1" style="padding:5px;"> 
                                        <i class="fas fa-bullhorn "></i>  Broadcast 
                                      </button>
                                    </a>
                                  </div>
                                </div>
                              </div>
                          @endforeach
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>


@endsection