@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-4">
              
            <div class="card-header bg-success text-white "> Edit Announcement:
                <div class="float-right">
                    <a href="{{ route('admin.announcement.index') }}" class="btn btn-outline-light btn-sm"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                </div>  
            </div>

                <div class="card-body">

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <p>
                            <strong>Input Error!</strong>
                        </p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
               
                   
               
                    <form action="{{route('admin.announcement.update',$announcement->id)}}" method="post">
                        {{ csrf_field() }}

                        {{ method_field('PUT') }}
                        

                        <div class="form-group">
                            <input type="text" class="form-control" id="tittle" name="tittle" value="{{ $announcement->tittle }}">
                        </div>

                        <div class="form-group">
                                <input type="text" class="form-control" id="sender" name="sender" value="{{ $announcement->sender }}">
                            </div>
                 
                         <div class="form-group">
                            <label for="message">Message Body </label>
                            <textarea class="form-control" id="message" name="message" rows="5">{{ $announcement->message }}</textarea>
                        </div>
                 
                        <button type="submit" class="btn btn-success float-right"><i class="fa fa-plus" aria-hidden="true"> </i> Update</button>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

@endsection