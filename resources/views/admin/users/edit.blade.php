@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
             
            <div class="card-header bg-light text-center text-dark font-weight-bold">  Edit User Role for: {{ $user->firstname }} {{ $user->lastname }}</div>

                <div class="card-body">

                    <form action="{{route('admin.users.update',$user)}}" method="post">
                      @csrf
                        {{ method_field('PUT') }}

                        <div class="row justify-content-center mb-3">
                          <img class="rounded-circle" height="150px" width="150px" src="/storage/avatars/{{ $user->avatar }}" />
                        </div>

                        <div class="row justify-content-center mb-3 text-primary font-weight-bolder">
                          {{ implode(', ', $user->roles()->get()->pluck('name')->toArray()) }}
                        </div>
                      <div class="row justify-content-center">
                          @foreach ($roles as $role)
                        
                          <div class="custom-control bg-white custom-switch col-sm-3 border border-primary m-4 text-center pt-3 pb-3 bg-light" style=" border-radius:6px;">
                              <input type="checkbox" name="roles[]" class="custom-control-input" id="{{$role->id}}" value="{{$role->id}}">
                              <label class="custom-control-label" for="{{$role->id}}">{{$role->name}}</label>
                            </div>
                              {{-- <div class="form-check col-sm-4 border m-3 text-center pt-3 pb-2 bg-light">
                              <input type="checkbox" name="roles[]" id="" value="{{$role->id}}">
                              <label for="checkbox" class="font-weight-bold">{{$role->name}}</label>
                              </div> --}}
                          
                          @endforeach
                      </div>

                      <div class="row justify-content-center mt-3 mb-4">
                        <button type="submit" class="btn btn-success">Update</button>
                       
                        <a href="{{route('admin.users.index')}}" class="btn btn-outline-default text-danger float-right">Cancel</a>
                     
                      </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

@endsection