@extends('layouts.app')

@section('content')





<div class="container-fluid">
    <div class="row justify-content-center m-0">
        <div class="col-sm-2 p-0">
        
                  <!-- Sidebar -->
             <div class="bg-light border" id="sidebar-wrapper">
                  
                <div class="list-group list-group-flush">
                  <a href="#" class="list-group-item list-group-item-action bg-dark text-white">ADMIN</a>
                  <a href="{{ route('admin.users.index') }}" class="list-group-item list-group-item-action bg-light font-weight-bold">USERS</a>
                  {{-- <a href="{{ route('admin.group.index') }}" class="list-group-item list-group-item-action bg-light font-weight-bold">GROUPS</a> --}}
                  <a href="{{ route('admin.announcement.index') }}" class="list-group-item list-group-item-action bg-light font-weight-bold">ANNOUNCEMENT</a>
                  <a href="{{ route('admin.registration.index') }}" class="list-group-item list-group-item-action bg-success text-white font-weight-bold">REGISTRATION</a>
                  <a href="{{ route('inbox') }}" class="list-group-item list-group-item-action bg-light font-weight-bold">INBOX
                    @php
                      $count = DB::table('contacts')->where('status','=','Not Treated')->count();
                    @endphp
                    @if ($count !== 0)
                      <span class="badge badge-pill badge-danger ml-3">
                        {{ $count }}
                      </span>
                    @endif
                  </a>
                  <a href="{{ route('quota') }}" class="list-group-item list-group-item-action bg-light font-weight-bold">QUOTA</a>
                 
                </div>
              </div>
              <!-- /#sidebar-wrapper -->




        </div>
        <div class="col-md-10">
            <div class="row mb-0">
                {{-- breadcrumb --}}
     
                       <nav aria-label="breadcrumb col-sm-5">
                           <ol class="breadcrumb ">
                             <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                             <li class="breadcrumb-item"><a href="#">Configuration</a></li>
                             <li class="breadcrumb-item "><a href="#">Admin</a></li>
                             <li class="breadcrumb-item active" aria-current="page">Registration</li>
                           </ol>
                         </nav>
                         <div class="col-sm-3"> 
                            
     
                               <div class="btn-group" role="group" aria-label="Third group">
                                   <button type="button" class="btn btn-default bg-light p-2"><i class="fa fa-sync-alt" aria-hidden="true"></i></button>
                                 </div>
                         </div>
     
                 </div>
                 {{-- breadcrumb --}}
            <div class="card mt-3">


                <div class="card-header bg-light text-dark font-weight-bolder"> <h3 class="font-weight-bolder">Registered Users</h3></div>

                <div class="card-body  table-responsive">
                 
                       
                 

                   <table id="example" class="table table-borderless">
                                  @if ($message = Session::get('success'))
                            
                                            <div class="alert alert-success alert-block">
                            
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                            
                                                <strong>{{ $message }}</strong>
                            
                                            </div>
                            
                                    @endif
                                        @if ($message = Session::get('problem'))
                            
                                        <div class="alert alert-success alert-block">
                        
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                        
                                            <strong>{{ $message }}</strong>
                        
                                        </div>
                        
                                    @endif
                            
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                    <thead>
                      <tr class="border-1 small">
                       
                        <th scope="col" class="pl-0">First Name</th>
                        <th scope="col" class="pl-0">Last Name</th>
                        <th scope="col" class="pl-0">Email</th>
                        <th scope="col" class="pl-0">Username</th>
                        <th scope="col" class="pl-0">Date of Registration</th>
                        <th scope="col" class="pl-0">Actions</th>
                      
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr class="small">
                                {{-- <th scope="row" class=""> {{ $user->id }}</th> --}}
                                <td class="pl-0"> {{ ucfirst( $user->firstname) }}</td>
                                <td class="pl-0"> {{ ucfirst($user->lastname) }}</td>
                                <td class="pl-0"> {{ $user->email }}</td>
                                <td class="pl-0"> {{ $user->username }}</td>
                                <td class="pl-0"> {{ $user->created_at }}</td>

                                @if ($user != Auth::user())
                                      @if ($user->activated == 1)
                                        <td class="pl-0">

                                            <a href="{{route('admin.registration.edit',$user->id)}} " class=" float-"><button type="button" class="btn btn-default small text-danger  float-left mr-1 p-0" style="padding:5px; "> <i class="fa fa-user-times" aria-hidden="true"></i> Deactivate&nbsp;</button></a>
                                          
                                        </td>
                                      
                                      @else
                                        <td class="pl-0"> 

                                            <a href="{{route('admin.registration.edit',$user->id)}}"class=""><button type="button" class="btn btn-default small text-success float-left mr-1 p-0" style="padding:5px;"> <i class="fas fa-user-check "></i> Activate</button></a>
                                          
                                        </td>

                                      @endif
                                  @endif
                                        
                              
                               
                               
                            </tr>
                      @endforeach

                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
  $(document).ready(function() {
    $('#example').DataTable( {
        "paging":   true,
        "ordering": true,
        "info":     false,
        language: {
          searchPlaceholder: "Search"
      } 
    },
   );

  });

  </script>

<style>
  input,select{
      border:1px solid #00968859 !important;
      padding: 20px !important;
  }
  select{
      border:1px solid #3490dc !important;
      padding: 10px !important;
      min-height: 45px !important;
      min-width: 100px !important;
      color: black !important;
      
  }
  #example_filter > label > input{
      /* background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIgICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgICB2ZXJzaW9uPSIxLjEiICAgaWQ9InN2ZzQ0ODUiICAgdmlld0JveD0iMCAwIDIxLjk5OTk5OSAyMS45OTk5OTkiICAgaGVpZ2h0PSIyMiIgICB3aWR0aD0iMjIiPiAgPGRlZnMgICAgIGlkPSJkZWZzNDQ4NyIgLz4gIDxtZXRhZGF0YSAgICAgaWQ9Im1ldGFkYXRhNDQ5MCI+ICAgIDxyZGY6UkRGPiAgICAgIDxjYzpXb3JrICAgICAgICAgcmRmOmFib3V0PSIiPiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+ICAgICAgICA8ZGM6dHlwZSAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4gICAgICAgIDxkYzp0aXRsZT48L2RjOnRpdGxlPiAgICAgIDwvY2M6V29yaz4gICAgPC9yZGY6UkRGPiAgPC9tZXRhZGF0YT4gIDxnICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDMwLjM2MjIpIiAgICAgaWQ9ImxheWVyMSI+ICAgIDxnICAgICAgIHN0eWxlPSJvcGFjaXR5OjAuNSIgICAgICAgaWQ9ImcxNyIgICAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNjAuNCw4NjYuMjQxMzQpIj4gICAgICA8cGF0aCAgICAgICAgIGlkPSJwYXRoMTkiICAgICAgICAgZD0ibSAtNTAuNSwxNzkuMSBjIC0yLjcsMCAtNC45LC0yLjIgLTQuOSwtNC45IDAsLTIuNyAyLjIsLTQuOSA0LjksLTQuOSAyLjcsMCA0LjksMi4yIDQuOSw0LjkgMCwyLjcgLTIuMiw0LjkgLTQuOSw0LjkgeiBtIDAsLTguOCBjIC0yLjIsMCAtMy45LDEuNyAtMy45LDMuOSAwLDIuMiAxLjcsMy45IDMuOSwzLjkgMi4yLDAgMy45LC0xLjcgMy45LC0zLjkgMCwtMi4yIC0xLjcsLTMuOSAtMy45LC0zLjkgeiIgICAgICAgICBjbGFzcz0ic3Q0IiAvPiAgICAgIDxyZWN0ICAgICAgICAgaWQ9InJlY3QyMSIgICAgICAgICBoZWlnaHQ9IjUiICAgICAgICAgd2lkdGg9IjAuODk5OTk5OTgiICAgICAgICAgY2xhc3M9InN0NCIgICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjY5NjQsLTAuNzE3NiwwLjcxNzYsMC42OTY0LC0xNDIuMzkzOCwyMS41MDE1KSIgICAgICAgICB5PSIxNzYuNjAwMDEiICAgICAgICAgeD0iLTQ2LjIwMDAwMSIgLz4gICAgPC9nPiAgPC9nPjwvc3ZnPg==); */
      background-image: url(/images/icons/search.svg);
      background-repeat: no-repeat;
      background-color: #fff;
      background-size: 18px;
      background-position: 167px 11px !important;
      font-weight: bold;
  }
  #example_filter > label{
    color: #fff;
  }
</style>
@endsection

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>