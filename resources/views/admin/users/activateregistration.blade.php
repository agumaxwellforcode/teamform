@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-4">
              
            <div class="card-header bg-light text-primary font-weight-bold ">Activate User: {{ $user->firstname }} {{ $user->lastname }}</div>

                <div class="card-body">
                    
                    <form action="{{route('admin.registration.update',$user)}}" method="post">
                      @csrf
                        {{ method_field('PUT') }}

                        <div class="row justify-content-center mb-3">
                          <img class="rounded-circle" height="150px" width="150px" src="/storage/avatars/{{ $user->avatar }}" />

                        
                        </div>

                        {{-- <div class="row justify-content-center mb-3 text-primary font-weight-bolder">
                          {{  $user->firstname }} - {{  $user->lastname }}-{{  $user->activated }}
                        </div> --}}
                        @if($user->activated == 1 )
                          <input type="hidden" name="activate" value="0" required>
                        @else
                          <input type="hidden" name="activate" value="1" required>
                        
                        @endif
                      

                        <div class="row justify-content-center  mb-2">

                        <div class="table-responsive col-sm-7">
                          <table class="table col-sm-12  table-bordered table-condensed table p-0 m-0 container-fluid">
                            
                              <tbody class="">
                                <tr class="fill">
                                  <td >First Name</td>
                                  <td class="text-primary"> {{  $user->firstname }}</td>
                                </tr>
                                <tr>
                                  <td > Last Name</td>
                                  <td class="text-primary">{{  $user->lastname }}</td>
                                </tr>
                                <tr>
                                    <td >Email</td>
                                    <td class="text-primary">{{  $user->email }}</td>
                                  </tr>
                                  <tr>
                                    <td > Username</td>
                                    <td class="text-primary">{{  $user->username }}</td>
                                  </tr>
                                  <tr>
                                      <td > Date Reg.</td>
                                      <td class="text-primary">{{  $user->created_at }}</td>
                                    </tr>
                                  
                              </tbody>
                          </table>
                        </div>
                      </div>
                      <div class="row justify-content-center mt-3 mb-3">
                        <div class="col-md-7 ">
                          @if($user->activated == 1 )
                            <button type="submit" class="btn btn-danger">Deactivate</button>
                          @else
                            <button type="submit" class="btn btn-success">Activate</button>
                          
                          @endif
                       
                          
                        <a href="{{route('admin.registration.index')}}" class="btn btn-outline-primary float-right">Cancel</a>
                        </div>
                      </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

@endsection