@extends('layouts.head')

@section('content')
<div class="container-fluid pt-5">
   
        

            <div class="row justify-content-center" >
                    <div class="col-md-11 " style="height: 95vh;">
                        <div class="card border-bottom-0 ">
                            <div class="card-header font-weight-bold text-success">
                                <img class="" height="60px" width="60px" src="/images/logo1.jpeg" /> &nbsp; Print Comments on @if (count($comments) > 0 ){{$comments[0]->filename}}. @endif 
                                <button onclick="window.print()" class="btn btn-default mt-3 text-primary float-right" data-toggle="tooltip" data-placement="bottom" title="Print"><i class="fa fa-print" aria-hidden="true"></i></button>
                                <button onclick="goBack()" class="btn btn-default mt-3 text-danger float-right" data-toggle="tooltip" data-placement="bottom" title="Go Back"><i class="fa fa-arrow-alt-circle-left" aria-hidden="true"></i></button>
                            </div>
                            <div class="card-body">
                                @if (count($comments) > 0 )
                                @foreach ($comments as $comment)
                                    <div class="card mb-3 border-bottom" style="width: ; border-radius:2px;">
                                        <div class="card-body">
                                            @php
                                                $commenter=DB::table('users') ->where('id','=',$comment->commenter_id) ->first();
                                            @endphp
                                            <img class="rounded-circle float-right border-2 border-primary" height="30px" width="30px" src="/storage/avatars/{{ $commenter->avatar }}" />
                                        <h6 class="card-title font-weight-bold">{{ $commenter->firstname }} {{ $commenter->lastname }}</h6>
                                        <h6 class="card-subtitle mb-2 text-muted small">{{ $comment->created_at }}</h6>
                                        <p class="card-text font-weight-bold text-dark">{{$comment->comment}}.</p>
        
        
        
        
                                        <p class="text-success font-weight-bold">Replies</p>
        
                                        @php
                                            $replies=DB::table('replies') ->where('comment_id','=',$comment->id)->orderBy('id','asc')->get();
                                        @endphp
                                        @if (count($replies) > 0 )
                                            @foreach ($replies as $reply)
                                                <div class="card mb-3" style="width: ; border-radius:2px;">
                                                    <div class="card-body">
                                                        @php
                                                            $replyer = DB::table('users') ->where('id','=',$reply->replyer_id)->first();
                                                            //  dd($replyer->avatar)
                                                        @endphp
                                                        <img class="rounded-circle float-right border-2 border-primary" height="30px" width="30px" src="/storage/avatars/{{ $replyer->avatar }}" />
                                                    <h6 class="card-title font-weight-bold">{{ $replyer->firstname }} {{ $replyer->lastname }}</h6>
                                                    <p class="card-text small font-weight-bold text-dark">{{$reply->reply}}.</p>
                    
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
        
        
                                        {{-- <a href="#collapseReply{{$comment->id}}" class="card-link text-success small font-weight-bold" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample">Reply comment</a> --}}
                                        </div>
                                    </div>
                                @endforeach
                            @else 
                            <div class="card w-100">
                                <div class="card-body">
                                  <h5 class="card-title">No Comment</h5>
                                  <p class="card-text">Sorry, No comment has been made on this file.</p>
                                  <button onclick="goBack()" class="btn btn-primary">Back</button>
                                </div>
                              </div>
                            @endif

                            </div>
                          </div>
                    </div>

        </div>




        
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script>
    function goBack() {
      window.history.back();
    }
    </script>
@endsection