@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header font-weight-bold text-primary">
                  


                    {{$folder->path}}/




                       
                        <div class="float-right">
                                <a onclick="goBack()" class="btn btn-default text-danger "> </i>  Back</a>
                            </div>  

                </div>

                @if ($message = Session::get('success'))
    
                <div class="alert alert-success text-center alert-block">

                    <button type="button" class="close" data-dismiss="alert">×</button>

                    <strong>{{ $message }}</strong>

                </div>

            @endif
            @if ($message = Session::get('error'))
    
            <div class="alert alert-danger text-center alert-block">

                <button type="button" class="close" data-dismiss="alert">×</button>

                <strong>{{ $message }}</strong>

            </div>

        @endif
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{-- You are logged in!
                    <a href="{{route('push')}}">Uplaod</a> --}}

                    <div class="row justify-content-center">

                        <div class="col-sm-8 mt-3">
                                <form action="{{ route('subfolderCreate') }}"  method="POST">
                                    @csrf
                                    {{ method_field('POST') }}
{{-- {{dd($user->id)}} --}}
                                    {{-- <input type="hidden" name="user_id" value="{{$user->id}}"> --}}
                                    <input type="hidden" name="folder_id" value="{{$folder->id}}">
                                    <input type="hidden" name="folder_path" value="{{$folder->path}}">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1" class="font-weight-bold">Folder Name</label>
                                            <input type="text" name="name" class="form-control font-weight-bold" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter the folder name" required>
                                        
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1" class="font-weight-bold">Access level</label>
                                            <select class="form-control font-weight-bold" id="exampleFormControlSelect1" name="permision">
                                                {{-- <option value="#">Who has access to this file?</option> --}}
                                                <option value="all">Everyone</option>
                                                <option value="admin">Admin Only</option>
                                            </select>
                                        </div>
                                    <br>
                                        <button type="submit" class="btn btn-primary float-right mt-1"> <i class="fa fa-plus" aria-hidden="true"></i> Create</button>
                                    </form>
                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    function goBack() {
      window.history.back();
    }
    </script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

@endsection
