@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header font-weight-bold">
                    <!-- Example single danger button -->
                        <div class="btn-group">
                                <button type="button" class="btn btn-primary dropdown-toggle font-weight-bolder" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <i class="fa fa-plus" aria-hidden="true"></i> Add New 
                                </button>
                                <div class="dropdown-menu shadow-lg " id="">
                                  <a class="dropdown-item font-weight-bold text-success" href="{{route('push')}}"> <i class="fa fa-file" aria-hidden="true"></i> File</a>
                                  {{-- <a class="dropdown-item font-weight-bold text-success" href="{{route('folder.create')}}"> <i class="fa fa" aria-hidden="true"></i> Folder</a> --}}
                                
                                </div>
                            </div>
                        <div class="float-right">
                          
                          <form class="form-inline float-left mr-2">
                           
                            <div class="input-group ">
                                <input type="text" name="search" id="search" class="form-control border-primary" placeholder="Search" aria-label="Search" aria-describedby="button-addon2">
                                <div class="input-group-append">
                                  <button class="btn btn-outline-primary bg-white" style="border-left:0px; " type="submit" id="button-addon2"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </div>
                              </div>
                        
                        </form>
                          <div class="btn-group " id="bg" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-primary text-white"><a href="#"><i class="fa fa-list text-white" aria-hidden="true"></i></a></button>
                            <button type="button" class="btn btn-primary text-white"><a href="{{ route('home') }}"><i class="fa fa-th text-white" aria-hidden="true"></i></a></button>
                          </div>
                          
                            </div>  

                </div>
                        <div class="table-responsive pt-3 float-right" id="searchTable">
                          {{-- <h6 align="center" class=" font-weight-bolder">Search result : <span id="total_records"></span></h6> --}}
                          <table class="table table-borderless bg-white col-sm-12 col-md-10 col-lg-4 float-right">
                            <thead>
                              <tr>
                               <th class="bg-white"> <h6 align="left" class=" font-weight-bolder  text-success"></h6></th>
                              </tr>
                              <tr>
                                <th class=""> <h6 align="left" class=" font-weight-bolder  text-success"></h6></th>
                               </tr>
                             </thead>
                            <tbody class="bg-white shadow-lg">
                                
                            </tbody>
                          </table>
                        </div>
                                @if (session('status'))
                                    <div class="alert alert-success col-sm-12" role="alert">
                                        {{ session('status') }}
                                        
                                          <button type="button" class="close" data-dismiss="alert">×</button>

                                    </div>
                                @endif

                <div class="card-body">



{{-- 
                  <ul class="nav nav-tabs  muli " id="myTab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active muli small text-success" id="home-tab" data-toggle="tab" href="#pf" role="tab" aria-controls="pf" aria-selected="true">Public Folders</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link muli small text-success" id="profile-tab" data-toggle="tab" href="#sf" role="tab" aria-controls="sf" aria-selected="false">Private Folders</a>
                    </li>
                   </ul>
                  <div class="tab-content pt-2" id="myTabContent">
                    <div class="tab-pane fade show active pt-1" id="pf" role="tabpanel" aria-labelledby="home-tab">
                            PUBLIC FOLDERS
                          <div class="col-sm-12">
                            <h6  class="font-weight-bolder pl-0 text-right" style="font-size:10px">PUBLIC FOLDERS</h6>
                            @if(count($publicFolders) > 0)
                            <div class="row p-1 ">
                              
                              @foreach ($publicFolders as $publicFolder)
                              <div class="col-sm-1 col-md-3 col-lg-1 col-xl-1 m-2 p-4 bg-white text-center ">
                                  <a href="{{route('subhome',$publicFolder)}}" class="{{$publicFolder->id}}"><img class="img img-fluid rounded" src="/images/icons/folder.svg" alt="" srcset=""></a>
                                    <p class="text-center" style="font-size:10px;">{{$publicFolder->name}}</p>
                              
                                      <div class="dropdown-menu dropdown-menu-sm bg-white fileRightClick" id="{{$publicFolder->id}}" style="min-width:auto !important; position:absolute">
                                        <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('subhome',$publicFolder)}}">Open </a>
                                        
                                        
                                        
                                        @can('manage-users')
                                            <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-danger" href="{{route('deleteFolder',$publicFolder)}}">Delete <i class="fa fa-trash" aria-hidden="true"></i></a></a>
                                        @endcan
                                      </div>
                                      <script>
                    
                                        $('.{{$publicFolder->id}}').on('contextmenu', function(e) {
                                        
                                          $('#{{$publicFolder->id}}').css({
                                            display: "block",
                                          
                                          }).addClass("show");
                                          return false; //blocks default Webbrowser right click menu
                    
                                        })
                                        $('.row').on("click", function() {
                                            $('#{{$publicFolder->id}}').removeClass("show").hide();
                                        });
                                        $('.row').on("contextmenu", function() {
                                                $('#{{$publicFolder->id}}').removeClass("show").hide();
                                            });
                                        
                                        $('#{{$publicFolder->id}} a').on("click", function() {
                                          $(this).parent().removeClass("show").hide();
                                        });
                                        
                                    </script>
                            
                            
                            </div>
                              @endforeach
                              
                              
                          </div>
                          @else
                          <div class="row justify-content-center">
                            
                              <h4 class=" font-weight-bolder text-light" style="color:dimgrey">No Public Folder</h4> 
                        
                            
                          </div>
                        @endif
                      </div>
                    </div>

                    <div class="tab-pane fade pt-1" id="sf" role="tabpanel" aria-labelledby="profile-tab">
                       {{-- PRIVATE FOLDERS --}}
                          {{-- <div class="col-sm-12 ">
                            <h6  class="font-weight-bolder text-right" style="font-size:10px">MY PRIVATE FOLDERS</h6>
                            @if(count($privateFolders) > 0)
                            <div class="row p-1">

                            
                                
                                @foreach ($privateFolders as $privateFolder)
                                
                                <div class="col-sm-1 col-md-3 col-lg-1 col-xl-1 m-2 p-4 bg-white text-center">
                                    <a href="{{route('subhome',$privateFolder)}}" class="{{$privateFolder->id}}"><img class="img img-fluid rounded" src="/images/icons/folder.svg" alt="" srcset=""></a>
                                <p class="text-center" style="font-size:10px;">{{$privateFolder->name}}</p>
                              
                              


                                            <div class="dropdown-menu dropdown-menu-sm bg-white fileRightClick" id="{{$privateFolder->id}}" style="min-width:auto !important;">
                                              <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('subhome',$privateFolder)}}">Open </a>
                                              <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-dark" href="{{route('share',$privateFolder)}}">Share <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                              <a class="dropdown-item pl-3 pr-3 font-weight-bolder text-danger" href="{{route('deleteFolder',$privateFolder)}}">Delete <i class="fa fa-trash" aria-hidden="true"></i></a></a>
                                            </div>
                                            <script>
                          
                                              $('.{{$privateFolder->id}}').on('contextmenu', function(e) {
                                              
                                                $('#{{$privateFolder->id}}').css({
                                                  display: "block",
                                                
                                                }).addClass("show");
                                                return false; //blocks default Webbrowser right click menu
                          
                                              })
                                              $('.row').on("click", function() {
                                                  $('#{{$privateFolder->id}}').removeClass("show").hide();
                                              });
                                              $('.row').on("contextmenu", function() {
                                                      $('#{{$privateFolder->id}}').removeClass("show").hide();
                                                  });
                                              
                                              $('#{{$privateFolder->id}} a').on("click", function() {
                                                $(this).parent().removeClass("show").hide();
                                              });
                                              
                                          </script>




                              </div>
                                @endforeach

                              
                              
                            </div>
                            @else
                            <div class="row justify-content-center">
                            
                                <h4 class=" font-weight-bolder text-light" style="color:dimgrey">No Private Folder</h4> 
                          
                            
                            </div>
                          @endif
                        </div>
                    </div>
                    
                  </div>  --}}












                  <ul class="nav nav-tabs  muli" id="myTab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active muli small text-success" id="home-tab" data-toggle="tab" href="#pff" role="tab" aria-controls="pff" aria-selected="true">My Space</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link muli small text-success" id="profile-tab" data-toggle="tab" href="#shf" role="tab" aria-controls="shf" aria-selected="false">Shared Files</a>
                    </li>
                    {{-- <li class="nav-item">
                      <a class="nav-link muli small text-success" id="profile-tab" data-toggle="tab" href="#sff" role="tab" aria-controls="sff" aria-selected="false">Private Files</a>
                    </li> --}}
                    
                   </ul>
                  <div class="tab-content pt-2" id="myTabContent">
                    <div class="tab-pane fade show active pt-1" id="pff" role="tabpanel" aria-labelledby="home-tab">
                      <h6  class="font-weight-bolder text-right" style="font-size:10px">MY SPACE / PRIVATE FILES</h6>
                      @if(count($publicFiles) > 0)
                        <div class="row p-1">
                      
                          @foreach ($publicFiles as $publicFile)
                            <div class="col-sm-1 col-md-2 col-lg-1   p-3 pl-0 pr-0   text-center  ">
                            {{-- {{dd($extension)}}  $extension =='docx'--}}
                            @if(pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='docx' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='DOCX' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='doc' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='DOC' )
                            <a href="{{route('text',$publicFile)}}" class="{{$publicFile->id}}"><img class="img img-fluid rounded" style="object-fit: cover; height:auto; width:auto" src="/images/icons/docs.svg" alt="" srcset=""></a>
                               <span class="text-center" style="font-size:10px;">{{str_limit($publicFile->filename, '12')}}</span>
                          

                               <div class="dropdown-menu shadow-lg border-0 dropdown-menu bg-white p-0 fileRightClick" id="{{$publicFile->id}}" >
                                  <a class="dropdown-item pl-0 pr-0 text-center" style=" background-color:  #00968859" href="{{route('text',$publicFile)}}"> <img class="img img-fluid rounded" style="object-fit: cover; height:70%; width:30%" src="/images/icons/docs.svg" alt="" srcset=""></a>
                                  <a class="dropdown-item text-primary" href="{{route('text',$publicFile)}}"> <i class="fa fa-folder-open" aria-hidden="true"></i> Open </a>
                                  <a class="dropdown-item text-primary" href="{{route('share',$publicFile)}}"> <i class="fa fa-share" aria-hidden="true"></i> Share</a>
                                <a class="dropdown-item text-primary" href="{{route('deleteFile',$publicFile)}}"> <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                </div>
                                <script>
              
                                  $('.{{$publicFile->id}}').on('contextmenu', function(e) {
                                   
                                    $('#{{$publicFile->id}}').css({
                                      display: "block",
                                      position: "absolute",
                                      top: "0",
                                      left: "0",
                                      "z-index": "20",
                                     
                                    }).addClass("show");
                                    return false; //blocks default Webbrowser right click menu
              
                                  })
                                  $('.row').on("click", function() {
                                      $('#{{$publicFile->id}}').removeClass("show").hide();
                                  });
                                  $('.row').on("contextmenu", function() {
                                          $('#{{$publicFile->id}}').removeClass("show").hide();
                                      });
                                  $('.container').on("contextmenu", function() {
                                      $('#{{$publicFile->id}}').removeClass("show").hide();
                                  });
                                  
                                  $('#{{$publicFile->id}} a').on("click", function() {
                                    $(this).parent().removeClass("show").hide();
                                  });
                                  
                              </script>






                       






                           
                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='PNG' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='png' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='JPG' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='jpg' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='JPEG' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='jpeg')
                            <a href="{{route('image',$publicFile)}}" class="{{$publicFile->id}}"><img class="img img-fluid rounded" style="object-fit: cover; height:auto; width:auto"  src="/images/icons/image.svg" alt="" srcset=""></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($publicFile->filename, '12')}}</span>


                            <div class="dropdown-menu shadow-lg border-0 dropdown-menu bg-white p-0 fileRightClick" id="{{$publicFile->id}}" >
                              <a class="dropdown-item pl-0 pr-0 text-center" style=" background-color:  #00968859" href="{{route('text',$publicFile)}}"> <img class="img img-fluid rounded" style="object-fit: cover; height:70%; width:30%" src="/images/icons/image.svg" alt="" srcset=""></a>
                              <a class="dropdown-item text-primary" href="{{route('text',$publicFile)}}"> <i class="fa fa-folder-open" aria-hidden="true"></i> Open </a>
                              <a class="dropdown-item text-primary" href="{{route('share',$publicFile)}}"> <i class="fa fa-share" aria-hidden="true"></i> Share</a>
                            <a class="dropdown-item text-primary" href="{{route('deleteFile',$publicFile)}}"> <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                            </div>
                            <script>
          
                              $('.{{$publicFile->id}}').on('contextmenu', function(e) {
                               
                                $('#{{$publicFile->id}}').css({
                                  display: "block",
                                  position: "absolute",
                                  top: "0",
                                  left: "0",
                                  "z-index": "20",
                                 
                                }).addClass("show");
                                return false; //blocks default Webbrowser right click menu
          
                              })
                              $('.row').on("click", function() {
                                  $('#{{$publicFile->id}}').removeClass("show").hide();
                              });
                              $('.row').on("contextmenu", function() {
                                      $('#{{$publicFile->id}}').removeClass("show").hide();
                                  });
                              $('.container').on("contextmenu", function() {
                                  $('#{{$publicFile->id}}').removeClass("show").hide();
                              });
                              
                              $('#{{$publicFile->id}} a').on("click", function() {
                                $(this).parent().removeClass("show").hide();
                              });
                              
                          </script>


                            


                           


                           


                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='pdf' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='PDF')
                            <a href="{{route('pdf',$publicFile)}}" class="{{$publicFile->id}}"><img class="img img-fluid rounded" style="object-fit: cover; height:auto; width:auto" src="/images/icons/pdf.svg" alt="" srcset="" ></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($publicFile->filename, '12')}}</span>


                            <div class="dropdown-menu shadow-lg border-0 dropdown-menu bg-white p-0 fileRightClick" id="{{$publicFile->id}}" >
                              <a class="dropdown-item pl-0 pr-0 text-center" style=" background-color:  #00968859" href="{{route('text',$publicFile)}}"> <img class="img img-fluid rounded" style="object-fit: cover; height:70%; width:30%" src="/images/icons/pdf.svg" alt="" srcset=""></a>
                              <a class="dropdown-item text-primary" href="{{route('text',$publicFile)}}"> <i class="fa fa-folder-open" aria-hidden="true"></i> Open </a>
                              <a class="dropdown-item text-primary" href="{{route('share',$publicFile)}}"> <i class="fa fa-share" aria-hidden="true"></i> Share</a>
                            <a class="dropdown-item text-primary" href="{{route('deleteFile',$publicFile)}}"> <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                            </div>
                            <script>
          
                              $('.{{$publicFile->id}}').on('contextmenu', function(e) {
                               
                                $('#{{$publicFile->id}}').css({
                                  display: "block",
                                  position: "absolute",
                                  top: "0",
                                  left: "0",
                                  "z-index": "20",
                                 
                                }).addClass("show");
                                return false; //blocks default Webbrowser right click menu
          
                              })
                              $('.row').on("click", function() {
                                  $('#{{$publicFile->id}}').removeClass("show").hide();
                              });
                              $('.row').on("contextmenu", function() {
                                      $('#{{$publicFile->id}}').removeClass("show").hide();
                                  });
                              $('.container').on("contextmenu", function() {
                                  $('#{{$publicFile->id}}').removeClass("show").hide();
                              });
                              
                              $('#{{$publicFile->id}} a').on("click", function() {
                                $(this).parent().removeClass("show").hide();
                              });
                              
                          </script>


                         


                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='mp3' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='MP3' )
                            <a href="{{route('audio',$publicFile)}}" class="{{$publicFile->id}}"><img class="img img-fluid rounded" src="/images/icons/mp3.svg" alt="" srcset="" ></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($publicFile->filename, '12')}}</span>


                            <div class="dropdown-menu shadow-lg border-0 dropdown-menu bg-white p-0 fileRightClick" id="{{$publicFile->id}}" >
                              <a class="dropdown-item pl-0 pr-0 text-center" style=" background-color:  #00968859" href="{{route('text',$publicFile)}}"> <img class="img img-fluid rounded" style="object-fit: cover; height:70%; width:30%" src="/images/icons/mp3.svg" alt="" srcset=""></a>
                              <a class="dropdown-item text-primary" href="{{route('text',$publicFile)}}"> <i class="fa fa-folder-open" aria-hidden="true"></i> Open </a>
                              <a class="dropdown-item text-primary" href="{{route('share',$publicFile)}}"> <i class="fa fa-share" aria-hidden="true"></i> Share</a>
                            <a class="dropdown-item text-primary" href="{{route('deleteFile',$publicFile)}}"> <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                            </div>
                            <script>
          
                              $('.{{$publicFile->id}}').on('contextmenu', function(e) {
                               
                                $('#{{$publicFile->id}}').css({
                                  display: "block",
                                  position: "absolute",
                                  top: "0",
                                  left: "0",
                                  "z-index": "20",
                                 
                                }).addClass("show");
                                return false; //blocks default Webbrowser right click menu
          
                              })
                              $('.row').on("click", function() {
                                  $('#{{$publicFile->id}}').removeClass("show").hide();
                              });
                              $('.row').on("contextmenu", function() {
                                      $('#{{$publicFile->id}}').removeClass("show").hide();
                                  });
                              $('.container').on("contextmenu", function() {
                                  $('#{{$publicFile->id}}').removeClass("show").hide();
                              });
                              
                              $('#{{$publicFile->id}} a').on("click", function() {
                                $(this).parent().removeClass("show").hide();
                              });
                              
                          </script>


                          


                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='xlsx' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='XLSX' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='csv' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='CSV' )
                            <a href="{{route('sheet',$publicFile)}}" class="{{$publicFile->id}}"><img class="img img-fluid rounded" src="/images/icons/sheets.svg" alt="" srcset="" ></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($publicFile->filename, '12')}}</span>


                            <div class="dropdown-menu shadow-lg border-0 dropdown-menu bg-white p-0 fileRightClick" id="{{$publicFile->id}}" >
                              <a class="dropdown-item pl-0 pr-0 text-center" style=" background-color:  #00968859" href="{{route('text',$publicFile)}}"> <img class="img img-fluid rounded" style="object-fit: cover; height:70%; width:30%" src="/images/icons/sheets.svg" alt="" srcset=""></a>
                              <a class="dropdown-item text-primary" href="{{route('text',$publicFile)}}"> <i class="fa fa-folder-open" aria-hidden="true"></i> Open </a>
                              <a class="dropdown-item text-primary" href="{{route('share',$publicFile)}}"> <i class="fa fa-share" aria-hidden="true"></i> Share</a>
                            <a class="dropdown-item text-primary" href="{{route('deleteFile',$publicFile)}}"> <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                            </div>
                            <script>
          
                              $('.{{$publicFile->id}}').on('contextmenu', function(e) {
                               
                                $('#{{$publicFile->id}}').css({
                                  display: "block",
                                  position: "absolute",
                                  top: "0",
                                  left: "0",
                                  "z-index": "20",
                                 
                                }).addClass("show");
                                return false; //blocks default Webbrowser right click menu
          
                              })
                              $('.row').on("click", function() {
                                  $('#{{$publicFile->id}}').removeClass("show").hide();
                              });
                              $('.row').on("contextmenu", function() {
                                      $('#{{$publicFile->id}}').removeClass("show").hide();
                                  });
                              $('.container').on("contextmenu", function() {
                                  $('#{{$publicFile->id}}').removeClass("show").hide();
                              });
                              
                              $('#{{$publicFile->id}} a').on("click", function() {
                                $(this).parent().removeClass("show").hide();
                              });
                              
                          </script>







                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='html' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='HTML' )
                            <a href="{{route('sheet',$publicFile)}}" class="{{$publicFile->id}}"><img class="img img-fluid rounded" src="/images/icons/html.svg" alt="" srcset="" ></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($publicFile->filename, '12')}}</span>
                            

                            <div class="dropdown-menu shadow-lg border-0 dropdown-menu bg-white p-0 fileRightClick" id="{{$publicFile->id}}" >
                              <a class="dropdown-item pl-0 pr-0 text-center" style=" background-color:  #00968859" href="{{route('text',$publicFile)}}"> <img class="img img-fluid rounded" style="object-fit: cover; height:70%; width:30%" src="/images/icons/html.svg" alt="" srcset=""></a>
                              <a class="dropdown-item text-primary" href="{{route('text',$publicFile)}}"> <i class="fa fa-folder-open" aria-hidden="true"></i> Open </a>
                              <a class="dropdown-item text-primary" href="{{route('share',$publicFile)}}"> <i class="fa fa-share" aria-hidden="true"></i> Share</a>
                            <a class="dropdown-item text-primary" href="{{route('deleteFile',$publicFile)}}"> <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                            </div>
                            <script>
          
                              $('.{{$publicFile->id}}').on('contextmenu', function(e) {
                               
                                $('#{{$publicFile->id}}').css({
                                  display: "block",
                                  position: "absolute",
                                  top: "0",
                                  left: "0",
                                  "z-index": "20",
                                 
                                }).addClass("show");
                                return false; //blocks default Webbrowser right click menu
          
                              })
                              $('.row').on("click", function() {
                                  $('#{{$publicFile->id}}').removeClass("show").hide();
                              });
                              $('.row').on("contextmenu", function() {
                                      $('#{{$publicFile->id}}').removeClass("show").hide();
                                  });
                              $('.container').on("contextmenu", function() {
                                  $('#{{$publicFile->id}}').removeClass("show").hide();
                              });
                              
                              $('#{{$publicFile->id}} a').on("click", function() {
                                $(this).parent().removeClass("show").hide();
                              });
                              
                          </script>







                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='pptx' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='PPTX' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='PPT' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='ppt' )
                            <a href="{{route('slide',$publicFile)}}" class="{{$publicFile->id}}"><img class="img img-fluid rounded" src="/images/icons/slides.svg" alt="" srcset="" ></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($publicFile->filename, '12')}}</span>

                            <div class="dropdown-menu shadow-lg border-0 dropdown-menu bg-white p-0 fileRightClick" id="{{$publicFile->id}}" >
                              <a class="dropdown-item pl-0 pr-0 text-center" style=" background-color:  #00968859" href="{{route('text',$publicFile)}}"> <img class="img img-fluid rounded" style="object-fit: cover; height:70%; width:30%" src="/images/icons/slides.svg" alt="" srcset=""></a>
                              <a class="dropdown-item text-primary" href="{{route('text',$publicFile)}}"> <i class="fa fa-folder-open" aria-hidden="true"></i> Open </a>
                              <a class="dropdown-item text-primary" href="{{route('share',$publicFile)}}"> <i class="fa fa-share" aria-hidden="true"></i> Share</a>
                            <a class="dropdown-item text-primary" href="{{route('deleteFile',$publicFile)}}"> <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                            </div>
                            <script>
          
                              $('.{{$publicFile->id}}').on('contextmenu', function(e) {
                               
                                $('#{{$publicFile->id}}').css({
                                  display: "block",
                                  position: "absolute",
                                  top: "0",
                                  left: "0",
                                  "z-index": "20",
                                 
                                }).addClass("show");
                                return false; //blocks default Webbrowser right click menu
          
                              })
                              $('.row').on("click", function() {
                                  $('#{{$publicFile->id}}').removeClass("show").hide();
                              });
                              $('.row').on("contextmenu", function() {
                                      $('#{{$publicFile->id}}').removeClass("show").hide();
                                  });
                              $('.container').on("contextmenu", function() {
                                  $('#{{$publicFile->id}}').removeClass("show").hide();
                              });
                              
                              $('#{{$publicFile->id}} a').on("click", function() {
                                $(this).parent().removeClass("show").hide();
                              });
                              
                          </script>







                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='mp4' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='MP4' )
                          
                            <a href="{{route('video',$publicFile)}}" class="{{$publicFile->id}}"><img class="img img-fluid rounded" src="/images/icons/cone.svg" alt="" srcset="" ></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($publicFile->filename, '12')}}</span>

                            <div class="dropdown-menu shadow-lg border-0 dropdown-menu bg-white p-0 fileRightClick" id="{{$publicFile->id}}" >
                              <a class="dropdown-item pl-0 pr-0 text-center" style=" background-color:  #00968859" href="{{route('text',$publicFile)}}"> <img class="img img-fluid rounded" style="object-fit: cover; height:70%; width:30%" src="/images/icons/cone.svg" alt="" srcset=""></a>
                              <a class="dropdown-item text-primary" href="{{route('text',$publicFile)}}"> <i class="fa fa-folder-open" aria-hidden="true"></i> Open </a>
                              <a class="dropdown-item text-primary" href="{{route('share',$publicFile)}}"> <i class="fa fa-share" aria-hidden="true"></i> Share</a>
                            <a class="dropdown-item text-primary" href="{{route('deleteFile',$publicFile)}}"> <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                            </div>
                            <script>
          
                              $('.{{$publicFile->id}}').on('contextmenu', function(e) {
                               
                                $('#{{$publicFile->id}}').css({
                                  display: "block",
                                  position: "absolute",
                                  top: "0",
                                  left: "0",
                                  "z-index": "20",
                                 
                                }).addClass("show");
                                return false; //blocks default Webbrowser right click menu
          
                              })
                              $('.row').on("click", function() {
                                  $('#{{$publicFile->id}}').removeClass("show").hide();
                              });
                              $('.row').on("contextmenu", function() {
                                      $('#{{$publicFile->id}}').removeClass("show").hide();
                                  });
                              $('.container').on("contextmenu", function() {
                                  $('#{{$publicFile->id}}').removeClass("show").hide();
                              });
                              
                              $('#{{$publicFile->id}} a').on("click", function() {
                                $(this).parent().removeClass("show").hide();
                              });
                              
                          </script>







                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='zip' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='ZIP' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='rar' || pathinfo(storage_path(Storage::disk('public')->url("files/$publicFile->filename")), PATHINFO_EXTENSION) =='RAR' )
                            <a href="#"><img class="img img-fluid rounded" src="/images/icons/zip.svg" alt="" srcset=""  ></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($publicFile->filename, '12')}}</span>

                            <div class="dropdown-menu shadow-lg border-0 dropdown-menu bg-white p-0 fileRightClick" id="{{$publicFile->id}}" >
                              <a class="dropdown-item pl-0 pr-0 text-center" style=" background-color:  #00968859" href="{{route('text',$publicFile)}}"> <img class="img img-fluid rounded" style="object-fit: cover; height:70%; width:30%" src="/images/icons/zip.svg" alt="" srcset=""></a>
                              <a class="dropdown-item text-primary" href="{{route('text',$publicFile)}}"> <i class="fa fa-folder-open" aria-hidden="true"></i> Open </a>
                              <a class="dropdown-item text-primary" href="{{route('share',$publicFile)}}"> <i class="fa fa-share" aria-hidden="true"></i> Share</a>
                            <a class="dropdown-item text-primary" href="{{route('deleteFile',$publicFile)}}"> <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                            </div>
                            <script>
          
                              $('.{{$publicFile->id}}').on('contextmenu', function(e) {
                               
                                $('#{{$publicFile->id}}').css({
                                  display: "block",
                                  position: "absolute",
                                  top: "0",
                                  left: "0",
                                  "z-index": "20",
                                 
                                }).addClass("show");
                                return false; //blocks default Webbrowser right click menu
          
                              })
                              $('.row').on("click", function() {
                                  $('#{{$publicFile->id}}').removeClass("show").hide();
                              });
                              $('.row').on("contextmenu", function() {
                                      $('#{{$publicFile->id}}').removeClass("show").hide();
                                  });
                              $('.container').on("contextmenu", function() {
                                  $('#{{$publicFile->id}}').removeClass("show").hide();
                              });
                              
                              $('#{{$publicFile->id}} a').on("click", function() {
                                $(this).parent().removeClass("show").hide();
                              });
                              
                          </script>


                            
                          
                           @endif
                          </div>
                          @endforeach
                          
                      </div>

                      @else
                      <div class="row justify-content-center">
                       
                          <h2 class=" font-weight-bolder text-light" style="color:dimgrey">No Files Uploaded</h2> 
                    
                       
                      </div>
                    @endif
                    </div>

                    <div class="tab-pane fade pt-1" id="shf" role="tabpanel" aria-labelledby="profile-tab">
                      <h6  class="font-weight-bolder text-right" style="font-size:10px">FILES SHARED WITH ME</h6>
                      @if(count($shares) > 0)
                      <div class="row p-1">
                         
                          @foreach ($shares as $share)
                          <div class="col-sm-1 col-md-2 col-lg-1   p-3 pl-0 pr-0   text-center ">
          
                          {{-- {{dd($extension)}}  $extension =='docx'--}}
                          @if(pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='docx' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='DOCX' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='DOC'|| pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='doc' )
                             
                                  <a href="{{route('text',$share->file_id)}}" class="{{$share->file_id}}s"><img class="img img-fluid rounded"  src="/images/icons/docs.svg" alt="" srcset=""></a>
                                   <span class="text-center" style="font-size:10px;">{{str_limit($share->filename, '12')}}</span>
              
                                   <div class="dropdown-menu shadow-lg border-0 dropdown-menu bg-white p-0 fileRightClick" id="{{$share->file_id}}s" >
                                    <a class="dropdown-item pl-0 pr-0 text-center" style=" background-color:  #00968859" href="{{route('text',$share)}}"> <img class="img img-fluid rounded" style="object-fit: cover; height:70%; width:30%" src="/images/icons/docs.svg" alt="" srcset=""></a>
                                    <a class="dropdown-item text-primary" href="{{route('text',$share->file_id)}}"> <i class="fa fa-folder-open" aria-hidden="true"></i> Open </a>
                                    <a class="dropdown-item text-primary" href="{{route('share',$share)}}"> <i class="fa fa-share" aria-hidden="true"></i> Share</a>
                                    <a class="dropdown-item text-primary" href="{{route('deleteFile',$share->file_id)}}"> <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                  </div>

                                  <script>
                
                                    $('.{{$share->file_id}}s').on('contextmenu', function(e) {
                                     
                                      $('#{{$share->file_id}}s').css({
                                        display: "block",
                                        position: "absolute",
                                        top: "0",
                                        left: "0",
                                        "z-index": "20",
                                       
                                      }).addClass("show");
                                      return false; //blocks default Webbrowser right click menu
                
                                    })
                                    $('.row').on("click", function() {
                                        $('#{{$share->file_id}}s').removeClass("show").hide();
                                    });
                                    $('.row').on("contextmenu", function() {
                                            $('#{{$share->file_id}}s').removeClass("show").hide();
                                        });
                                    $('.container').on("contextmenu", function() {
                                        $('#{{$share->file_id}}s').removeClass("show").hide();
                                    });
                                    
                                    $('#{{$share->file_id}}s a').on("click", function() {
                                      $(this).parent().removeClass("show").hide();
                                    });
                                    
                                </script>




                          
          
                         
                          @elseif(pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='PNG' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='png' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='JPG' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='jpg' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='JPEG' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='jpeg' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='gif')
                               
                                      <a href="{{route('image',$share->file_id)}}" class="{{$share->file_id}}s"><img class="img img-fluid rounded" style="object-fit: cover; height:auto; width:auto"  src="/images/icons/image.svg" alt="" srcset=""></a>
                                    <span class="text-center" style="font-size:10px;">{{str_limit($share->filename, '12')}}</span>
          
          
                                    <div class="dropdown-menu shadow-lg border-0 dropdown-menu bg-white p-0 fileRightClick" id="{{$share->file_id}}s" >
                                      <a class="dropdown-item pl-0 pr-0 text-center" style=" background-color:  #00968859" href="{{route('text',$share)}}"> <img class="img img-fluid rounded" style="object-fit: cover; height:70%; width:30%" src="/images/icons/image.svg" alt="" srcset=""></a>
                                      <a class="dropdown-item text-primary" href="{{route('text',$share->file_id)}}"> <i class="fa fa-folder-open" aria-hidden="true"></i> Open </a>
                                      <a class="dropdown-item text-primary" href="{{route('share',$share)}}"> <i class="fa fa-share" aria-hidden="true"></i> Share</a>
                                      <a class="dropdown-item text-primary" href="{{route('deleteFile',$share->file_id)}}"> <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                    </div>
  
                                    <script>
                  
                                      $('.{{$share->file_id}}s').on('contextmenu', function(e) {
                                       
                                        $('#{{$share->file_id}}s').css({
                                          display: "block",
                                          position: "absolute",
                                          top: "0",
                                          left: "0",
                                          "z-index": "20",
                                         
                                        }).addClass("show");
                                        return false; //blocks default Webbrowser right click menu
                  
                                      })
                                      $('.row').on("click", function() {
                                          $('#{{$share->file_id}}s').removeClass("show").hide();
                                      });
                                      $('.row').on("contextmenu", function() {
                                              $('#{{$share->file_id}}s').removeClass("show").hide();
                                          });
                                      $('.container').on("contextmenu", function() {
                                          $('#{{$share->file_id}}s').removeClass("show").hide();
                                      });
                                      
                                      $('#{{$share->file_id}}s a').on("click", function() {
                                        $(this).parent().removeClass("show").hide();
                                      });
                                      
                                  </script>







          
                          @elseif(pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='pdf' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='PDF')
                          
                               
          
                                          <a href="{{route('pdf',$share->file_id)}}" class="{{$share->file_id}}s"><img class="img img-fluid rounded" src="/images/icons/pdf.svg" alt="" srcset="" ></a>
                                              <span class="text-center" style="font-size:10px;">{{str_limit($share->filename, '12')}}</span>
          
          
                                              <div class="dropdown-menu shadow-lg border-0 dropdown-menu bg-white p-0 fileRightClick" id="{{$share->file_id}}s" >
                                                <a class="dropdown-item pl-0 pr-0 text-center" style=" background-color:  #00968859" href="{{route('text',$share)}}"> <img class="img img-fluid rounded" style="object-fit: cover; height:70%; width:30%" src="/images/icons/pdf.svg" alt="" srcset=""></a>
                                                <a class="dropdown-item text-primary" href="{{route('text',$share->file_id)}}"> <i class="fa fa-folder-open" aria-hidden="true"></i> Open </a>
                                                <a class="dropdown-item text-primary" href="{{route('share',$share)}}"> <i class="fa fa-share" aria-hidden="true"></i> Share</a>
                                                <a class="dropdown-item text-primary" href="{{route('deleteFile',$share->file_id)}}"> <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                              </div>
            
                                              <script>
                            
                                                $('.{{$share->file_id}}s').on('contextmenu', function(e) {
                                                 
                                                  $('#{{$share->file_id}}s').css({
                                                    display: "block",
                                                    position: "absolute",
                                                    top: "0",
                                                    left: "0",
                                                    "z-index": "20",
                                                   
                                                  }).addClass("show");
                                                  return false; //blocks default Webbrowser right click menu
                            
                                                })
                                                $('.row').on("click", function() {
                                                    $('#{{$share->file_id}}s').removeClass("show").hide();
                                                });
                                                $('.row').on("contextmenu", function() {
                                                        $('#{{$share->file_id}}s').removeClass("show").hide();
                                                    });
                                                $('.container').on("contextmenu", function() {
                                                    $('#{{$share->file_id}}s').removeClass("show").hide();
                                                });
                                                
                                                $('#{{$share->file_id}}s a').on("click", function() {
                                                  $(this).parent().removeClass("show").hide();
                                                });
                                                
                                            </script>
                              





          
                          @elseif(pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='mp3' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='MP3')
                              
                               
          
                                        <a href="{{route('audio',$share->file_id)}}" class="{{$share->file_id}}s"><img class="img img-fluid rounded" src="/images/icons/mp3.svg" alt="" srcset="" ></a>
                                      <span class="text-center" style="font-size:10px;">{{str_limit($share->filename, '12')}}</span>
          
          
                                      <div class="dropdown-menu shadow-lg border-0 dropdown-menu bg-white p-0 fileRightClick" id="{{$share->file_id}}s" >
                                        <a class="dropdown-item pl-0 pr-0 text-center" style=" background-color:  #00968859" href="{{route('text',$share)}}"> <img class="img img-fluid rounded" style="object-fit: cover; height:70%; width:30%" src="/images/icons/mp3.svg" alt="" srcset=""></a>
                                        <a class="dropdown-item text-primary" href="{{route('text',$share->file_id)}}"> <i class="fa fa-folder-open" aria-hidden="true"></i> Open </a>
                                        <a class="dropdown-item text-primary" href="{{route('share',$share->file_id)}}"> <i class="fa fa-share" aria-hidden="true"></i> Share</a>
                                        <a class="dropdown-item text-primary" href="{{route('deleteFile',$share->file_id)}}"> <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                      </div>
    
                                      <script>
                    
                                        $('.{{$share->file_id}}s').on('contextmenu', function(e) {
                                         
                                          $('#{{$share->file_id}}s').css({
                                            display: "block",
                                            position: "absolute",
                                            top: "0",
                                            left: "0",
                                            "z-index": "20",
                                           
                                          }).addClass("show");
                                          return false; //blocks default Webbrowser right click menu
                    
                                        })
                                        $('.row').on("click", function() {
                                            $('#{{$share->file_id}}s').removeClass("show").hide();
                                        });
                                        $('.row').on("contextmenu", function() {
                                                $('#{{$share->file_id}}s').removeClass("show").hide();
                                            });
                                        $('.container').on("contextmenu", function() {
                                            $('#{{$share->file_id}}s').removeClass("show").hide();
                                        });
                                        
                                        $('#{{$share->file_id}}s a').on("click", function() {
                                          $(this).parent().removeClass("show").hide();
                                        });
                                        
                                    </script>
                                







          
                          @elseif(pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='csv' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='CSV' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='xlsx' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='XLSX')
                         
                              
          
                                      <a href="{{route('sheet',$share->file_id)}}" class="{{$share->file_id}}s"><img class="img img-fluid rounded" src="/images/icons/sheets.svg" alt="" srcset="" ></a>
                                    <span class="text-center" style="font-size:10px;">{{str_limit($share->filename, '12')}}</span>
          
          
                                    <div class="dropdown-menu shadow-lg border-0 dropdown-menu bg-white p-0 fileRightClick" id="{{$share->file_id}}s" >
                                      <a class="dropdown-item pl-0 pr-0 text-center" style=" background-color:  #00968859" href="{{route('text',$share)}}"> <img class="img img-fluid rounded" style="object-fit: cover; height:70%; width:30%" src="/images/icons/sheets.svg" alt="" srcset=""></a>
                                      <a class="dropdown-item text-primary" href="{{route('text',$share->file_id)}}"> <i class="fa fa-folder-open" aria-hidden="true"></i> Open </a>
                                      <a class="dropdown-item text-primary" href="{{route('share',$share->file_id)}}"> <i class="fa fa-share" aria-hidden="true"></i> Share</a>
                                      <a class="dropdown-item text-primary" href="{{route('deleteFile',$share->file_id)}}"> <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                    </div>
  
                                    <script>
                  
                                      $('.{{$share->file_id}}s').on('contextmenu', function(e) {
                                       
                                        $('#{{$share->file_id}}s').css({
                                          display: "block",
                                          position: "absolute",
                                          top: "0",
                                          left: "0",
                                          "z-index": "20",
                                         
                                        }).addClass("show");
                                        return false; //blocks default Webbrowser right click menu
                  
                                      })
                                      $('.row').on("click", function() {
                                          $('#{{$share->file_id}}s').removeClass("show").hide();
                                      });
                                      $('.row').on("contextmenu", function() {
                                              $('#{{$share->file_id}}s').removeClass("show").hide();
                                          });
                                      $('.container').on("contextmenu", function() {
                                          $('#{{$share->file_id}}s').removeClass("show").hide();
                                      });
                                      
                                      $('#{{$share->file_id}}s a').on("click", function() {
                                        $(this).parent().removeClass("show").hide();
                                      });
                                      
                                  </script>
                              







          
                          
                          @elseif(pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='html' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='HTML')
                                    
                                    
                                        <a href="{{route('text',$share->file_id)}}" class="{{$share->file_id}}s"><img class="img img-fluid rounded" src="/images/icons/html.svg" alt="" srcset="" ></a>
                                          <span class="text-center" style="font-size:10px;">{{str_limit($share->filename, '12')}}</span>
                                            
          
                                          <div class="dropdown-menu shadow-lg border-0 dropdown-menu bg-white p-0 fileRightClick" id="{{$share->file_id}}s" >
                                            <a class="dropdown-item pl-0 pr-0 text-center" style=" background-color:  #00968859" href="{{route('text',$share)}}"> <img class="img img-fluid rounded" style="object-fit: cover; height:70%; width:30%" src="/images/icons/html.svg" alt="" srcset=""></a>
                                            <a class="dropdown-item text-primary" href="{{route('text',$share->file_id)}}"> <i class="fa fa-folder-open" aria-hidden="true"></i> Open </a>
                                            <a class="dropdown-item text-primary" href="{{route('share',$share->file_id)}}"> <i class="fa fa-share" aria-hidden="true"></i> Share</a>
                                            <a class="dropdown-item text-primary" href="{{route('deleteFile',$share->file_id)}}"> <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                          </div>
        
                                          <script>
                        
                                            $('.{{$share->file_id}}s').on('contextmenu', function(e) {
                                             
                                              $('#{{$share->file_id}}s').css({
                                                display: "block",
                                                position: "absolute",
                                                top: "0",
                                                left: "0",
                                                "z-index": "20",
                                               
                                              }).addClass("show");
                                              return false; //blocks default Webbrowser right click menu
                        
                                            })
                                            $('.row').on("click", function() {
                                                $('#{{$share->file_id}}s').removeClass("show").hide();
                                            });
                                            $('.row').on("contextmenu", function() {
                                                    $('#{{$share->file_id}}s').removeClass("show").hide();
                                                });
                                            $('.container').on("contextmenu", function() {
                                                $('#{{$share->file_id}}s').removeClass("show").hide();
                                            });
                                            
                                            $('#{{$share->file_id}}s a').on("click", function() {
                                              $(this).parent().removeClass("show").hide();
                                            });
                                            
                                        </script>
                                  









          
                          @elseif(pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='pptx' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='PPTX' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='PPT' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='ppt')
                                
                             
                                      <a href="{{route('slide',$share->file_id)}}" class="{{$share->file_id}}s"><img class="img img-fluid rounded" src="/images/icons/slides.svg" alt="" srcset="" ></a>
                                         <span class="text-center" style="font-size:10px;">{{str_limit($share->filename, '12')}}</span>
                                         
                                         <div class="dropdown-menu shadow-lg border-0 dropdown-menu bg-white p-0 fileRightClick" id="{{$share->file_id}}s" >
                                          <a class="dropdown-item pl-0 pr-0 text-center" style=" background-color:  #00968859" href="{{route('text',$share)}}"> <img class="img img-fluid rounded" style="object-fit: cover; height:70%; width:30%" src="/images/icons/slides.svg" alt="" srcset=""></a>
                                          <a class="dropdown-item text-primary" href="{{route('text',$share->file_id)}}"> <i class="fa fa-folder-open" aria-hidden="true"></i> Open </a>
                                          <a class="dropdown-item text-primary" href="{{route('share',$share->file_id)}}"> <i class="fa fa-share" aria-hidden="true"></i> Share</a>
                                          <a class="dropdown-item text-primary" href="{{route('deleteFile',$share->file_id)}}"> <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                        </div>
      
                                        <script>
                      
                                          $('.{{$share->file_id}}s').on('contextmenu', function(e) {
                                           
                                            $('#{{$share->file_id}}s').css({
                                              display: "block",
                                              position: "absolute",
                                              top: "0",
                                              left: "0",
                                              "z-index": "20",
                                             
                                            }).addClass("show");
                                            return false; //blocks default Webbrowser right click menu
                      
                                          })
                                          $('.row').on("click", function() {
                                              $('#{{$share->file_id}}s').removeClass("show").hide();
                                          });
                                          $('.row').on("contextmenu", function() {
                                                  $('#{{$share->file_id}}s').removeClass("show").hide();
                                              });
                                          $('.container').on("contextmenu", function() {
                                              $('#{{$share->file_id}}s').removeClass("show").hide();
                                          });
                                          
                                          $('#{{$share->file_id}}s a').on("click", function() {
                                            $(this).parent().removeClass("show").hide();
                                          });
                                          
                                      </script>
                              





                              
                          @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$share->filename")), PATHINFO_EXTENSION) =='mp4' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='MP4')
                                 
                                      <a href="{{route('video',$share->file_id)}}" class="{{$share->file_id}}s"><img class="img img-fluid rounded" src="/images/icons/cone.svg" alt="" srcset="" ></a>
                                    <span class="text-center" style="font-size:10px;">{{str_limit($share->filename, '12')}}</span>
          
          
                                    <div class="dropdown-menu shadow-lg border-0 dropdown-menu bg-white p-0 fileRightClick" id="{{$share->file_id}}s" >
                                      <a class="dropdown-item pl-0 pr-0 text-center" style=" background-color:  #00968859" href="{{route('text',$share)}}"> <img class="img img-fluid rounded" style="object-fit: cover; height:70%; width:30%" src="/images/icons/cone.svg" alt="" srcset=""></a>
                                      <a class="dropdown-item text-primary" href="{{route('text',$share->file_id)}}"> <i class="fa fa-folder-open" aria-hidden="true"></i> Open </a>
                                      <a class="dropdown-item text-primary" href="{{route('share',$share->file_id)}}"> <i class="fa fa-share" aria-hidden="true"></i> Share</a>
                                      <a class="dropdown-item text-primary" href="{{route('deleteFile',$share->file_id)}}"> <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                    </div>
  
                                    <script>
                  
                                      $('.{{$share->file_id}}s').on('contextmenu', function(e) {
                                       
                                        $('#{{$share->file_id}}s').css({
                                          display: "block",
                                          position: "absolute",
                                          top: "0",
                                          left: "0",
                                          "z-index": "20",
                                         
                                        }).addClass("show");
                                        return false; //blocks default Webbrowser right click menu
                  
                                      })
                                      $('.row').on("click", function() {
                                          $('#{{$share->file_id}}s').removeClass("show").hide();
                                      });
                                      $('.row').on("contextmenu", function() {
                                              $('#{{$share->file_id}}s').removeClass("show").hide();
                                          });
                                      $('.container').on("contextmenu", function() {
                                          $('#{{$share->file_id}}s').removeClass("show").hide();
                                      });
                                      
                                      $('#{{$share->file_id}}s a').on("click", function() {
                                        $(this).parent().removeClass("show").hide();
                                      });
                                      
                                  </script>
                                






          
                          @elseif(pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='zip' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='ZIP' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='rar' || pathinfo(storage_path(Storage::disk('public')->url("$share->path/$share->filename")), PATHINFO_EXTENSION) =='RAR')
                               
          
                                          <a href="#"><img class="img img-fluid rounded" src="/images/icons/zip.svg" alt="" srcset=""  ></a>
                                        <span class="text-center" style="font-size:10px;">{{str_limit($share->filename, '12')}}</span>
          
          
                                        <div class="dropdown-menu shadow-lg border-0 dropdown-menu bg-white p-0 fileRightClick" id="{{$share->file_id}}s" >
                                          <a class="dropdown-item pl-0 pr-0 text-center" style=" background-color:  #00968859" href="{{route('text',$share)}}"> <img class="img img-fluid rounded" style="object-fit: cover; height:70%; width:30%" src="/images/icons/zip.svg" alt="" srcset=""></a>
                                          <a class="dropdown-item text-primary" href="{{route('text',$share->file_id)}}"> <i class="fa fa-folder-open" aria-hidden="true"></i> Open </a>
                                          <a class="dropdown-item text-primary" href="{{route('share',$share->file_id)}}"> <i class="fa fa-share" aria-hidden="true"></i> Share</a>
                                          <a class="dropdown-item text-primary" href="{{route('deleteFile',$share->file_id)}}"> <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                        </div>
      
                                        <script>
                      
                                          $('.{{$share->file_id}}s').on('contextmenu', function(e) {
                                           
                                            $('#{{$share->file_id}}s').css({
                                              display: "block",
                                              position: "absolute",
                                              top: "0",
                                              left: "0",
                                              "z-index": "20",
                                             
                                            }).addClass("show");
                                            return false; //blocks default Webbrowser right click menu
                      
                                          })
                                          $('.row').on("click", function() {
                                              $('#{{$share->file_id}}s').removeClass("show").hide();
                                          });
                                          $('.row').on("contextmenu", function() {
                                                  $('#{{$share->file_id}}s').removeClass("show").hide();
                                              });
                                          $('.container').on("contextmenu", function() {
                                              $('#{{$share->file_id}}s').removeClass("show").hide();
                                          });
                                          
                                          $('#{{$share->file_id}}s a').on("click", function() {
                                            $(this).parent().removeClass("show").hide();
                                          });
                                          
                                      </script>
                               




                        
                         @endif
                        </div>
                        @endforeach
                              
                          </div>
                          @else
                          <div class="row justify-content-center">
                           
                              <h2 class=" font-weight-bolder text-light" style="color:dimgrey">No Shared Files</h2> 
                        
                           
                          </div>
                        @endif
                    </div>


                    {{-- <div class="tab-pane fade pt-1" id="sff" role="tabpanel" aria-labelledby="profile-tab">
                      <h6  class="font-weight-bolder text-right" style="font-size:10px">MY SPACE / PRIVATE FILES</h6>

                        @if(count($privateFiles) > 0)
                        <div class="row p-1">
                      
                            @foreach ($privateFiles as $privateFile)
                            <div class="col-sm-1 col-md-2 col-lg-1 p-4 pl-0 pr-0   text-center ">

                              @if(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='docx' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='DOCX')
                              <a href="{{route('text',$privateFile)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:auto; width:auto" src="/images/icons/docs.svg" alt="" srcset=""></a>
                                <span class="text-center" style="font-size:10px;">{{str_limit($privateFile->filename, '12')}}</span>
                            
                              @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='PNG')
                              <a href="{{route('image',$privateFile)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:auto; width:auto"  src="{{"storage/files/$privateFile->filename"}}" alt="" srcset=""></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($privateFile->filename, '12')}}</span>
                        
                              @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='png')
                              <a href="{{route('image',$privateFile)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:auto; width:auto"  src="{{"storage/files/$privateFile->filename"}}" alt="" srcset=""></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($privateFile->filename, '12')}}</span>
                        
                              @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='jpg')
                              <a href="{{route('image',$privateFile)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:auto; width:auto"  src="{{"storage/files/$privateFile->filename"}}" alt="" srcset=""></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($privateFile->filename, '12')}}</span>
                        
                              @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='JPG')
                              <a href="{{route('image',$privateFile)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:100%; width:100%"   src="{{"storage/files/$privateFile->filename"}}" alt="" srcset=""></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($privateFile->filename, '12')}}</span>
                        
                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='JPEG')
                            <a href="{{route('image',$privateFile)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:auto; width:auto"  src="{{"storage/files/$privateFile->filename"}}" alt="" srcset=""></a>
                              <span class="text-center" style="font-size:10px;">{{str_limit($privateFile->filename, '12')}}</span>
                        
                            @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='jpeg')
                            <a href="{{route('image',$privateFile)}}"><img class="img img-fluid rounded" style="object-fit: cover; height:auto; width:auto"  src="{{"storage/files/$privateFile->filename"}}" alt="" srcset=""></a>
                              <span class="text-center" style="font-size:10px;">{{str_limit($privateFile->filename, '12')}}</span>
                        
                              @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='pdf' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='PDF')
                              <a href="{{route('pdf',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/pdf.svg" alt="" srcset="" ></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($privateFile->filename, '12')}}</span>
                        
                              @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='mp3' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='MP3')
                              <a href="{{route('audio',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/mp3.svg" alt="" srcset="" ></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($privateFile->filename, '12')}}</span>
                        
                              @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='csv' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='CSV')
                              <a href="{{route('sheet',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/sheets.svg" alt="" srcset="" ></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($privateFile->filename, '12')}}</span>
                        
                              @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='xlsx' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='XLSX')
                              <a href="{{route('sheet',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/sheets.svg" alt="" srcset="" ></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($privateFile->filename, '12')}}</span>
                        
                              @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='html' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='HTML')
                              <a href="{{route('sheet',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/html.svg" alt="" srcset="" ></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($privateFile->filename, '12')}}</span>
                              
                        
                              @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='pptx' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='PPTX' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='PPT' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='pptx')
                              <a href="{{route('slide',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/slides.svg" alt="" srcset="" ></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($privateFile->filename, '12')}}</span>
                        
                      
                              @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='mp4' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='MP4')
                            
                              <a href="{{route('video',$privateFile)}}"><img class="img img-fluid rounded" src="/images/icons/cone.svg" alt="" srcset="" ></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($privateFile->filename, '12')}}</span>
                        
                              @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='zip' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='ZIP')
                              <a href="#"><img class="img img-fluid rounded" src="/images/icons/zip.svg" alt="" srcset=""  ></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($privateFile->filename, '12')}}</span>
                        
                              @elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='rar' || pathinfo(storage_path(Storage::disk('public')->url("files/$privateFile->filename")), PATHINFO_EXTENSION) =='RAR')
                              <a href="#"><img class="img img-fluid rounded" src="/images/icons/zip.svg" alt="" srcset=""  ></a>
                            <span class="text-center" style="font-size:10px;">{{str_limit($privateFile->filename, '12')}}</span>
                              @endif
                            </div>
                            @endforeach
                            
                        </div>
                        @else
                        <div class="row justify-content-center">
                        
                            <h2 class=" font-weight-bolder text-light" style="color:dimgrey">No Private Files</h2> 
                      
                        
                        </div>
                      @endif
                    </div> --}}
                    
                  </div>
















                    
                   

                    <div class="row bg-white">
            

                   
                    <!-- Button trigger modal -->
                    <button type="button" id="myBtn1" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                        </i> <i class="fa fa-file-signature" aria-hidden="true"></i>
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title font-weight-bold text-primary h6" id="exampleModalCenterTitle"> <i class="fa fa-file-signature" aria-hidden="true"></i> Verify Authorization </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body pt-4">
                            <form action="{{route('confirmSS')}}" method="post">
                              @csrf
                                  {{ method_field('POST') }}
                            <div class="input-group pb-4">
                              <input class="form-control  border-1 border-primary border-right-0 shadow-none" required type="text" name="code" placeholder="Enter signature code" aria-label="Recipient's " aria-describedby="my-addon">
                              <div class="input-group-append border-1 border-primary border-left-0">
                                <span class="input-group-text border-1 border-primary border-left-0 bg-white" id="my-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                              </div>
                            </div>

                            <button type="submit" class="btn btn-primary float-right font-weight-bold small"><i class="fa fa-certificate" aria-hidden="true"></i> Verify</button> 
                            <a  class="card-link text-success font-weight-bold  small pt-2" type="button" data-toggle="collapse" data-target="#collapseReply" aria-expanded="false" aria-controls="collapseExample" >
                              <i class="fa fa-times" aria-hidden="true"></i> Cancel
                          </a>
                          </form>
                          </div>
                          <div class="modal-footer">
                            
                          </div>
                        </div>
                      </div>
                    </div>

                    
          
                    
                </div>








                       
                </div>
            </div>
        </div>
    </div>
</div>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>


<script type="text/javascript">
  $(document).ready(function(){

    fetch_customer_data();

        function fetch_customer_data(query = '')
        {
        $.ajax({
          url:"{{ route('live_search.action') }}",
          method:'GET',
          data:{query:query},
          dataType:'json',
          success:function(data)
          {
          $('tbody').html(data.table_data);
          $('#total_records').text(data.total_data);
          }
        })
        }

    $(document).on('keyup', '#search', function(){
    var query = $(this).val();
    fetch_customer_data(query);
    });
    
    });

</script>

<script>
  $(document).ready(function(){
    $('#search').keyup(function(){
      $('#searchTable').css("display", "inline-block");
    });

    $('#search').focusout(function(){
      $('#searchTable').css("display", "none");
    });
   
  });
  </script>

  
<style>
  .nav-tabs{
      /* background-color:  #00968859 !important; */
  }
  .nav-tabs .nav-item .nav-link{
      font-weight: bold;
      color:  #2d2d2d !important;
      border-radius: 0px !important;
      /* border-bottom: 3px solid transparent !important; */
  }
  .nav-tabs .nav-item .active{
      background-color:  #00968859 !important;
      color: #000 !important;
      
  }
  .nnav-tabs .nav-link.active,
  .nav-tabs .show > .nav-link {
      color: #fff !important;
      background-color: #009688 !important;
      /* border-bottom: 3px solid #00cab6 !important; */
  }
  #add .dropdown-item:hover{
    color: #009688 !important;
  }
  .fileRightClick{
    /* position: absolute;
    z-index: 20000; */
  }
  .custom-control-input:checked ~ .custom-control-label::before {
        color: #fff;
        background-color: #009688 !important;
    }
     /* Extra small devices (phones, 600px and down) */
     @media only screen and (max-width: 600px) {
        .header, .list-group-item, .message, .nav-link, .dropdown-item, .hb, .btn, .form-control {font-size: 10px;}
        #search{max-width: 100px;}
        #bg{display: none}
    }
</style>

@endsection
