@extends('layouts.head')

@section('content')
  <div class="container-fluid">
    <div class="row justify-content-center" id="row">

      <div class="col-sm-5" id="board" style="height:100vh">
          <div class="row justify-content-center p-0" id="img">
            <div class="col-sm-5 text-center">
            <img class="img img-fluid " src="/images/logo1.png" alt="" srcset="">
          </div>
          <h2 class="col-sm-10 text-center text-white font-weight-bolder mb-2">Team form</h2>
          {{-- <p class="pl-5 pr-5 pt-1 text-center font-weight-light text-white">The Team form is a knowledge management platform containing historical and practical resources including templates and archives for memos, reports, letters, job aids, policy papers, and other important documents used by the division.</p> --}}

          </div>
      </div>

      <div class="col-sm-7 pr-0 pl-0">

        {{-- @extends('layouts.navbar') --}}
        <nav class="navbar navbar-expand-lg navbar-light bg-white text-dark mb-3">
          <div class="container mr-3 ml-3">
            <a class="navbar-brand" href="#"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div id="navbarNavDropdown" class="navbar-collapse collapse">
                <ul class="navbar-nav mr-auto">
                   
                </ul>
                <ul class="navbar-nav mt-3">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/register') }}">Register</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/login') }}"></a>
                    </li>
                   
                  
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/login') }}">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/login') }}"></a>
                    </li>
                    
                   
                    
                    <li class="nav-item" style="border-bottom:4px solid green;">
                        <a class="nav-link" href="{{ url('/about') }}">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/login') }}"></a>
                    </li>
                   
                    <li class="nav-item" >
                        <a class="nav-link" href="{{ url('/contact') }}">Contact</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/register') }}"></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/register') }}"></a>
                    </li>
                </ul>
            </div>
          </div>
        </nav>
       <br> <br>
          <div class="row justify-content-center m-0">
             <div class="col-sm-8">
               
                <div class="col-sm-12 mb-5 mt-5">
                    <div class="row justify-content-center">
                     
                        <h5 class=" font-weight-bold mb-4 mt-5 p-2" style="border-bottom:4px solid green">About Team form</h5> <br>
                            <div class=" text-justify font-weight-bold">
                                Welcome to the Team form of the Department of Health Planning, Research & Statistics - Federal Ministry of health (DHPRS-FMoH).

                                The Team form is a knowledge management platform containing historical and practical resources including templates and archives for memos, reports, letters, job aids, policy papers, and other important documents used by the division. 
                                
                                As this resource evolves, DHPRS hopes to build the capacity of staff in various areas by providing life-time access to relevant materials and resources.
                                
                                This platform aims to enhance file storage, retrieval and resource sharing amongst members of the DHPRS.
                                
                            </div>
                           
                    </div>
                </div>

             
               
                </div>
               
            </div>
        </div>
    </div>
</div>
@endsection
             