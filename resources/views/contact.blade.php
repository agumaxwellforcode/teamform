@extends('layouts.head')

@section('content')
  <div class="container-fluid">
    <div class="row justify-content-center" id="row">

      <div class="col-sm-12" id="boardc" style="height:100vh">
        <div class="row justify-content-center align-content-center m-0">
          <div class="col-sm-11 col-md-6 col-lg-4 col-xl-3 p-3 shadow pt-5" style="margin-top:15vh; background-color:rgba(255, 255, 255, 0.61); border-radius:10px;">    
              <p class=" font-weight-bolder text-center text-success"> <i class="fa fa-phone" aria-hidden="true"></i> Contact Us</p>
                                <form action="/contact" method="post" enctype="multipart/form-data">
                                    @csrf

                                    <div class="form-group mb-3{{ $errors->has('name') ? ' has-error' : '' }}">
                                        {{-- <label for="name" class="col-md-4 control-label">Name</label> --}}
                    
                                        <div class="col-md-12">
                                          <input id="name" type="text" class="form-control" name="name"  required
                                                autofocus placeholder="Please enter your full name">
                    
                                          @if ($errors->has('name'))
                                            <span class="help-block text-dark">
                                                                <strong>{{ $errors->first('name') }}</strong>
                                                            </span>
                                          @endif
                                        </div>
                                      </div>

                                      <div class="form-group mb-3{{ $errors->has('email') ? ' has-error' : '' }}">
                                        {{-- <label for="name" class="col-md-4 control-label">Name</label> --}}
                    
                                        <div class="col-md-12">
                                          <input id="email" type="email" class="form-control" name="email"  required
                                                autofocus placeholder="Please enter your email address">
                    
                                          @if ($errors->has('email'))
                                            <span class="help-block text-dark">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                            </span>
                                          @endif
                                        </div>
                                      </div>

                                      <div class="form-group mb-3{{ $errors->has('designation') ? ' has-error' : '' }}">
                                        {{-- <label for="name" class="col-md-4 control-label">Name</label> --}}
                    
                                        <div class="col-md-12">
                                          <input id="designation" type="text" class="form-control" name="designation"  required
                                                autofocus placeholder="Please enter your designation">
                    
                                          @if ($errors->has('designation'))
                                            <span class="help-block text-dark">
                                                                <strong>{{ $errors->first('designation') }}</strong>
                                                            </span>
                                          @endif
                                        </div>
                                      </div>

                                      <div class="form-group mb-3{{ $errors->has('subject') ? ' has-error' : '' }}">
                                        {{-- <label for="name" class="col-md-4 control-label">Name</label> --}}
                    
                                        <div class="col-md-12">
                                            <select class="custom-select" name="subject">
                                                
                                                <option value="Enquiry">Enquiry</option>
                                                <option value="Complaint">Complaint</option>
                                                <option value="Contribution">Contribution</option>
                                              </select>
                                          @if ($errors->has('subject'))
                                            <span class="help-block text-dark">
                                                                <strong>{{ $errors->first('subject') }}</strong>
                                                            </span>
                                          @endif
                                        </div>
                                      </div>

                                      <div class="form-group mb-3{{ $errors->has('comment') ? ' has-error' : '' }}">
                                        {{-- <label for="name" class="col-md-4 control-label">Name</label> --}}
                    
                                        <div class="col-md-12">
                                            <label for="exampleFormControlTextarea1">Comment</label>
                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="comment" required></textarea>
                    
                                          @if ($errors->has('comment'))
                                            <span class="help-block text-dark">
                                                                <strong>{{ $errors->first('comment') }}</strong>
                                                            </span>
                                          @endif
                                        </div>
                                      </div>

                                      <div class="form-group ">
                                        <div class="col-md-12 offset-md-0">
                                            <button type="submit" class="btn btn-primary btn-block">
                                                {{ __('Send') }}
                                            </button>
            
                                        </div>
                                        
                                    </div>


                                </form>
                              </div>
                            </div>
                            <a class="text-center" href="{{route('login')}}" id="myBtn" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                          </div>
                          </div>
                        </div>
                    
                          
                      @endsection
                            
                           
                  