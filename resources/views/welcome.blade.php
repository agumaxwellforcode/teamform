<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Team form</title>

        <!-- Fonts -->
        
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <script src="https://kit.fontawesome.com/bf6c353baa.js" crossorigin="anonymous"></script>
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding:10px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                /* text-transform: uppercase; */
                border: 1px solid #fff;
                border-radius: 4px;
            }
            .links > a:hover {
                color: green !important;
                background-color: #fff;
                font-weight: bolder !important;
            }

            .wl {
                color: #636b6f;
                padding:10px;
                margin: 3px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                /* text-transform: uppercase; */
                border: 1px solid #fff;
                border-radius: 4px;
            }
            .wl:hover {
                color: green !important;
                background-color: #fff;
                font-weight: bolder !important;
            }
            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body style=" background-image:url('/images/board.png'); ">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        {{-- <a class="btn btn-primary" href="{{ url('/home') }}"style="color:white;" > <i class="fa fa-home" aria-hidden="true"></i> HOME</a> --}}
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                 
                </div>

                <div class="row justify-content-center" id="img">
                    <div class="col-sm-5 text-center">
                    <img class="img img-fluid " src="/images/logo1.png" alt="" srcset="">
                  </div>
                  @php
                      $user=Auth::user();

                  @endphp
                  <h1 style="color:white; margin-top:2px;" class="col-sm-10 text-center text-white font-weight-bolder mb-4 ">{{$user->firstname}} {{$user->lastname}}</h1> 
                  <hr> <h3 style="color:white;"> Welcome to FMOH Team form.</h3> <br>
                    <div class="row justify-content-center ">
                        <a class="btn btn-primary wl" href="{{ url('/home') }}"style="color:white;" > <i class="fa fa-home" aria-hidden="true"></i> Home</a>
                        <a class="btn btn-primary wl" href="{{ url('/profile') }}"style="color:white;" > <i class="fa fa-user" aria-hidden="true"></i> Profile</a>

                        
                    </div>
                  </div>
            </div>
        </div>
    </body>
</html>
