@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-8 col-lg-9" style="background:url('/images/music.svg');background-repeat:no-repeat; background-position:center">
         
                <div class="card-header font-weight-bolder">AUDIO PLAYER
                        <div class="float-right">
                            <div class="btn-group">
                                <button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <i class="fa fa-fingerprint" aria-hidden="true"></i>  
                                 
                                </button>
                                <div class="dropdown-menu shadow-lg border-0">
                                        @can('manage-users')
                                          <a  href="{{route('comment.printComment',$file)}}" class="dropdown-item font-weight-bold "> <i class="fa fa-comment-alt" aria-hidden="true"></i> Comments </a>
                                        
                                        @endcan
                                </div>
                              </div>
                                <a  href="{{route('home')}}" class="btn btn-default btn-sm"> </i> <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</a>
                            </div>  

                </div>
                  <div class="row justify-content-center"> 
                        <div class="col-sm-10" style="height:82vh">
                            <div class="card mb-3 shadow p-5 align-self-center" style=" background-color:rgba(255, 255, 255, 0.61); border-radius:10px; top:30%;">
                                <div class="card-body">
                                <h6 class="card-title text-primary font-weight-bold"> <marquee behavior="scroll" direction="right"> {{ $file->filename }}</marquee>
                                   </h6>
                                        <audio controls class="col-sm-12">
                                            <source ssrc="{{url($response)}}" type="audio/ogg">
                                            <source src="{{url($response)}}" type="audio/mpeg">
                                        Your browser does not support the audio element.
                                    </audio>
                                </div>
                            </div>
                    </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-3 p-3 bg-white">
                    <div class="collapse" id="collapseExample">
                        <div class="card mb-3 shadow" style="width: 18rem; border-radius:8px;">
                            <div class="card-body">
                                <img class="rounded-circle float-right" height="30px" width="30px" src="/storage/avatars/{{ Auth::user()->avatar }}" />
                              <h5 class="card-title font-weight-bold">{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</h5>
                              <h6 class="card-subtitle  text-muted small">{{ Auth::user()->updated_at }}</h6>

                            <form action="{{route('comment.create')}}" method="post">
                                @csrf
                                    {{ method_field('POST') }}
                                  <div class="form-group">
                                    <label for=""></label>
                                    <textarea id="txtarea" class="form-control border-1 border-primary" name="comment" id="" rows="1" required></textarea>
                                  </div>
                              <input type="hidden" name="file_id" value="{{$file->id}}">
                              <input type="hidden" name="filename" value="{{$file->filename}}">
                              <input type="hidden" name="folder_id" value="{{$file->folder_id}}">
                                  @if ($file->folder_id == "0")
                                    <input type="hidden" name="path" value="files/{{$file->filename}}">
                                  @else
                                    @php
                                        $path=DB::table('folders') ->where('id','=',$file->folder_id) ->first();
                                    @endphp
                                        <input type="hidden" name="path" value="{{$path->path}}/{{$file->filename}}">
                                  @endif
                                     
                              <input type="hidden" name="owner_id" value="{{$file->user_id}}">
                              <input type="hidden" name="commenter_id" value="{{ Auth::user()->id }}">
                              <input type="hidden" name="url" value="{{ url()->current() }}">

                              <button type="submit" class="btn btn-primary font-weight-bold">Comment</button> 
                              <a  class="card-link text-success font-weight-bold float-right pt-2" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" >
                                </i> <i class="fa fa-times" aria-hidden="true"></i> Cancel
                            </a>
                            </form>

                            </div>
                          </div>
                      </div>
                    @php
                        $comments=DB::table('comments') ->where('file_id','=',$file->id)->where('status','=','Open')->orderBy('id','desc')->get();
                    @endphp
                    @if (count($comments) > 0 )
                        @foreach ($comments as $comment)
                            <div class="card mb-3 shadow" style="width: 18rem; border-radius:8px;">
                                <div class="card-body">
                                    @php
                                        $commenter=DB::table('users') ->where('id','=',$comment->commenter_id) ->first();
                                    @endphp
                                    <img class="rounded-circle float-right border-2 border-primary" height="30px" width="30px" src="/storage/avatars/{{ $commenter->avatar }}" />
                                <h6 class="card-title font-weight-bold">{{ $commenter->firstname }} {{ $commenter->lastname }}</h6>
                                <h6 class="card-subtitle mb-2 text-muted small">{{ $comment->created_at }}</h6>
                                <p class="card-text font-weight-bold text-dark">{{$comment->comment}}.</p>







































                                @php
                                    $replies=DB::table('replies') ->where('comment_id','=',$comment->id)->orderBy('id','asc')->get();
                                @endphp
                                @if (count($replies) > 0 )
                                    @foreach ($replies as $reply)
                                        <div class="card mb-3 shadow" style="width: 18rem; border-radius:8px;">
                                            <div class="card-body">
                                                @php
                                                    $replyer = DB::table('users') ->where('id','=',$reply->replyer_id)->first();
                                                    //  dd($replyer->avatar)
                                                @endphp
                                                <img class="rounded-circle float-right border-2 border-primary" height="30px" width="30px" src="/storage/avatars/{{ $replyer->avatar }}" />
                                            <h6 class="card-title font-weight-bold">{{ $replyer->firstname }} {{ $replyer->lastname }}</h6>
                                            <p class="card-text small font-weight-bold text-dark">{{$reply->reply}}.</p>
            
            
            
                                            {{-- <a href="{{route('comment.closeComment',$comment->id)}}" class="card-link text-success small font-weight-bold"><i class="fa fa-times" aria-hidden="true"></i> Close</a> --}}
                                           
                                            </div>
                                        </div>
                                    @endforeach
                                @endif






















                                
                                <div class="collapse mb-2" id="collapseReply{{$comment->id}}">
                                    <hr>
                                    <div class="card card-body border-0 p-0">
                                        <form action="{{route('reply.create')}}" method="post">
                                            @csrf
                                                {{ method_field('POST') }}
                                          <div class="form-group">
                                            <textarea id="txtarea" class="form-control border-1 border-primary" name="reply" id="" rows="1" required></textarea>
                                          </div>
                                          <input type="hidden" name="file_id" value="{{$file->id}}">
                                          <input type="hidden" name="comment_id" value="{{$comment->id}}">
                                          <input type="hidden" name="replyer_id" value="{{ Auth::user()->id }}">

                                          <input type="hidden" name="url" value="{{ url()->current() }}">
                                          <input type="hidden" name="filename" value="{{$file->filename}}">
                                          <input type="hidden" name="owner" value="{{$file->user_id}}">
            
                                          <button type="submit" class="btn btn-primary btn-sm font-weight-bold">Reply This comment</button> 
                                          <a  class="card-link text-success font-weight-bold small float-right pt-2" type="button" data-toggle="collapse" data-target="#collapseReply{{$comment->id}}" aria-expanded="false" aria-controls="collapseExample" >
                                             <i class="fa fa-times" aria-hidden="true"></i> Cancel
                                          </a>
                                        </form>
            
                                    </div>
                                  </div>





                                {{-- <a href="{{route('comment.closeComment',$comment->id)}}" class="card-link text-success small font-weight-bold"><i class="fa fa-times" aria-hidden="true"></i> Close</a> --}}
                                <a href="#collapseReply{{$comment->id}}" class="card-link text-success small font-weight-bold" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample">Reply comment</a>
                                </div>
                            </div>
                        @endforeach
                    @endif
                    
                        <button id="myBtn1" class="btn btn-primary shadow  " type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" data-toggle="tooltip" data-placement="bottom" title="Add Comment">
                            </i> <i class="fa fa-comment-alt" aria-hidden="true"></i>
                        </button>
                      
                     
                </div>
          
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<script>
    function goBack() {
      window.history.back();
    }
    </script>
    <script>
        function expandTextarea(id) {
            document.getElementById(id).addEventListener('keyup', function() {
                this.style.overflow = 'hidden';
                this.style.height = 0;
                this.style.height = this.scrollHeight + 'px';
            }, false);
        }

        expandTextarea('txtarea');
    </script>

@endsection
