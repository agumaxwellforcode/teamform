<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserRegisteredSuccessfully extends Notification
{
    use Queueable;

    /** 
     *  @var User
     **/

     Protected $user;

    /**
     * Create a new notification instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this ->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        /** @var User $user */
        $user = $this->user;
        $firstname = $this->user->firstname;
        $lastname = $this->user->lastname;
        return (new MailMessage)
        
                    ->from(env('ADMIN_MAIL','nasarawahmb@gmail.com'))
                       
                    // ->from(config('mail.from'))

                    ->subject('Successfully created your new account')

                    ->greeting(sprintf('Hello %s',$firstname ))

                    ->line('Your registration to Nasarawa Hospital Management Board system was successfull, Please click the button to activate your account and refresh the page after that.')

                    ->action('Activate Account', route('activate.user', $user->activation_code));

                    // ->line('Warm Regards!');
    }


    

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
