<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SignStamp extends Model
{
    protected $fillable = [
        'file','filename', 'file_id', 'owner', 'action', 'actioneer', 'signature','stamp', 'code'
    ];
}
