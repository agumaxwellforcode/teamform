<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subfolder extends Model
{
    protected $fillable = [
        'name',
        'user_id',
        'preceeding_folder_id',
        'path',
        'permision',
    ];
}
