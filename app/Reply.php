<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $fillable =[
        'file_id','comment_id','replyer_id','reply','status'
    ];
}
