<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
   protected $fillable =[
       
       'sharer_id',
       'reciepient_id',
       'file_id',
       'filename',
       'folder_id',
       'path',
       'acknowledged',
    
   ];
}
