<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable =[
        'file_id','filename','folder_id','path','owner_id','commenter_id','comment','status'
    ];
}
