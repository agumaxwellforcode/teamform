<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileNotification extends Model
{
    protected $fillable = [
        'url',
        'owner',
        'actioneer',
        'action',
        'filename',
        'seen',
    ];
}
