<?php

namespace App\Http\Controllers;

use App\Reply;
use App\File;
use Illuminate\Http\Request;
use App\FileNotification;

class ReplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'file_id' => 'required',
            'comment_id' => 'required',
            'replyer_id' => 'required',
            'reply' => 'required', 

            'filename' => 'required', 
            'owner' => 'required', 
          ]);

          Reply::create([
            'file_id' => $request->get('file_id'),
            'comment_id' => $request->get('comment_id'),
            'replyer_id' => $request->get('replyer_id'),
            'reply' =>  $request->get('reply'),
            'status' =>  "Open",
          ]);

          FileNotification::create([
            'url' => $request->get('url'),
            'filename' => $request->get('filename'),
            'owner' =>  $request->get('owner'),
            'actioneer' =>  $request->get('replyer_id'),
            'action' =>   "replied a comment on a document you uploaded",
            'seen' =>  "0",
          ]);


           return back()->with('toast_success', 'Replied');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function show(Reply $reply)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function edit(Reply $reply)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reply $reply)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reply $reply)
    {
        //
    }
}
