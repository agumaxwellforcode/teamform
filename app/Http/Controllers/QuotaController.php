<?php

namespace App\Http\Controllers;

use App\quota;
use Auth;
use Illuminate\Http\Request;
use App\Charts\totalUsage;
use App\User;
use App\File;
use App\Share;
use App\Folder;
use App\Folderfiles;
use Illuminate\Support\Facades\Storage;


class QuotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::count();

        $mfiles = File::count();
        $sfiles = Folderfiles::count();
        $files = $mfiles + $sfiles; 

        $folders = Folder::count();

        $chart = new totalUsage;
        $chart->options([
            'tooltip' => [
                'show' => true, // or false, depending on what you want.
            ],
        ]);
       
        $chart->labels(['Users', 'Files', 'Folders']);

        $chart->dataset('My dataset', 'doughnut', [$users, $files, $folders])->options([
            
            'backgroundColor' => [
                '#cc65fe',
                '#6cb2eb ',
                '#ffce56',
            ]
        ]);


///////////////////////////////////////////////////////////////////////////////////// quota for all users





    // $value 
$files = File::get();

$sum = 0;
$sumf = 0;

    foreach ($files as $file) {
        
        $filesize = Storage::disk('public')->size("files/$file->filename");
        $sumf += $filesize;
       
    }
    $sumff = 0;
    $folderfiles = Folderfiles::get();
    
    foreach ($folderfiles as $folderfile) {
        $ffpath = Folder::where('id', '=', $folderfile->folder_id)->first();

        if ( $ffpath !== NULL) {
           
            $filesize2 = Storage::disk('public')->size("$ffpath->path/$folderfile->filename");
           
            $sumff += $filesize2;
           
        }
       
      
    }
   
    $sumr = $sumf + $sumff ;
    // dd($sumr);
    $sum = round($sumr,1);
    $normalSum = $sumf + $sumff;
    $remaining = 21474836480 - $sum;
    
    $normalRemaining = 21474836480 - $sum;

    $kb = 1024;
    $mb = $kb * 1024;
    $gb = $mb * 1024;
    $tb = $gb * 1024;

    if (($sum >= 0) && ($sum < $kb)) {
        $sum = $sum . ' B';
    } elseif (($sum >= $kb) && ($sum < $mb)) {
        $sum = ($sum / $kb) . ' KB';
    } elseif (($sum >= $mb) && ($sum < $gb)) {
        $sum = ($sum / $mb) . ' MB';
    } elseif (($sum >= $gb) && ($sum < $tb)) {
        $sum = ($sum / $gb) . ' GB';
    } elseif ($sum >= $tb) {
        $sum = ($sum / $tb) . ' TB';
    } else {
        $sum = $sum . ' B';
    }
    if (($remaining >= 0) && ($remaining < $kb)) {
        $remaining = $remaining . ' B';
    } elseif (($remaining >= $kb) && ($remaining < $mb)) {
        $remaining = ($remaining / $kb) . ' KB';
    } elseif (($remaining >= $mb) && ($remaining < $gb)) {
        $remaining = ($remaining / $mb) . ' MB';
    } elseif (($remaining >= $gb) && ($remaining < $tb)) {
        $remaining = ($remaining / $gb) . ' GB';
    } elseif ($remaining >= $tb) {
        $remaining = ($remaining / $tb) . ' TB';
    } else {
        $remaining = $remaining . ' B';
    }




    // dd($normalSum);

        $bar = new totalUsage;
       
        $bar->labels([ 'Space Used','Space Available']);

        $bar->dataset('Usage', 'doughnut', [$normalSum, $normalRemaining])->options([
            
            'backgroundColor' => [
                '#6cb2eb ',
                '#ffce56',
            ]
        ]);

       ///////////////////////////////////////////////////////////////////////////////////// quotaeach user





    // $User
    $user = Auth::user();
    if ($user !== Null){
        $userfiles = File::where('user_id', '=', $user->id)->get();
        $userfolderfiles = Folderfiles::where('user_id', '=', $user->id)->get();
        $usersum = 0;
        $usersumf = 0;
        $usersumff = 0;
            foreach ($userfiles as $userfile) {
                
                $userfilesize = Storage::disk('public')->size("files/$userfile->filename");
                $usersumf += $userfilesize;
            
            }
            foreach ($userfolderfiles as $userfolderfile) {

                $userpath = Folder::where('id', '=', $userfolderfile->folder_id)->first();

                if ( $userpath !== NULL) {
            
                $userfilesize2 = Storage::disk('public')->size("$userpath->path/$userfolderfile->filename");
               
                $usersumff += $userfilesize2;
               
            }


          
            // dd($path);
          
          
          
        }
        $usersum = $usersumf + $usersumff ;
        $usernormalSum = $usersumf + $usersumff;
        $userremaining = (21474836480/$users) - $usersum;
        $usernormalRemaining = (21474836480/$users) - $usersum;
    
        $kb = 1024;
        $mb = $kb * 1024;
        $gb = $mb * 1024;
        $tb = $gb * 1024;
    
        if (($usersum >= 0) && ($usersum < $kb)) {
            $usersum = $usersum . ' B';
        } elseif (($usersum >= $kb) && ($usersum < $mb)) {
            $usersum = ($usersum / $kb) . ' KB';
        } elseif (($usersum >= $mb) && ($usersum < $gb)) {
            $usersum = ($usersum / $mb) . ' MB';
        } elseif (($usersum >= $gb) && ($usersum < $tb)) {
            $usersum = ($usersum / $gb) . ' GB';
        } elseif ($usersum >= $tb) {
            $usersum = ($usersum / $tb) . ' TB';
        } else {
            $usersum = $usersum . ' B';
        }
        if (($userremaining >= 0) && ($userremaining < $kb)) {
            $userremaining = $userremaining . ' B';
        } elseif (($userremaining >= $kb) && ($userremaining < $mb)) {
            $userremaining = ($userremaining / $kb) . ' KB';
        } elseif (($userremaining >= $mb) && ($userremaining < $gb)) {
            $userremaining = ($userremaining / $mb) . ' MB';
        } elseif (($userremaining >= $gb) && ($userremaining < $tb)) {
            $userremaining = ($userremaining / $gb) . ' GB';
        } elseif ($userremaining >= $tb) {
            $userremaining = ($userremaining / $tb) . ' TB';
        } else {
            $userremaining = $userremaining . ' B';
        }
    
    
    
    
        // dd($normalSum);
    
            $userChart = new totalUsage;
           
            $userChart->labels([ 'Space Used','Space Available']);
    
            $userChart->dataset('My dataset', 'doughnut', [$usernormalSum, $usernormalRemaining])->options([
                
                'backgroundColor' => [
                    '#6cb2eb ',
                    '#ffce56',
                ]
            ]);
    
            return view('quota', compact(['chart','bar','userChart']));
        }
        else{
            return redirect()->route('logout');
        }
        }




































    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\quota  $quota
     * @return \Illuminate\Http\Response
     */
    public function show(quota $quota)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\quota  $quota
     * @return \Illuminate\Http\Response
     */
    public function edit(quota $quota)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\quota  $quota
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, quota $quota)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\quota  $quota
     * @return \Illuminate\Http\Response
     */
    public function destroy(quota $quota)
    {
        //
    }


    function formatSize($sum){ 
        $kb = 1024;
        $mb = $kb * 1024;
        $gb = $mb * 1024;
        $tb = $gb * 1024;
        if (($sum >= 0) && ($sum < $kb)) {
        return $sum . ' B';
        } elseif (($sum >= $kb) && ($sum < $mb)) {
        return ceil($sum / $kb) . ' KB';
        } elseif (($sum >= $mb) && ($sum < $gb)) {
        return ceil($sum / $mb) . ' MB';
        } elseif (($sum >= $gb) && ($sum < $tb)) {
        return ceil($sum / $gb) . ' GB';
        } elseif ($sum >= $tb) {
        return ceil($sum / $tb) . ' TB';
        } else {
        return $sum . ' B';
        }
        }

        function folderSize($dir){
            $total_size = 0;
            $count = 0;
            $dir_array = scandir($dir);
              foreach($dir_array as $key=>$filename){
                if($filename!=".." && $filename!="."){
                   if(is_dir($dir."/".$filename)){
                      $new_foldersize = foldersize($dir."/".$filename);
                      $total_size = $total_size+ $new_foldersize;
                    }else if(is_file($dir."/".$filename)){
                      $total_size = $total_size + filesize($dir."/".$filename);
                      $count++;
                    }
               }
             }
            return $total_size;
            }
}
