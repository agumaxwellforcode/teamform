<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Response;
// import the storage facade
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;
use File;
use App\Folder;
use App\Folderfiles;
use DB;
use Illuminate\Http\Request;
use User;
use Auth;
Use Alert;

class FolderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user= Auth::user();
      $publicFolders= Folder::where('preceeding_folder_id','=','0')->where('permision','=','all')->get();
      $privateFolders= Folder::where('preceeding_folder_id','=','0')->where('user_id','=',$user->id)->where('permision','=','me')->get();

      return view('folderindex')->with ([
          'user'=>  $user,
          'publicFolders'=> $publicFolders,
          'privateFolders'=> $privateFolders,
      ]);
    }


/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createfolder()
    {
        $user= Auth::user();

      return view('foldercreate')->with ([
        'user'=>  $user,
      ]);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request){
        // dd($request);
       


        }
        // if (!Storage::exists($folder)) {
        //     Storage::makeDirectory($folder, 0775, true, true);
        // }
        // if (Storage::exists($folder)) {
        //     return back()->with('message', 'A folder with this name already exist');
        // }

        // return back()->with('message', 'Folder created successfully');

        //  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'permision' => 'required',
            'user_id' => 'required',
          ]);
        
        $foldername = $request->name;
  
        $path = ("files/$foldername");
      
        // if(!Storage::disk('public')->getVisibility($path)){

            Storage::disk('public')->makeDirectory($path, 0777, true, true);

          
           
              Folder::create([
                'name' => $request->get('name'),
                'permision' => $request->get('permision'),
                'user_id' => $request->get('user_id'),
                'path' => $path,
                'preceeding_folder_id' => '0',

              ]);

            return redirect()->route('home')->with('toast_success', 'Folder created successfully');

     }
       
       
 

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }




    public function deleteFolder(Folder $folder)
    {
        // dd($folder);






        $item = Folder::where('id','=',$folder->id)->first();

        $subfolders = Folder::where('preceeding_folder_id','=',$folder->id)->get();

        $contents = Folderfiles::where('folder_id','=',$folder->id)->get();


        foreach ($subfolders as $subfolder){
        
             $subfolder->delete();

           }
        
        foreach ($contents as $content){
              // dd($share->id);
              $content->delete();

              Storage::disk('public')->delete("$folder->path/$content->filename");

            }


        
        $item->delete();

        Storage::disk('public')->deleteDirectory("$folder->path");

        return  back()->with('toast_success', 'Folder and its contents Deleted Successfully');



    }

    











}
