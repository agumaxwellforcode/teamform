<?php

namespace App\Http\Controllers;

use App\Share;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;
use App\File;
use App\Folderfiles;
use App\Folder;
use App\Subfolder;
use DB;
use App\User;
use Auth;
use Illuminate\Support\Facades\Response as FacadeResponse;
Use Alert;


class ShareController extends Controller
{



    public function share(File $file)
    {
        // dd($file);
        $user=Auth::user();
        $users=User::all();
        
        if($file->folder_id == '0'){
            $path ='files/';
        }
        else{
            $folderpath= Folder::where('id','=',$file->id);
            $path=$folderpath->path;
           
        }
        
        $file_id=$file->id ;
        $folder_id=$file->folder_id ;
        $filename=$file->filename ;

        return view('share')->with(
            [
                'users'=>  $users,
                'path'=>  $path,
                'file_id'=>  $file_id,
                'folder_id'=>  $folder_id,
                'filename'=>  $filename,
            ]
        );

    }



    public function subshare(Folderfiles $file)
    {


     

    //    dd($file);
        $user=Auth::user();
        $users=User::all();
        
        if($file->folder_id == '0'){
            $path ='files/';
        }
        else{
            //  dd( $file->folder_id);
             $folderpath = DB::table('folders')->where('id','=',$file->folder_id)->first();

            // $file = Folderfiles::where('id','=',$file->folder_id)->get();

            //   dd( $folderpath);

            $path=$folderpath->path;
            // dd( $path);
        }
        
        $file_id=$file->id ;
        $folder_id=$file->folder_id ;
        $filename=$file->filename ;
        $path=$path;
        return view('share')->with(
            [
                'users'=>  $users,
                'path'=>  $path,
                'file_id'=>  $file_id,
                'folder_id'=>  $folder_id,
                'filename'=>  $filename,
            ]
        );

    }




































    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

//   dd($request->sharer_id);



        for ($i = 0; $i < count($request->reciepient_id); $i++) {

            $reciepients[] = [
                'sharer_id' => $request->sharer_id,
                'reciepient_id' => $request->reciepient_id[$i],
                'file_id' => $request->file_id,
                'filename' => $request->filename,
                'folder_id' => $request->folder_id,
                'path' => $request->path,
                'acknowledged' => '0'
            ];
        }
        Share::insert($reciepients);
        return redirect('home')->with('toast_success', 'File shared successfully ');

       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Share  $share
     * @return \Illuminate\Http\Response
     */
    public function show(Share $share)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Share  $share
     * @return \Illuminate\Http\Response
     */
    public function edit(Share $share)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Share  $share
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Share $share)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Share  $share
     * @return \Illuminate\Http\Response
     */
    public function destroy(Share $share)
    {
        //
    }
}
