<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;
use App\File;
use App\Folderfiles;
use App\Folder;
use DB;
use User;
use Auth;
Use Alert;


class FolderfilesController extends Controller
{
    public function __construct()
    {
        // This will block anyone who is not registered from continuing with this request
        $this->middleware('auth');
    }
 public function push(){
 
        $files= Folderfiles::all()->sortByDesc('created_at');
        if($files){
        foreach($files as $file){
            $extension = pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION);
            $ext= $extension;
        }
       
      
        $user= Auth::user();

        // $extension = $file->getClientOriginalExtension();

      return view('push')->with ([
          'user'=>  $user,
          'files'=> $files,
          'ext'=> $ext,
      ]);
        }
        return view('push')->with ([
            'user'=>  $user,
        ]);
    
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    //    dd($request->permision);
        $request->validate([
            'folder' => 'required',
            'path' => 'required',
            'permision' => 'required',
            'file' => 'required',
            'file.*' => 'mimes:doc,pdf,docx,zip',
            // 'user' => 'required'
          ]);
        
    $user = Auth::User();
    $user_id = $user->id;
    //  dd($request->hasfile('file'));
          if($request->hasfile('file'))
          {
    
            $file=$request->file('file');
            // dd($file);
            $path=$request->path;
             
                 $name=$file->getClientOriginalName();
                //  dd($name);

                //   dd($file);
                $file->storeAs("/$path/", $name);
                //  $file->move(public_path()."/$path/", $name);  
                 $data = $name;  
             
          }
        //   dd($data);
    
          $folderfile= new Folderfiles;
          $folderfile->filename=$data;
          $folderfile->user_id= $user_id;
          $folderfile->folder_id= $request->folder;
          $folderfile->permision= $request->permision;
        //    dd($folderfile);
         
         $folderfile->save();
    
    
        //    $files=DB::table('files');
           
        //    $files->create([
        //     'filename' => $request->get('filename'),
        //     'overview' => $request->get('overview'),
        //     'user' => $request->get('user')
        //   ]);
    
        //   return back()->with('message', 'Your file is submitted Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user= Auth::user();
        $folders= Folder::all();

      return view('subpush')->with ([
          'user'=>  $user,
          'folders'=>  $folders,
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }















    public function files(Folder $folder)
    {
    $afolders = Folder::where('preceeding_folder_id','=',$folder->id)->get();
        //    dd($afolders);
    $targets = Folder::where('id','=',$folder->id)->get();
            foreach($targets as $target){
            
                $files= Folderfiles::where('folder_id','=',$target->id)->get()->sortByDesc('created_at');
            }
        
    foreach($files as $file){
        if($file){
            $extension = pathinfo(storage_path(Storage::disk('public')->url("$target->path/$file->filename")), PATHINFO_EXTENSION);
            $ext= $extension;
        }
      
    }
    // if(($files)||($afolders)){

    $user= Auth::user();

    // $extension = $file->getClientOriginalExtension();
        //    dd($afolders);
  return view('subpush')->with ([
      'user'=>  $user,
      'files'=> $files,
      'afolders'=> $afolders,
      'target'=> $target,
    //   'ext'=> $ext,
  ]);

    // }

    // return view('subpush')->with ([
    //     'user'=>  $user,
    // ]);
}





   
     
   

}
