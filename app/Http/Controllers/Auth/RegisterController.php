<?php

namespace App\Http\Controllers\Auth;


use App\Notifications\UserRegisteredSuccessfully;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
Use Alert;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
         * Register new account.
         *
         * @param Request $request
         * @return User
         */
        protected function register(Request $request)
        {
            /** @var User $user */
            $validatedData = $request->validate([
                'firstname' => ['required', 'string', 'max:255'],
                'lastname' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'username' => ['required', 'string', 'min:6', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);
            try {
                $validatedData['password'] = Hash::make($validatedData['password']);
                $validatedData['activation_code'] = Str::random(30).time();
                $user = app(User::class)->create($validatedData);
            } catch (\Exception $exception) {
                
                logger()->error($exception);

                // dd($exception);
                // return redirect()->back()->with('message', 'Unable to create new user/User registration unsuccessful.');
                return redirect()->back()->with('toast_error', 'Unable to create new user/User registration unsuccessful.');
            }
            $user->notify(new UserRegisteredSuccessfully($user));
            // return redirect()->back()->with('message', 'Congratulation, Please check your email and activate your account.');
            return redirect()->back()->with('success', 'Congratulation, Please check your email and activate your account.');
        }



     /**
             * Activate the user with given activation code.
             * 
             * @param string $activationCode
             * @return string
             */
            public function activateUser(string $activationCode)
            {
                try {
                    $user = app(User::class)->where('activation_code', $activationCode)->first();
                    if (!$user) {
                        return "Invalid Activation Code.";
                    }
                    $user->status          = 1;
                    $user->activation_code = null;
                    $user->save();
                    auth()->login($user);
                } catch (\Exception $exception) {
                    logger()->error($exception);
                }
    // /**
    //  * Get a validator for an incoming registration request.
    //  *
    //  * @param  array  $data
    //  * @return \Illuminate\Contracts\Validation\Validator
    //  */
    // protected function validator(array $data)
    // {
    //     return Validator::make($data, [
    //         'name' => ['required', 'string', 'max:255'],
    //         'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
    //         'password' => ['required', 'string', 'min:8', 'confirmed'],
    //     ]);
    // }

    // /**
    //  * Create a new user instance after a valid registration.
    //  *
    //  * @param  array  $data
    //  * @return \App\User
    //  */
    // protected function create(array $data)
    // {
    //     return User::create([
    //         'name' => $data['name'],
    //         'email' => $data['email'],
    //         'password' => Hash::make($data['password']),
    //     ]);
     }
}
