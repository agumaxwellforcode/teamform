<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use DateTime;
use RealRashid\SweetAlert\Facades\Alert;



class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::all();
        return view('admin.inbox.index')->with([
            'contacts'=> $contacts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'designation' => 'required',
            'subject' => 'required',
            'comment' => 'required',
          ]);

          Contact::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'designation' => $request->get('designation'),
            'subject' =>  $request->get('subject'),
            'status' =>  "Not Treated",
            'comment' =>  $request->get('comment'),
            'date_treated' =>  "00-00-0000",
          ]);

           return back()->with('success', 'Message sent successfully, Thank you!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        $contact->delete();

        return  back()->with('toast_success', 'Message Deleted Successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function inboxView(Contact $contact)
    {
        // dd($contact);
        // $contact = Contact::all();
        return view('admin.inbox.inboxView')->with([
            'contact' => $contact          
        ]);
    }

    
    
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function inboxTreated(Contact $contact)
    {
        $time = new DateTime();
        $contact = Contact::where('id','=',$contact->id)->first();
       
        // $user = Auth::user();
    //  dd($contact);
       
        $contact->status = 'Treated';
        $contact->date_treated = $time->format('d-m-Y');
       

        $contact->save();

        return redirect()->route('inbox')->with('toast_success','Message Treated !');
    }




     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function inboxProgress(Contact $contact)
    {
        $time = new DateTime();
        $contact = Contact::where('id','=',$contact->id)->first();
       
        // $user = Auth::user();
    //  dd($contact);
       
        $contact->status = 'In Progress';
        // $contact->date_treated = $time->format('d-m-Y');
       

        $contact->save();
        return redirect()->route('inbox')->with('toast_warning','Message Request in Progress !');
    }




    
     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function inboxNotTreated(Contact $contact)
    {
        $time = new DateTime();
        $contact = Contact::where('id','=',$contact->id)->first();
       
        // $user = Auth::user();
    //  dd($contact);
       
        $contact->status = 'Not Treated';
        $contact->date_treated = '00-00-0000';
       

        $contact->save();
        return redirect()->route('inbox')->with('toast_error', '  Message Not Treated !');
    }


}
