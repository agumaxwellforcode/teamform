<?php

namespace App\Http\Controllers;

use App\Subfolder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;
use File;
use App\Folderfiles;
use App\Folder;
use App\Share;
use DB;
use User;
use Auth;
use Illuminate\Support\Facades\Response as FacadeResponse;
Use Alert;


class SubfolderController extends Controller
{
   
    private $prev;
   
    public function subFolderLoad(Folder $folder){
        // dd($folder);
        $user = Auth::user();
        $Folder=Folder::where('id','=',$folder->id)->first();
    //   dd($Folder);
       
        
    return view('subfolderCreate')->with ([
                'user'=>  $user,
                'folder'=>  $folder,
             ]);

    }
   
   
   
   
   
   
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
            $request->validate([
                'folder_id' => 'required',
                'folder_path' => 'required',

                'name' => 'required',
                'permision' => 'required',
                // 'user_id' => 'required',
              ]);

            $user = Auth::user();

//  dd($request->get('folder_id'));
            $this->prev = $request->folder_id;
            
            $folder_id = $request->folder_id;
            $foldername = $request->name;
            $folderpath = $request->folder_path;
      
            $path = ("$folderpath/$foldername");
          
            // if(!Storage::disk('public')->getVisibility($path)){
    
                Storage::disk('public')->makeDirectory($path, 0777, true, true);
    
              
               
            
        
                  Folder::create([
                    'name' => $request->get('name'),
                    'permision' => $request->get('permision'),
                    'user_id' => $user->id,
                    'path' => $path,
                    'preceeding_folder_id' => $request->get('folder_id'),
                  ]);
    
                return redirect()->route('subhome',$folder_id)->with('toast_success', 'Folder created successfully');
    
       
           
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subfolder  $subfolder
     * @return \Illuminate\Http\Response
     */
    public function show(Subfolder $subfolder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subfolder  $subfolder
     * @return \Illuminate\Http\Response
     */
    public function edit(Subfolder $subfolder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subfolder  $subfolder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subfolder $subfolder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subfolder  $subfolder
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subfolder $subfolder)
    {
        //
    }


        public function back(){
            
           

            // return Redirect::to($request->request->get('http_referrer'));
        }































        public function subVideo(Folderfiles $file)
        {
            $file->filename;
            $folder= Folder::where('id','=',$file->folder_id)->first();
            $path=$folder->path;
            // dd($path);
            $fileContents = Storage::disk('public')->get("$path/$file->filename");
            $response = FacadeResponse::make($fileContents, 200);
            $response->header('Content-Type', "video/mp4");
        
        //    $response = Storage::disk('public')->get("files/$file->filename");
                    $response= "storage/$path/$file->filename" ;
        
            return view('video')->with ([
                'response'=>  $response
            ]);
        }
        
        
        public function subaudio(Folderfiles $file)
        {
            $file->filename;
            $folder= Folder::where('id','=',$file->folder_id)->first();
            $path=$folder->path;
            // dd($path);
            $fileContents = Storage::disk('public')->get("$path/$file->filename");
            $response = FacadeResponse::make($fileContents, 200);
            $response->header('Content-Type', "audio/mp3");
        
        //    $response = Storage::disk('public')->get("files/$file->filename");
                    $response= "storage/$path/$file->filename" ;
        
            return view('audio')->with ([
                'response'=>  $response
            ]);
        }
        
        public function subimage(Folderfiles $file)
        {
            $file->filename;
            $folder= Folder::where('id','=',$file->folder_id)->first();
            $path=$folder->path;
            // dd($path);
            $fileContents = Storage::disk('public')->get("$path/$file->filename");
            $response = FacadeResponse::make($fileContents, 200);
            $response->header('Content-Type', "image/*");
        
        //    $response = Storage::disk('public')->get("files/$file->filename");
                    $response= "storage/$path/$file->filename" ;
        
            return view('image')->with ([
                'response'=>  $response
            ]);
        }
        
        
        public function subtext(Folderfiles $file)
        {
            $file->filename;
            $folder= Folder::where('id','=',$file->folder_id)->first();
            $path=$folder->path;
            // dd($path);
            $fileContents = Storage::disk('public')->get("$path/$file->filename");
            
            $response = FacadeResponse::make($fileContents, 200);
            $response->header('Content-Type',"text/docx,text/doc");
        
        //    $response = Storage::disk('public')->get("files/$file->filename");
                    $response= "storage/$path/$file->filename" ;
        
            return view('text')->with ([
                'response'=>  $response
            ]);
        }
        
        
            public function subsheet(Folderfiles $file)
            {
                $file->filename;
                $folder= Folder::where('id','=',$file->folder_id)->first();
                $path=$folder->path;
                // dd($path);
                $fileContents = Storage::disk('public')->get("$path/$file->filename");
                $response = FacadeResponse::make($fileContents, 200);
                $response->header('Content-Type', "sheets/xls.csv");
        
            //    $response = Storage::disk('public')->get("files/$file->filename");
                        $response= "storage/$path/$file->filename" ;
        
                return view('sheet')->with ([
                    'response'=>  $response
                ]);
            }
        
        
        
            public function subpdf(Folderfiles $file)
            {
                $file->filename;
                $folder= Folder::where('id','=',$file->folder_id)->first();
                $path=$folder->path;
                // dd($path);
                $fileContents = Storage::disk('public')->get("$path/$file->filename");
                $response = FacadeResponse::make($fileContents, 200);
                $response->header('Content-Type', "application/pdf");
        
            //    $response = Storage::disk('public')->get("files/$file->filename");
                        $response= "storage/$path/$file->filename" ;
        
                return view('pdf')->with ([
                    'response'=>  $response
                ]);
            }
        
            public function subslide(Folderfiles $file)
            {
                $file->filename;
                $folder= Folder::where('id','=',$file->folder_id)->first();
                $path=$folder->path;
                // dd($path);
                $fileContents = Storage::disk('public')->get("$path/$file->filename");
                $response = FacadeResponse::make($fileContents, 200);
                $response->header('Content-Type', "application/pptx");
        
            //    $response = Storage::disk('public')->get("files/$file->filename");
                        $response= "storage/$path/$file->filename" ;
        
                return view('slide')->with ([
                    'response'=>  $response
                ]);
            }
        
        











            public function deleteSubFile(Folderfiles $file)
            {
                $file->filename;
                //  dd($file->id);
                $folder= Folder::where('id','=',$file->folder_id)->first();
                $path=$folder->path;
                $item = Folderfiles::where('id','=',$file->id)->first();
        
                $shares = Share::where('file_id','=',$file->id)->get();
                
                foreach ($shares as $share){
                    // dd($share->id);
                    $share->delete();
                }
                
                $item->delete();
        
                Storage::disk('public')->delete("$path/$file->filename");
        
                return  back()->with('message', 'File Deleted Successfully');
        
        
            }




            public function deleteSUbFolder(Folder $folder)
            {
                // dd($folder);
        
        
                $item = Folder::where('id','=',$folder->id)->first();
        
                $subfolders = Folder::where('preceeding_folder_id','=',$folder->id)->get();
        
                $contents = Folderfiles::where('folder_id','=',$folder->id)->get();
        
        
                foreach ($subfolders as $subfolder){
                
                    $ffiles = Folderfiles::where('folder_id','=',$subfolder->id)->get();
                    $ffiles->delete();
                    $subfolder->delete();
                    
                   }
                
                foreach ($contents as $content){
                     
                      $content->delete();
                      Storage::disk('public')->delete("$folder->path/$content->filename");
               
                    }
        
        
                
                $item->delete();
        
                Storage::disk('public')->deleteDirectory("$folder->name");
        
                return  back()->with('message', 'Folder and its contents Deleted Successfully');
        
        
        
            }




}
