<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gate;

use Auth;
use User;
use App\File;
use App\Share;
use App\Folder;
use App\Folderfiles;
use Illuminate\Support\Facades\Storage;
Use Alert;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        if (Gate::denies('activated-user')) {
            
            return view('activation');
        }
        $user= Auth::user();


        $publicFolders= Folder::where('preceeding_folder_id','=','0')->where('permision','=','all')->get();
        $privateFolders= Folder::where('preceeding_folder_id','=','0')->where('user_id','=',$user->id)->where('permision','=','me')->get();




        $publicFiles= File::where('folder_id','=','0')->where('user_id','=',$user->id)->where('permision','=','Private')->get();

        foreach($publicFiles as $publicFile){
            $extension = pathinfo(storage_path(Storage::disk('local')->url("files/$publicFile->filename")), PATHINFO_EXTENSION);
        }




        $privateFiles= File::where('folder_id','=','0')->where('user_id','=',$user->id)->where('permision','=','Private')->get();

        foreach($privateFiles as $privateFile){
            $extension = pathinfo(storage_path(Storage::disk('local')->url("files/$publicFile->filename")), PATHINFO_EXTENSION);
        }


        $shares = Share::where('reciepient_id','=',$user->id)->get();
      
       

             foreach($shares as $share){
        

           
                // echo($share->file_id);    
          
            
               
           
          
             }     
           
            //  

            
      
        $user= Auth::user();

        return view('home')->with ([
            'user'=>  $user,

            'publicFiles'=> $publicFiles,
            'privateFiles'=> $privateFiles,

            'publicFolders'=> $publicFolders,
            'privateFolders'=> $privateFolders,

            'shares'=> $shares,
           

            // 'extension'=> $extension,
        ]);;
    }


    function action(Request $request)
    {
     if($request->ajax())
     {
      $output = '';
      $query = $request->get('query');
      if($query != '')
      {
       $data = File::where('filename', 'like', '%'.$query.'%')
         ->orderBy('id', 'desc')
         ->get();
         
      }
     
      $total_row = $data->count();
      if($total_row > 0)
      {
       foreach($data as $row)
       {
            if (pathinfo(storage_path(Storage::disk('public')->url("files/$row->filename")), PATHINFO_EXTENSION) =='docx' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='DOCX'|| pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='DOC' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='doc' ) {
               
                $output .= '
                <tr class="mb-0 mt-0">
                <td class="text-left "><div class="alert alert-light mb-0" role="alert"> <a href="{{route(text,$row)}}" class="slink" ><img class="img img-fluid rounded float-left " style="object-fit: cover; height:25px; width:25px" src="/images/icons/docs.svg" alt="" srcset=""> &nbsp;&nbsp;'.$row->filename. '&nbsp;&nbsp;&nbsp;&nbsp;'.$row->path.'</a></div></td>
               
                </tr>
                ';
            }
            elseif (pathinfo(storage_path(Storage::disk('public')->url("files/$row->filename")), PATHINFO_EXTENSION) =='csv' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='xlsx' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='XLSX') {
               
                $output .= '
                <tr class="mb-0 mt-0">
                <td class="text-left "><div class="alert alert-light mb-0" role="alert"> <a href="{{route(text,$row)}} class="slink" ><img class="img img-fluid rounded float-left " style="object-fit: cover; height:25px; width:25px" src="/images/icons/sheets.svg" alt="" srcset=""> &nbsp;&nbsp;'.$row->filename.'&nbsp;&nbsp;&nbsp;&nbsp;'.$row->path.'</a></div></td>
                </tr>
                ';
            }
            elseif (pathinfo(storage_path(Storage::disk('public')->url("files/$row->filename")), PATHINFO_EXTENSION) =='ppt' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='PPT' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='pptx'|| pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='PPTX') {
               
                $output .= '
                <tr class="mb-0 mt-0">
                <td class="text-left "><div class="alert alert-light mb-0" role="alert"> <a href="{{route(text,$row)}} class="slink" ><img class="img img-fluid rounded float-left " style="object-fit: cover; height:25px; width:25px" src="/images/icons/slides.svg" alt="" srcset=""> &nbsp;&nbsp;'.$row->filename.'&nbsp;&nbsp;&nbsp;&nbsp;'.$row->path.'</a></div></td>
                </tr>
                ';
            }
            elseif (pathinfo(storage_path(Storage::disk('public')->url("files/$row->filename")), PATHINFO_EXTENSION) =='pdf' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='PDF' ) {
               
                $output .= '
                <tr class="mb-0 mt-0">
                <td class="text-left "><div class="alert alert-light mb-0" role="alert"> <a href="{{route(text,$row)}} class="slink" ><img class="img img-fluid rounded float-left " style="object-fit: cover; height:25px; width:25px" src="/images/icons/pdf.svg" alt="" srcset=""> &nbsp;&nbsp;'.$row->filename.'&nbsp;&nbsp;&nbsp;&nbsp;'.$row->path.'</a></div></td>
                </tr>
                ';
            }
            elseif (pathinfo(storage_path(Storage::disk('public')->url("files/$row->filename")), PATHINFO_EXTENSION) =='jpg' || pathinfo(storage_path(Storage::disk('public')->url("files/$row->filename")), PATHINFO_EXTENSION) =='JPG' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='jpeg' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='JPEG' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='png' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='PNG') {
              
                $output .= '
                <tr class="mb-0 mt-0">
                <td class="text-left "><div class="alert alert-light mb-0" role="alert"> <a href="{{route(text,$row)}} class="slink" ><img class="img img-fluid rounded float-left " style="object-fit: cover; height:25px; width:25px" src="{{url("storage/files/$row->filename")}}" alt="" srcset=""> &nbsp;&nbsp;'.$row->filename.'&nbsp;&nbsp;&nbsp;&nbsp;'.$row->path.'</a></div></td>
                </tr>
                ';
            }
            elseif (pathinfo(storage_path(Storage::disk('public')->url("files/$row->filename")), PATHINFO_EXTENSION) =='mp3' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='MP3' ) {
               
                $output .= '
                <tr class="mb-0 mt-0">
                <td class="text-left "><div class="alert alert-light mb-0" role="alert"> <a href="{{route(text,$row)}} class="slink" ><img class="img img-fluid rounded float-left " style="object-fit: cover; height:25px; width:25px" src="/images/icons/mp3.svg" alt="" srcset=""> &nbsp;&nbsp;'.$row->filename.'&nbsp;&nbsp;&nbsp;&nbsp;'.$row->path.'</a></div></td>
                </tr>
                ';
            }
            elseif (pathinfo(storage_path(Storage::disk('public')->url("files/$row->filename")), PATHINFO_EXTENSION) =='mp4' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='MP4' ) {
               
                $output .= '
                <tr class="mb-0 mt-0">
                <td class="text-left "><div class="alert alert-light mb-0" role="alert"> <a href="{{route(text,$row)}} class="slink" ><img class="img img-fluid rounded float-left " style="object-fit: cover; height:25px; width:25px" src="/images/icons/cone.svg" alt="" srcset=""> &nbsp;&nbsp;'.$row->filename.'&nbsp;&nbsp;&nbsp;&nbsp;'.$row->path.'</a></div></td>
                </tr>
                ';
            }
                // $output .= '
                // <tr class="mb-0 mt-0">
                // <td class="text-left "><div class="alert alert-light mb-0" role="alert">'.$row->filename.'</div></td>
                // </tr>
                // ';
       }

       
      }
      else if($total_row <= 0)
      {
      $data = Folderfiles::where('filename', 'like', '%'.$query.'%')
      ->orderBy('id', 'desc')
      ->get();
    
     $total_row = $data->count();
     if($total_row > 0)
     {
      foreach($data as $row)
      {
           if (pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='docx' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='DOCX'|| pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='DOC' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='doc' ) {
              
               $output .= '
               <tr class="mb-0 mt-0">
               <td class="text-left "><div class="alert alert-light mb-0" role="alert"> <a href="{{route(text,$row)}}" class="slink" ><img class="img img-fluid rounded float-left " style="object-fit: cover; height:25px; width:25px" src="/images/icons/docs.svg" alt="" srcset=""> &nbsp;&nbsp;'.$row->filename. '&nbsp;&nbsp;&nbsp;&nbsp;'.$row->path.'</a></div></td>
              
               </tr>
               ';
           }
           elseif (pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='csv' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='xlsx' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='XLSX') {
              
               $output .= '
               <tr class="mb-0 mt-0">
               <td class="text-left "><div class="alert alert-light mb-0" role="alert"> <a href="{{route(text,$row)}} class="slink" ><img class="img img-fluid rounded float-left " style="object-fit: cover; height:25px; width:25px" src="/images/icons/sheets.svg" alt="" srcset=""> &nbsp;&nbsp;'.$row->filename.'&nbsp;&nbsp;&nbsp;&nbsp;'.$row->path.'</a></div></td>
               </tr>
               ';
           }



           elseif (pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='jpg' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='JPG' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='jpeg' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='JPEG' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='png' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='PNG') {
              
            $output .= '
            <tr class="mb-0 mt-0">
            <td class="text-left "><div class="alert alert-light mb-0" role="alert"> <a href="{{route(text,$row)}} class="slink" ><img class="img img-fluid rounded float-left " style="object-fit: cover; height:25px; width:25px" src="{{url("storage/$row->path/$row->filename")}}" alt="" srcset=""> &nbsp;&nbsp;'.$row->filename.'&nbsp;&nbsp;&nbsp;&nbsp;'.$row->path.'</a></div></td>
            </tr>
            ';
        }



           elseif (pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='ppt' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='PPT' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='pptx'|| pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='PPTX') {
              
               $output .= '
               <tr class="mb-0 mt-0">
               <td class="text-left "><div class="alert alert-light mb-0" role="alert"> <a href="{{route(text,$row)}} class="slink" ><img class="img img-fluid rounded float-left " style="object-fit: cover; height:25px; width:25px" src="/images/icons/slides.svg" alt="" srcset=""> &nbsp;&nbsp;'.$row->filename.'&nbsp;&nbsp;&nbsp;&nbsp;'.$row->path.'</a></div></td>
               </tr>
               ';
           }
           elseif (pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='pdf' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='PDF' ) {
              
               $output .= '
               <tr class="mb-0 mt-0">
               <td class="text-left "><div class="alert alert-light mb-0" role="alert"> <a href="{{route(text,$row)}} class="slink" ><img class="img img-fluid rounded float-left " style="object-fit: cover; height:25px; width:25px" src="/images/icons/pdf.svg" alt="" srcset=""> &nbsp;&nbsp;'.$row->filename.'&nbsp;&nbsp;&nbsp;&nbsp;'.$row->path.'</a></div></td>
               </tr>
               ';
           }
           elseif (pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='mp3' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='MP3' ) {
              
               $output .= '
               <tr class="mb-0 mt-0">
               <td class="text-left "><div class="alert alert-light mb-0" role="alert"> <a href="{{route(text,$row)}} class="slink" ><img class="img img-fluid rounded float-left " style="object-fit: cover; height:25px; width:25px" src="/images/icons/mp3.svg" alt="" srcset=""> &nbsp;&nbsp;'.$row->filename.'&nbsp;&nbsp;&nbsp;&nbsp;'.$row->path.'</a></div></td>
               </tr>
               ';
           }
           elseif (pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='mp4' || pathinfo(storage_path(Storage::disk('public')->url("$row->path/$row->filename")), PATHINFO_EXTENSION) =='MP4' ) {
              
               $output .= '
               <tr class="mb-0 mt-0">
               <td class="text-left "><div class="alert alert-light mb-0" role="alert"> <a href="{{route(text,$row)}} class="slink" ><img class="img img-fluid rounded float-left " style="object-fit: cover; height:25px; width:25px" src="/images/icons/cone.svg" alt="" srcset=""> &nbsp;&nbsp;'.$row->filename.'&nbsp;&nbsp;&nbsp;&nbsp;'.$row->path.'</a></div></td>
               </tr>
               ';
           }
               // $output .= '
               // <tr class="mb-0 mt-0">
               // <td class="text-left "><div class="alert alert-light mb-0" role="alert">'.$row->filename.'</div></td>
               // </tr>
               // ';
      }

    }
     }
      else
      {
       $output = '
       <tr>
        <td align="" colspan="5"><div class="alert alert-light mb-0 text-danger" role="alert">No Result for the search</div></td>
       </tr>
       ';
      }
      
      $data = array(
       'table_data'  => $output,
       'total_data'  => $total_row
      );

      echo json_encode($data);
     }
    }
























// /
// if($share->folder_id ==  '0'){

//     $sharedFiles= File::where('id','=',$share->file_id)->where('filename','=',$share->filename)->get();
//     // dd($sharedFiles); 
    
// }else
// {
//     $sharedFiles= Folderfiles::where('id','=',$share->file_id)->where('filename','=',$share->filename)->get();
   
 }
