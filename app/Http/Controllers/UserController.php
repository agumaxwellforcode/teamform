<?php

namespace App\Http\Controllers;


use Auth;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
Use Alert;


class UserController extends Controller
{
    public function profile(){
        $user = Auth::user();
        return view('profile',compact('user',$user));
    }


    public function update_avatar(Request $request){

        if ($request->hasFile('avatar'))
            {
                $request->validate([
                    'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'firstname' => 'required|min:3',
                    'lastname' => 'required|min:3',
                    'username' => 'required|min:3',
                    
                   
                ]);
        
                $user = Auth::user();
        
                $avatarName = $user->id.'_avatar'.time().'.'.request()->avatar->getClientOriginalExtension();
        
                $request->avatar->storeAs('avatars',$avatarName);
        
                $user->avatar = $avatarName;
                $user->firstname = request()->firstname;
                $user->lastname = request()->lastname;
                $user->username = request()->username;
               
                $user->save();
        
                return back()
                    ->with('toast_success','Profile information Updated successfully. ');
        
            }
            

                $request->validate([
                    // 'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'firstname' => 'required|min:3',
                    'lastname' => 'required|min:3',
                    'username' => 'required|min:3',
                    
                
                ]);

                $user = Auth::user();

                // $avatarName = $user->id.'_avatar'.time().'.'.request()->avatar->getClientOriginalExtension();

                // $request->avatar->storeAs('avatars',$avatarName);

                // $user->avatar = $avatarName;
                $user->firstname = request()->firstname;
                $user->lastname = request()->lastname;
                $user->username = request()->username;
            
                $user->save();

                return back()
                    ->with('toast_success','Profile information Updated successfully. ');

            }

            public function update_signature(Request $request){

                if ($request->hasFile('signature'))
                    {
                        $request->validate([
                            'signature' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                        ]);
                
                        $user = Auth::user();
                
                        $signatureName = 'signature'.time().'.'.request()->signature->getClientOriginalExtension();
                
                        $request->signature->storeAs('signatures',$signatureName);
                
                        $user->signature = $signatureName;
                        $user->save();
                
                        return back()
                            ->with('toast_success','Signature Updated successfully. ');
                
                    }
                }
}
