<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Gate;
use Illuminate\Http\Request;
Use Alert;

class UserController extends Controller
{

    public function _construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users =User::all();  
       return view('admin.users.index')-> with('users', $users);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if (Gate:: denies('edit-users')){
            return redirect(route('admin.users.index'))->with('toast_error','You dont have Clearance to perform this action');
        }
       $roles=Role::all();

       return view('admin.users.edit')->with ([
           'user' => $user,
           'roles' => $roles
       ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
       $user->roles()->sync($request->roles);

       return redirect()->route('admin.users.index');
    }



     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function registration(Request $request, User $user)
    {
       $user->roles()->sync($request->roles);

       return redirect()->route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {

        if (Gate:: denies('edit-users')){
            return redirect(route('admin.users.index'));
        }
        
       $user -> roles()->detach();
       $user -> groups()->detach();
       $user->delete();

       return redirect()->route('admin.users.index')->with('toast_success','User Deleted Successfully');
    }
}
