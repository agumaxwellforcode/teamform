<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Notification;
use App\FileNotification;
use App\User;
use DB;
use Auth;
use DateTime;
use App\Announcement;
use Illuminate\Http\Request;
Use Alert;


class notificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $notifications =  Notification::where('recipient_user_id', '=',$user->id)->get();
           
        if(!$notifications->isEmpty()){
                
            return view('notification')->with(
                    'notifications', $notifications,
                    'user', $user
                );
            }
            return view('notification')->with(
                    'notifications', $notifications,
                    'user', $user
            );
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $time = new DateTime();
        $time->format('Y-m-d H:i:s');
        // dd($time->format('H:i:s d-m-Y'));
        $user = Auth::user();

        for ($i = 0; $i < count($request->user); $i++) {

            $time = new DateTime();
            $time->format('Y-m-d H:i:s');

            $reciepients[] = [
                'announcement_id' => $request->announcement_id,
                'sender_id' => $user->id,
                'recipient_user_id' => $request->user[$i],
                'seen' => "0",
                'time_sent' => $time->format('H:i:s'),
                'time_seen' => 'Not Seen'
            ];
        }
        Notification::insert($reciepients);
        
        $announcements = Announcement::all(); 
       
        return redirect()->route('admin.announcement.index')->with('toast_success','Notification Sent!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function show($announcement)

    {
        $notifications= Announcement::where('id','=',$announcement)->get();
       
                    foreach ($notifications as $notification){

                        // dd($notification->message);

                        $users=User::all();
                        return view('admin.notification.index')->with ([
                            'notification' => $notification,
                            'users' => $users
                        ]);

                        }


       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function edit(Notification $notification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notification $notification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notification $notification)
    {
        //
    }











    public function notificationview($announcement)

    {

        $time = new DateTime();
        $time->format('Y-m-d H:i:s');

        $display = Announcement::where('id','=',$announcement)->first();
       
        $user = Auth::user();
        $notifications =  Notification::where('recipient_user_id', '=',$user->id)->get();
            
        // dd($notifications);

        $notification =  Notification::where('recipient_user_id', '=',$user->id)
                                     ->where('announcement_id', '=',$announcement)
                                     ->first();
        //    dd($message);
       
        $notification->seen = '1';
        $notification->time_seen = $time->format('H:i:s d-m-Y');

        $notification->save();
                
            return view('notificationview')->with([
                
                'display'=> $display,
                'displayType'=> '0',
                'displayf'=> [],
                ]);
                
       
    }



    public function notificationDelete($notification)

    {
      
        $user = Auth::user();
       
        $notification =  Notification::where('recipient_user_id', '=',$user->id)
                                     ->where('announcement_id', '=',$notification)
                                     ->first();
        //    dd($notification);
   

        $notification->delete();
                
            return redirect()->route('admin.notification.index')->with('toast_success', 'Notification successfully removed.');
                
       
    }



}
