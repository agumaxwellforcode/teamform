<?php

namespace App\Http\Controllers\Admin;

use App\Announcement;
use Gate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
Use Alert;


class announcementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $announcements =Announcement::all();  
        return view('admin.announcement.index')-> with('announcements', $announcements);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.announcement.createannouncement');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tittle' => 'required|min:5',
            'sender' => 'required|min:5',
            'message' => 'required|min:10'
        ]);

        Announcement::create($request->only(['tittle', 'sender', 'message']));

        return redirect()->route('admin.announcement.index')->with('toast_success', 'Announcement created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function show(Announcement $announcement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcement $announcement)
    {
        if (Gate:: denies('edit-users')){
            return redirect(route('admin.announcement.index'));
        }
      

       return view('admin.announcement.edit')->with ([
           'announcement' => $announcement,
       ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announcement $announcement)
    {
        //
        $request->validate([
            'tittle' => 'required|min:5',
            'sender' => 'required|min:5',
            'message' => 'required|min:10'
            ]);
    
            $announcement->update($request->all());
    
            return redirect()->route('admin.announcement.index')->with('toast_success','Announcement updated successfully');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcement $announcement)
    {
        $announcement->delete();
        return redirect()->route('admin.announcement.index')->with('toast_success', 'Announcement successfully removed.');
    }
}
