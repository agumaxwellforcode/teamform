<?php

namespace App\Http\Controllers\Admin;

use App\Group;
use App\User;
use Gate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
Use Alert;


class groupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups =Group::all();  
        return view('admin.group.index')-> with('groups', $groups);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.group.creategroup');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:5',
            'description' => 'required|min:20'
        ]);

        Group::create($request->only(['name', 'description']));

        return redirect()->route('admin.group.index')->with('toast_success', 'Group created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        if (Gate:: denies('edit-users')){
            return redirect(route('admin.group.index'));
        }
       $users=User::all();

       return view('admin.group.edit')->with ([
           'group' => $group,
           'users' => $users
       ]);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function editGroupUsers(Group $group)
    {
        if (Gate:: denies('edit-users')){
            return redirect(route('admin.group.index'));
        }
       $users=User::all();

       return view('admin.group.edit')->with ([
           'group' => $group,
           'users' => $users
       ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        $group->users()->sync($request->users);

        return redirect()->route('admin.group.index')->with('toast_success', 'User added to Group.');
    
    }


     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function registration(Request $request, Group $group)
    {
        $group->users()->sync($request->users);

        return redirect()->route('admin.group.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        $group -> users()->detach();
        $group->delete();
 
        return redirect()->route('admin.group.index')->with('toast_success', 'Group successfully removed.');
    }
}
