<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Gate;
use Illuminate\Http\Request;
Use Alert;

class registrationController extends Controller
{
   
    public function _construct(){

       
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users =User::all();  
        return view('admin.users.registration')-> with('users', $users);
    }

   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $registration)
    {
        if (Gate:: denies('edit-users')){
            return redirect(route('admin.registration.index'));
        }
        $roles=Role::all();
        
        return view('admin.users.activateregistration')->with ([
            'user' => $registration,
        ]);
       
     }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $registration)
    {

        // $request->validate([
        //     'activate' => 'required',
        // ]);

            $registration->activated = $request->input('activate');
            $registration->update();
           
            if($registration->activated == 0){
                return back()
                ->with('toast_success','User deactivation successful');
            }else{
                return back()
                ->with('toast_success','User activation successful');
            }
            
            //  $activated =  $user->activated;

        // if($activated == 1){

        //     $user->activated == 1;
        //     $user->save();

        //     return back()
        //     ->with('success','User Activated. ');
        // }
       
        // return back()
        // ->with('problem','User is a problem. ');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
