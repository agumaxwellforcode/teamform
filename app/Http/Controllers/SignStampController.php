<?php

namespace App\Http\Controllers;

use App\SignStamp;
use App\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response as FacadeResponse;
use Auth;
use App\User;
use Illuminate\Http\File as signedFile;
use Illuminate\Support\Facades\Storage;
use PDF;
use NcJoes\OfficeConverter\OfficeConverter;
use Alert;
use App\FileNotification;
use Image;


// use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\SimpleType\TblWidth;
use PhpOffice\PhpWord\Style\Table;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\Writer\Word2007\Element\Container;
use PhpOffice\Common\XMLWriter;
use ZipArchive;


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \PhpOffice\PhpSpreadsheet\Reader\IReader;
use \PhpOffice\PhpSpreadsheet\Reader\IWriter ;
use \PhpOffice\PhpSpreadsheet\IOFactory;
use \PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use \PhpOffice\PhpSpreadsheet\Worksheet\HeaderFooterDrawing;

class SignStampController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $signStamp = $request->code;
        $signSearch = SignStamp::where('code', '=', $signStamp)->first();
        if(!$signSearch){
            return back()
            ->with('warning', 'Authorization does not exist');
            // ->with('toast_success','Profile information Updated successfully. ');
        }
        elseif($signSearch){
            return view('exist')->with(['signature' => $signSearch]);
            // ->with('success','Signature is valid on '.$signSearch->filename)->timerProgressBar();
        }
    }
    
     /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SignStamp  $signStamp
     * @return \Illuminate\Http\Response
     */
    public function downloadSigned($signStamp)
    {
        $file = SignStamp::where('file_id', '=', $signStamp)->where('action', '=', "Signed a documented that you requested")->first();
        //  dd( $file);
        $realname= str_replace(".docx","",$file->filename);
        $response= "storage/signed/$realname" ;
        // dd($response);
        return Storage::disk('public')->download("signed/Signed_$file->filename");
        // return response()->download($response);
    }
    public function downloadStamped($signStamp)
    {
        $file = SignStamp::where('file_id', '=', $signStamp)->where('action', '=', "Stamped a documented that you requested")->first();
        //  dd( $file);
        $realname= str_replace(".docx","",$file->filename);
        $response= "storage/signed/$realname" ;
        // dd($response);
        return Storage::disk('public')->download("signed/Stamped_$file->filename");
        // return response()->download($response);
    }

    public function downloadAuthImage($signStamp)
    {
        $file = SignStamp::where('file_id', '=', $signStamp)->where('action', '=', "Authorized a documented that you requested")->first();
        
        return Storage::disk('public')->download("signed/$file->filename");
        // return response()->download($response);
    }

    public function downloadsignstamp($signStamp)
    {
        $file = SignStamp::where('file_id', '=', $signStamp)->where('action', '=', "Stamped and signed a documented that you requested")->first();
        // dd( $file);
        if(pathinfo(storage_path(Storage::disk('public')->url($file->filename)), PATHINFO_EXTENSION) =='xlsx'){
            $realname= str_replace(".xlsx",".xls",$file->filename);
            return Storage::disk('public')->download("signed/Signed and Stamped_$realname");

        }elseif(pathinfo(storage_path(Storage::disk('public')->url($file->filename)), PATHINFO_EXTENSION) =='csv'){
            $realname= str_replace(".csv",".xls",$file->filename);
            return Storage::disk('public')->download("signed/Signed and Stamped_$realname");

        }elseif(pathinfo(storage_path(Storage::disk('public')->url($file->filename)), PATHINFO_EXTENSION) =='xls'){
            $realname= str_replace(".xls",".xls",$file->filename);
            return Storage::disk('public')->download("signed/Signed and Stamped_$realname");

        }elseif(pathinfo(storage_path(Storage::disk('public')->url($file->filename)), PATHINFO_EXTENSION) =='docx'){
           $realname= str_replace(".docx","",$file->filename);
           return Storage::disk('public')->download("signed/Signed and Stamped_$file->filename");
        }
        // $realname= str_replace(".docx","",$file->filename);
        // $response= "storage/signed/$realname" ;
        // return Storage::disk('public')->download("signed/Signed and Stamped_$file->filename");
        // return response()->download($response);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\SignStamp  $signStamp
     * @return \Illuminate\Http\Response
     */
    public function show(SignStamp $signStamp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SignStamp  $signStamp
     * @return \Illuminate\Http\Response
     */
    public function edit(SignStamp $signStamp)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SignStamp  $signStamp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SignStamp $signStamp)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SignStamp  $signStamp
     * @return \Illuminate\Http\Response
     */
    public function destroy(SignStamp $signStamp)
    {






        return response()->download(storage_path('helloWorld.docx'));
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   

     public function sign(File $file)
     {
        $file->filename;
        $document= "storage/files/$file->filename" ;
        $owner = User::where('id','=',$file->user_id)->first();
        $signer = Auth::user();
        $signature = "storage/signatures/$signer->signature" ;


        // Available alpha caracters
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // generate a pin based on 2 * 7 digits + a random character
        $pin = mt_rand(1000000, 9999999)
            . mt_rand(1000000, 9999999)
            . $characters[rand(0, strlen($characters) - 1)];

        // shuffle the result
        $code = str_shuffle($pin);


        


        // dd($signature);

        require_once '../vendor/autoload.php';
        // \PhpOffice\PhpWord\Settings::setPdfRendererPath('../vendor/dompdf/dompdf');
        // \PhpOffice\PhpWord\Settings::setPdfRenderername('DomPDF');

         \PhpOffice\PhpWord\Settings::setPdfRendererPath('../vendor/mpdf/mpdf');
         \PhpOffice\PhpWord\Settings::setPdfRenderername(Settings::PDF_RENDERER_MPDF);

        // \PhpOffice\PhpWord\Settings::setPdfRendererPath('../vendor/tecnickcom/tcpdf');
        // \PhpOffice\PhpWord\Settings::setPdfRenderername(Settings::PDF_RENDERER_TCPDF);

        
        // \PhpOffice\PhpWord\Settings::setZipClass(\PhpOffice\PhpWord\Settings::ZipArchive);

        // \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($document);

        //static zone
        // $templateProcessor->setValue('date', htmlspecialchars(date('d.m.Y G:i:s'))); 



        $templateProcessor = new TemplateProcessor($document);
        $font = array('name' => 'Minion Pro', 'size' => 8, 'bold' => true);
        $templateProcessor->setValue('sign', ' <w:br/> Signed:     '.$signer->firstname .' '. $signer->lastname.'<w:br/> ${UserLogo} '. date("d-m-Y").'<w:br/> ${code}');
        // $templateProcessor->setValue('file', 'File:               '.$file->filename);
        $templateProcessor->setValue('code', $code);
        // $templateProcessor->setImageValue('CompanyLogo', $signature);
        $templateProcessor->setImageValue('UserLogo', array('path' => $signature, 'width' => 100, 'height' => 80, 'ratio' => true));
        // echo date('H:i:s'), ' Saving the result document...';


        

        try {
                $realname= str_replace(".docx","",$file->filename);
                $templateProcessor->saveAs('storage/signed/Signed_'.$file->filename);


                


                SignStamp::create([
                    'file' =>  $document,
                    'filename' =>  $file->filename,
                    'file_id' =>  $file->id,
                    'owner' =>  $file->user_id,
                    'action' =>   "Signed a documented that you requested",
                    'actioneer' =>  $signer->id,
                    'signature' =>  $signature,
                    'stamp' =>  "NO",
                    'code' =>  $code,
                  ]);

                  FileNotification::create([
                    'url' => $file->id,
                    'owner' => $file->user_id,
                    'filename' => 'Signed_'.$file->filename,
                    'actioneer' => $signer->id,
                    'action' => "Signed a documented that you requested",
                    'seen' =>  "0",
                    'created_at' =>  now(),
                    'updated_at' =>  now(),
                  ]);
             //Load temp file

                $phpWord = \PhpOffice\PhpWord\IOFactory::load('Signed_'.$file->filename); 
                
  
             //Save it

                $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
                $xmlWriter->save('Signed_'.$realname.'.pdf', true, 'UTF-8', false); 
            
            } catch (Exception $e) {

            } 

            // return response()->download('Signed_'.$realname.'.pdf');
            return back()->with('success','Document has been signed');
                // $templateProcessor->saveAs($document);

    }











    public function stamp(File $file)
     {
        $file->filename;
        $document= "storage/files/$file->filename" ;
        $owner = User::where('id','=',$file->user_id)->first();
        $signer = Auth::user();
        $stamp = "storage/signatures/stamp.jpg" ;


        // Available alpha caracters
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // generate a pin based on 2 * 7 digits + a random character
        $pin = mt_rand(1000000, 9999999)
            . mt_rand(1000000, 9999999)
            . $characters[rand(0, strlen($characters) - 1)];

        // shuffle the result
        $code = str_shuffle($pin);

        // dd($signature);

        require_once '../vendor/autoload.php';
        // \PhpOffice\PhpWord\Settings::setPdfRendererPath('../vendor/dompdf/dompdf');
        // \PhpOffice\PhpWord\Settings::setPdfRenderername('DomPDF');

         \PhpOffice\PhpWord\Settings::setPdfRendererPath('../vendor/mpdf/mpdf');
         \PhpOffice\PhpWord\Settings::setPdfRenderername(Settings::PDF_RENDERER_MPDF);

        // \PhpOffice\PhpWord\Settings::setPdfRendererPath('../vendor/tecnickcom/tcpdf');
        // \PhpOffice\PhpWord\Settings::setPdfRenderername(Settings::PDF_RENDERER_TCPDF);

        
        // \PhpOffice\PhpWord\Settings::setZipClass(\PhpOffice\PhpWord\Settings::ZipArchive);

        // \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($document);

        //static zone
        // $templateProcessor->setValue('date', htmlspecialchars(date('d.m.Y G:i:s'))); 



        $templateProcessor = new TemplateProcessor($document);
        $font = array('name' => 'Minion Pro', 'size' => 8, 'bold' => true);
        $templateProcessor->setValue('sign', ' <w:br/> Stamped:     '.$signer->firstname .' '. $signer->lastname.'<w:br/> ${UserLogo} '. date("d-m-Y").'<w:br/> ${code}');
        // $templateProcessor->setValue('file', 'File:               '.$file->filename);
        $templateProcessor->setValue('code', $code);
        // $templateProcessor->setImageValue('CompanyLogo', $signature);
        $templateProcessor->setImageValue('UserLogo', array('path' => $stamp, 'width' => 160, 'height' => 160, 'ratio' => true));
        // echo date('H:i:s'), ' Saving the result document...';


        

        try {
                $realname= str_replace(".docx","",$file->filename);
                $templateProcessor->saveAs('storage/signed/Stamped_'.$file->filename);


                


                SignStamp::create([
                    'file' =>  $document,
                    'filename' =>  $file->filename,
                    'file_id' =>  $file->id,
                    'owner' =>  $file->user_id,
                    'action' =>   "Stamped a documented that you requested",
                    'actioneer' =>  $signer->id,
                    'signature' =>  "NO",
                    'stamp' =>  $stamp,
                    'code' =>  $code,
                  ]);

                  FileNotification::create([
                    'url' => $file->id,
                    'owner' => $file->user_id,
                    'filename' => 'Stamped_'.$file->filename,
                    'actioneer' => $signer->id,
                    'action' => "Stamped a documented that you requested",
                    'seen' =>  "0",
                    'created_at' =>  now(),
                    'updated_at' =>  now(),
                  ]);
             //Load temp file

                $phpWord = \PhpOffice\PhpWord\IOFactory::load('storage/signed/Stamped_'.$file->filename); 
                
  
             //Save it

                $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
                $xmlWriter->save('Stamped_'.$realname.'.pdf', true, 'UTF-8', false); 
            
            } catch (Exception $e) {

            } 

            // return response()->download('Signed_'.$realname.'.pdf');
            return back()->with('success','Document has been Stamped');
                // $templateProcessor->saveAs($document);

    }










    public function signstamp(File $file)
     {
        $file->filename;
        $document= "storage/files/$file->filename" ;
        $owner = User::where('id','=',$file->user_id)->first();
        $signer = Auth::user();
        $stamp = "storage/signatures/stamp.jpg" ;
        $signature = "storage/signatures/$signer->signature" ;


        // Available alpha caracters
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // generate a pin based on 2 * 7 digits + a random character
        $pin = mt_rand(1000000, 9999999)
            . mt_rand(1000000, 9999999)
            . $characters[rand(0, strlen($characters) - 1)];

        // shuffle the result
        $code = str_shuffle($pin);

        // dd($signature);

        require_once '../vendor/autoload.php';
        // \PhpOffice\PhpWord\Settings::setPdfRendererPath('../vendor/dompdf/dompdf');
        // \PhpOffice\PhpWord\Settings::setPdfRenderername('DomPDF');

         \PhpOffice\PhpWord\Settings::setPdfRendererPath('../vendor/mpdf/mpdf');
         \PhpOffice\PhpWord\Settings::setPdfRenderername(Settings::PDF_RENDERER_MPDF);

        // \PhpOffice\PhpWord\Settings::setPdfRendererPath('../vendor/tecnickcom/tcpdf');
        // \PhpOffice\PhpWord\Settings::setPdfRenderername(Settings::PDF_RENDERER_TCPDF);

        
        // \PhpOffice\PhpWord\Settings::setZipClass(\PhpOffice\PhpWord\Settings::ZipArchive);

        // \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($document);

        //static zone
        // $templateProcessor->setValue('date', htmlspecialchars(date('d.m.Y G:i:s'))); 



        $templateProcessor = new TemplateProcessor($document);
        $font = array('name' => 'Minion Pro', 'size' => 8, 'bold' => true);
        $templateProcessor->setValue('sign', ' <w:br/> Signed and Stamped:     '.$signer->firstname .' '. $signer->lastname.'<w:br/> ${stamp}${sign} Date:'. date("d-m-Y").'<w:br/> ${code}');
        // $templateProcessor->setValue('file', 'File:               '.$file->filename);
        $templateProcessor->setValue('code', $code);
        // $templateProcessor->setImageValue('CompanyLogo', $signature);
        $templateProcessor->setImageValue('stamp', array('path' => $stamp, 'width' => 160, 'height' => 160, 'ratio' => true));
        $templateProcessor->setImageValue('sign', array('path' => $signature, 'width' => 100, 'height' => 80, 'ratio' => true));
        // echo date('H:i:s'), ' Saving the result document...';


        

        try {
                $realname= str_replace(".docx","",$file->filename);
                $templateProcessor->saveAs('storage/signed/Signed and Stamped_'.$file->filename);


                


                SignStamp::create([
                    'file' =>  $document,
                    'filename' =>  $file->filename,
                    'file_id' =>  $file->id,
                    'owner' =>  $file->user_id,
                    'action' =>   "Stamped and signed a documented that you requested",
                    'actioneer' =>  $signer->id,
                    'signature' =>  $signature,
                    'stamp' =>  $stamp,
                    'code' =>  $code,
                  ]);

                  FileNotification::create([
                    'url' => $file->id,
                    'owner' => $file->user_id,
                    'filename' => 'Signed and Stamped_'.$file->filename,
                    'actioneer' => $signer->id,
                    'action' => "Stamped and signed a documented that you requested",
                    'seen' =>  "0",
                    'created_at' =>  now(),
                    'updated_at' =>  now(),
                  ]);
             //Load temp file

                $phpWord = \PhpOffice\PhpWord\IOFactory::load('storage/signed/Signed and Stamped_'.$file->filename); 
                
  
             //Save it

                $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
                $xmlWriter->save('Signed and Stamped_'.$realname.'.pdf', true, 'UTF-8', false); 
            
            } catch (Exception $e) {

            } 

            // return response()->download('Signed_'.$realname.'.pdf');
            return back()->with('success','Document has been Stamped and signed');
                // $templateProcessor->saveAs($document);

    }






















                /**
                     * Display a listing of the resource.
                     *
                     * @return \Illuminate\Http\Response
                     */
                

                    public function signSheet(File $file)
                    {
                    $file->filename;
                    $document= "storage/files/$file->filename" ;
                    $owner = User::where('id','=',$file->user_id)->first();
                    $signer = Auth::user();
                    $signature = "storage/signatures/$signer->signature" ;
                    $stamp = "storage/signatures/stamp.jpg" ;


                    


                    // Available alpha caracters
                    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

                    // generate a pin based on 2 * 7 digits + a random character
                    $pin = mt_rand(1000000, 9999999)
                        . mt_rand(1000000, 9999999)
                        . $characters[rand(0, strlen($characters) - 1)];

                    // shuffle the result
                    $code = str_shuffle($pin);



                    $sigimg = Image::make($signature); 
                    $stampimg = Image::make($stamp); 

                    $date = date("d-m-Y");
                    $verification = $signer->firstname .' '. $signer->lastname .' Date: '. $date.' '.$pin;
                    $string = wordwrap($verification,15,"|");
                    //create array of lines
                    $strings = explode("|",$string);
                    $i=3; //top position of string
                    //for each line added

                    foreach($strings as $string){
                        $sigimg->text($string, 800, 100+$i, function($font) {  
                            $font->file(public_path('font/traveling_typewriter/TravelingTypewriter.ttf'));  
                            $font->size(50);  
                            $font->color('#f2071f');  
                            $font->align('center');  
                            $font->valign('top');    
                        });  
                        $i=$i+42; //shift top postition down 42
                    }
                   $sigimg->save("storage/signatures/$code$signer->signature" );
                   $vsignature = "storage/signatures/$code$signer->signature" ;

                    foreach($strings as $string){
                        $stampimg->text($string, 400, $i, function($font) {  
                            $font->file(public_path('font/traveling_typewriter/TravelingTypewriter.ttf'));  
                            $font->size(50);  
                            $font->color('#f2071f');  
                            $font->align('center');  
                            $font->valign('top');    
                        });  
                        $i=$i+42; //shift top postition down 42
                    }
                    $stampimg->save("storage/signatures/$code.jpg" );
                    $vstamp = "storage/signatures/$code.jpg" ;



                    // dd($signature);

                    require_once '../vendor/autoload.php';
                    
                    // \PhpOffice\PhpWord\Settings::setPdfRendererPath('../vendor/dompdf/dompdf');
                    // \PhpOffice\PhpWord\Settings::setPdfRenderername('DomPDF');

                        \PhpOffice\PhpWord\Settings::setPdfRendererPath('../vendor/mpdf/mpdf');
                        \PhpOffice\PhpWord\Settings::setPdfRenderername(Settings::PDF_RENDERER_MPDF);

                    // \PhpOffice\PhpWord\Settings::setPdfRendererPath('../vendor/tecnickcom/tcpdf');
                    // \PhpOffice\PhpWord\Settings::setPdfRenderername(Settings::PDF_RENDERER_TCPDF);


                    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($document);
                     
                    $worksheet = $spreadsheet->getActiveSheet();

                    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');

                    // dd($location);
                    $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                    $drawing->setName('Authorized');
                    $drawing->setDescription('Authorized');
                    $drawing->setPath($vsignature);
                    $drawing->setHeight(150);
                    $drawing->setCoordinates('F30');
                    $drawing->setOffsetX(400);
                    $drawing->setRotation(25);
                    $drawing->getShadow()->setVisible(true);
                    $drawing->getShadow()->setDirection(45);
                    $drawing->setWorksheet($spreadsheet->getActiveSheet());



                    $drawing2 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                    $drawing2->setName('Authorized');
                    $drawing2->setDescription('Authorized');
                    $drawing2->setPath($vstamp);
                    $drawing2->setHeight(120);
                    $drawing2->setCoordinates('J30');
                    $drawing2->setOffsetX(400);
                    $drawing2->setRotation(25);
                    $drawing2->getShadow()->setVisible(true);
                    $drawing2->getShadow()->setDirection(45);
                    $drawing2->setWorksheet($spreadsheet->getActiveSheet());

                    // $writer->save($code.'Readwrites.xls');
                    
                    
                    try {
                        if(pathinfo(storage_path(Storage::disk('public')->url($document)), PATHINFO_EXTENSION) =='xls'){
                            
                            $writer->save('storage/signed/Signed and Stamped_'.$pin.$file->filename);

                            SignStamp::create([
                                'file' =>  $document,
                                'filename' =>  'Signed and Stamped_'.$pin.$file->filename,
                                'file_id' =>  $file->id,
                                'owner' =>  $file->user_id,
                                'action' =>   "Stamped and signed a documented that you requested",
                                'actioneer' =>  $signer->id,
                                'signature' =>  $vsignature,
                                'stamp' =>  $vstamp,
                                'code' =>  $code,
                              ]);
            
                              FileNotification::create([
                                'url' => $file->id,
                                'owner' => $file->user_id,
                                'filename' => 'Signed and Stamped_'.$pin.$file->filename,
                                'actioneer' => $signer->id,
                                'action' => "Stamped and signed a documented that you requested",
                                'seen' =>  "0",
                                'created_at' =>  now(),
                                'updated_at' =>  now(),
                              ]);

                        }elseif(pathinfo(storage_path(Storage::disk('public')->url($document)), PATHINFO_EXTENSION) =='xlsx'){
                           
                            
                            $realname= str_replace(".xlsx",".xls",$file->filename);
                            $writer->save('storage/signed/Signed and Stamped_'.$pin.$realname);
                            SignStamp::create([
                                'file' =>  $document,
                                'filename' =>  $pin.$realname,
                                'file_id' =>  $file->id,
                                'owner' =>  $file->user_id,
                                'action' =>   "Stamped and signed a documented that you requested",
                                'actioneer' =>  $signer->id,
                                'signature' =>  $vsignature,
                                'stamp' =>  $vstamp,
                                'code' =>  $code,
                              ]);
            
                              FileNotification::create([
                                'url' => $file->id,
                                'owner' => $file->user_id,
                                'filename' => 'Signed and Stamped_'.$pin.$realname,
                                'actioneer' => $signer->id,
                                'action' => "Stamped and signed a documented that you requested",
                                'seen' =>  "0",
                                'created_at' =>  now(),
                                'updated_at' =>  now(),
                              ]);
                        }
                        elseif(pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION) =='csv'){
                           
                            
                            $realname= str_replace(".csv",".xls",$file->filename);
                            $writer->save('storage/signed/Signed and Stamped_'.$pin.$realname);
                            SignStamp::create([
                                'file' =>  $document,
                                'filename' =>  $pin.$realname,
                                'file_id' =>  $file->id,
                                'owner' =>  $file->user_id,
                                'action' =>   "Stamped and signed a documented that you requested",
                                'actioneer' =>  $signer->id,
                                'signature' =>  $vsignature,
                                'stamp' =>  $vstamp,
                                'code' =>  $code,
                              ]);
            
                              FileNotification::create([
                                'url' => $file->id,
                                'owner' => $file->user_id,
                                'filename' => 'Signed and Stamped_'.$pin.$realname,
                                'actioneer' => $signer->id,
                                'action' => "Stamped and signed a documented that you requested",
                                'seen' =>  "0",
                                'created_at' =>  now(),
                                'updated_at' =>  now(),
                              ]);
                        }
                            

                            
                        } catch (Exception $e) {

                        } 
                       
                            // return response()->download($code.'Readwrites.xls');
                        

                        
                        return back()->with('success','Document has been Authorized');
                            // $templateProcessor->saveAs($document);

                }




















   /**
                     * Display a listing of the resource.
                     *
                     * @return \Illuminate\Http\Response
                     */
                

                    public function authImage(File $file)
                    {
                    $file->filename;
                    $document= "storage/files/$file->filename" ;
                    $owner = User::where('id','=',$file->user_id)->first();
                    $signer = Auth::user();
                    $signature = "storage/signatures/$signer->signature" ;
                    $stamp = "storage/signatures/stamp.jpg" ;


                    


                    // Available alpha caracters
                    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

                    // generate a pin based on 2 * 7 digits + a random character
                    $pin = mt_rand(1000000, 9999999)
                        . mt_rand(1000000, 9999999)
                        . $characters[rand(0, strlen($characters) - 1)];

                    // shuffle the result
                    $code = str_shuffle($pin);



                    $auth = Image::make($document); 
                    $stampimg = Image::make($stamp); 

                    $date = date("d-m-Y");
                    $verification = $signer->firstname .' '. $signer->lastname .' Date: '. $date.' '.$pin;
                    $string = wordwrap($verification,15,"|");
                    //create array of lines
                    $strings = explode("|",$string);
                    $i=3; //top position of string
                    //for each line added

                    foreach($strings as $string){
                        $auth->text($string, 100, 50+$i, function($font) {  
                            $font->file(public_path('font/traveling_typewriter/TravelingTypewriter.ttf'));  
                            $font->size(20);  
                            $font->color('#f2071f');  
                            $font->align('center');  
                            $font->valign('top');    
                        });  
                        $i=$i+42; //shift top postition down 42
                    }
                   $auth->save("storage/signed/Authorized_$pin$file->filename" );

                    foreach($strings as $string){
                        $stampimg->text($string, 400, $i, function($font) {  
                            $font->file(public_path('font/traveling_typewriter/TravelingTypewriter.ttf'));  
                            $font->size(50);  
                            $font->color('#f2071f');  
                            $font->align('center');  
                            $font->valign('top');    
                        });  
                        $i=$i+42; //shift top postition down 42
                    }
                    $stampimg->save("storage/signatures/$code.jpg" );
                    $vstamp = "storage/signatures/$code.jpg" ;

                    
                    
                    try {
                            // $writer->save('storage/signed/Authorized_'.$file->filename);

                            SignStamp::create([
                                'file' =>  $document,
                                'filename' =>  'Authorized_'.$pin.$file->filename,
                                'file_id' =>  $file->id,
                                'owner' =>  $file->user_id,
                                'action' =>   "Authorized a documented that you requested",
                                'actioneer' =>  $signer->id,
                                'signature' =>  $vstamp,
                                'stamp' =>  $vstamp,
                                'code' =>  $code,
                              ]);
            
                              FileNotification::create([
                                'url' => $file->id,
                                'owner' => $file->user_id,
                                'filename' => 'Authorized_'.$pin.$file->filename,
                                'actioneer' => $signer->id,
                                'action' => "Authorized a documented that you requested",
                                'seen' =>  "0",
                                'created_at' =>  now(),
                                'updated_at' =>  now(),
                              ]);

                        
                            
                        } catch (Exception $e) {

                        } 
                       
                            // return response()->download($code.'Readwrites.xls');
                        

                        
                        return back()->with('success','Document has been Authorized');
                            // $templateProcessor->saveAs($document);

                }


            }