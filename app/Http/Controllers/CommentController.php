<?php

namespace App\Http\Controllers;

use App\Comment;
use App\FileNotification;
use App\Reply;
use App\File;
use Illuminate\Http\Request;
use Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'file_id' => 'required',
            'filename' => 'required',
            'folder_id' => 'required',
            'path' => 'required',
            'owner_id' => 'required',
            'commenter_id' => 'required',
            'comment' => 'required',
            'url' => 'required',
          ]);

          Comment::create([
            'file_id' => $request->get('file_id'),
            'filename' => $request->get('filename'),
            'folder_id' => $request->get('folder_id'),
            'path' =>  $request->get('path'),
            'owner_id' =>  $request->get('owner_id'),
            'commenter_id' =>  $request->get('commenter_id'),
            'comment' =>  $request->get('comment'),
            'status' =>  "Open",
          ]);

          FileNotification::create([
            'url' => $request->get('url'),
            'filename' => $request->get('filename'),
            'owner' =>  $request->get('owner_id'),
            'actioneer' =>  $request->get('commenter_id'),
            'action' =>   "commented on a file you uploaded",
            'seen' =>  "0",
          ]);




           return back()->with('toast_success', 'Commented');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
    }

    /**
     * close the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function closeComment(Comment $comment)
    {
        $user = Auth::user();
        if ($user->id == $comment->commenter_id) {
            $replies = Reply::where('comment_id','=',$comment->id)->get();
            if (count($replies) > 0) {
                foreach ($replies as $reply) {
                    $reply->status = "Closed";
                    $reply->save();
                }
            }
            $comment->status = "Closed";
            $comment->save();
            return back()->with('toast_success', 'Comment closed');
        } else {
            return back()->with('toast_warning', 'This comment cannot be closed by you !');
        }  
    }

    /**
     * close the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function printComment($file)
    {
        $user = Auth::user();
        $comments = Comment::where('file_id', '=', $file)->get();

        return view('comments')->with([
            'comments' => $comments
        ]);
             
    }
}
