<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response as FacadeResponse;
use Auth;
use User;
use App\File;
use App\Share;
use App\Folder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
Use Alert;



class FileController extends Controller
{

    public function __construct()
    {
        // This will block anyone who is not registered from continuing with this request
        $this->middleware('auth');
    }

    public function push()
    {
        $files= File::all()->sortByDesc('created_at');
        $folders= Folder::where('preceeding_folder_id','=','0')->get();
        if($files){
        foreach($files as $file){
            $extension = pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION);
            $ext= $extension;
        }

       
      
        $user= Auth::user();

        // $extension = $file->getClientOriginalExtension();

      return view('push')->with ([
          'user'=>  $user,
          'files'=> $files,
          'folders'=> $folders,
        //   'ext'=> $ext,
      ]);
        }
        return view('push')->with ([
            'user'=>  $user,
        ]);
    }

    public function homelist()
    {
        $user= Auth::user();
        $publicFolders= Folder::where('preceeding_folder_id','=','0')->where('permision','=','all')->get();
        $privateFolders= Folder::where('preceeding_folder_id','=','0')->where('user_id','=',$user->id)->where('permision','=','me')->get();


        $files= File::all()->sortByDesc('created_at');
        if($files){
        foreach($files as $file){
            $extension = pathinfo(storage_path(Storage::disk('public')->url("files/$file->filename")), PATHINFO_EXTENSION);
            $ext= $extension;
        }
        $privateFiles= File::where('folder_id','=','0')->where('user_id','=',$user->id)->where('permision','=','me')->get();

        foreach($privateFiles as $privateFile){
            $extension = pathinfo(storage_path(Storage::disk('local')->url("files/$privateFile->filename")), PATHINFO_EXTENSION);
        }
       
      
        $user= Auth::user();

        // $extension = $file->getClientOriginalExtension();

      return view('homelist')->with ([
          'user'=>  $user,
          'files'=> $files,
          'privateFiles'=> $privateFiles,
          'publicFolders'=> $publicFolders,
          'privateFolders'=> $privateFolders,
        
      ]);
    }
    return view('push')->with ([
        'user'=>  $user,
    ]);

    }



    public function store(Request $request)
    {
        // dd($request->get('permision'));
       $request->validate([
        'permision' => 'required',
        'file' => 'required',
        'file.*' => 'mimes:doc,pdf,docx,zip',
        // 'user' => 'required'
      ]);

     

      if($request->hasfile('filename'))
      {

        $file=$request->file('filename');
         
             $name=$file->getClientOriginalName();

            
             $file->move(public_path().'/files/', $name);  
             $data[] = $name;  
         
      }

      $file= new \FileUp();
      $file->filename=json_encode($data);
      $file->permision=$request->get('permision');
      
     
     $file->save();


    //    $files=DB::table('files');
       
    //    $files->create([
    //     'filename' => $request->get('filename'),
    //     'overview' => $request->get('overview'),
    //     'user' => $request->get('user')
    //   ]);

      return back()->with('toast_success', 'Your file is submitted Successfully');
    }






    public function upload(Request $request)
    {

        
        $user= Auth::user();
         
      $uploadedFile = $request->file('file');
      $filename =$uploadedFile->getClientOriginalName();


      Storage::disk('public')->putFileAs('files/',$uploadedFile, $filename);

      $upload = new File();
      $upload->filename = $filename;
      $upload->user_id = $user->id;
      $upload->permision = $request->get('permision');
      $upload->folder_id = '0';

    //   dd( $upload->permision);
    //   $upload->user()->associate(auth()->user());
//   dd($upload->filename);
      $upload->save();
      

      return  back()->with('toast_success', 'Your file is submitted Successfully');
    }

    public function getFile($filename)
    {
        // The Response class has the helper function response() that you can use
        return response()->download(storage_path($filename), null, [], null);
    }



//      /**
//      * Get an array of all files in a directory.
//      *
//      * @param  string|null  $directory
//      * @param  bool  $recursive
//      * @return array
//      */
//     public function files($directory = null, $recursive = false)
//     {
//         $contents = $this->driver->listContents($directory, $recursive);
//         return $this->filterContentsByType($contents, 'file');
//     }







///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function pdf(File $file)
    {
        $file->filename;
        $fileContents = Storage::disk('public')->get("files/$file->filename");
        $response = FacadeResponse::make($fileContents, 200);
        $response->header('Content-Type', "application/pdf");
        $response= str_replace(' ', '%20',"storage/files/$file->filename") ;

        return view('pdf')->with ([
            'response'=>  $response,
            'file'=>  $file,
        ]);
    }

    public function text(File $file)
    {
        $file->filename;
        
        $response= "storage/files/$file->filename" ;
    
        return view('text')->with ([
            'response'=>  $response,
            'file'=>  $file,
        ]);
    }
    public function sheet(File $file)
    {
        $file->filename;
        $fileContents = Storage::disk('public')->get("files/$file->filename");
        $response = FacadeResponse::make($fileContents, 200);
        $response->header('Content-Type', "sheets/xls.csv");
        $response= "storage/files/$file->filename" ;

        return view('sheet')->with ([
            'response'=>  $response,
            'file'=>  $file,
        ]);
    }
    public function slide(File $file)
    {
        $file->filename;
        $fileContents = Storage::disk('public')->get("files/$file->filename");
        $response = FacadeResponse::make($fileContents, 200);
        $response->header('Content-Type', "application/pptx");
        $response= "storage/files/$file->filename" ;

        return view('slide')->with ([
            'response'=>  $response,
            'file'=>  $file,
        ]);
    }

    public function image(File $file)
    {
        $file->filename;
        $fileContents = Storage::disk('public')->get("files/$file->filename");
        $response = FacadeResponse::make($fileContents, 200);
        $response->header('Content-Type', "image/*");
        $response= "storage/files/$file->filename" ;

        return view('image')->with ([
            'response'=>  $response,
            'file'=>  $file,
        ]);
    }
    public function audio(File $file)
    {
        $file->filename;
        $fileContents = Storage::disk('public')->get("files/$file->filename");
        $response = FacadeResponse::make($fileContents, 200);
        $response->header('Content-Type', "audio/mp3");
        $response= "storage/files/$file->filename" ;

        return view('audio')->with ([
            'response'=>  $response,
            'file'=>  $file,
        ]);
    }

    

    public function Video(File $file)
    {
        $file->filename;
        $fileContents = Storage::disk('public')->get("files/$file->filename");
        $response = FacadeResponse::make($fileContents, 200);
        $response->header('Content-Type', "video/mp4");
        $response= "storage/files/$file->filename" ;

        return view('video')->with ([
            'response'=>  $response,
            'file'=>  $file,
        ]);
    }


    
    



    

    










    public function deleteFile(File $file)
    {
        $file->filename;
        // dd($file->id);

        $item = File::where('id','=',$file->id)->first();

        $shares = Share::where('file_id','=',$file->id)->get();
        
        foreach ($shares as $share){
            // dd($share->id);
            $share->delete();
        }
        
        $item->delete();

        Storage::disk('public')->delete("files/$file->filename");

        return  back()->with('toast_success', 'File Deleted Successfully');


    }










}
