<?php

namespace App\Http\Controllers;

use App\FileNotification;
use Illuminate\Http\Request;
use DateTime;
use Auth;

class FileNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FileNotification  $fileNotification
     * @return \Illuminate\Http\Response
     */
    public function show(FileNotification $fileNotification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FileNotification  $fileNotification
     * @return \Illuminate\Http\Response
     */
    public function edit(FileNotification $fileNotification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FileNotification  $fileNotification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FileNotification $fileNotification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FileNotification  $fileNotification
     * @return \Illuminate\Http\Response
     */
    public function destroy(FileNotification $fileNotification)
    {
        //
    }



    public function filenotificationview($filenotification)

    {

        $time = new DateTime();
        $time->format('Y-m-d H:i:s');

        $display = FileNotification::where('id','=',$filenotification)->first();
       
        $user = Auth::user();
       
        $display->seen = '1';

        $display->save();
                $displayType = 'file';
            return view('notificationview')->with([
                
                'displayf' => $display, 
                'displayType'=> $displayType, 
                
                ]);
                
       
    }



    public function filenotificationDelete(FileNotification $filenotification)

    {
        
        $filenotification->delete();
                
            return redirect()->route('admin.notification.index')->with('toast_success', 'Notification successfully removed.');
                
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function requestSignature(Request $request)
    {
        
        $request->validate([
            'url' => 'required',
            'filename' => 'required',
            'reciepient_id' => 'required',
            'sender' => 'required',
          ]);
          for ($i = 0; $i < count($request->reciepient_id); $i++) {

            $reciepients[] = [
                'url' => $request->url,
                'owner' => $request->reciepient_id[$i],
                'filename' => $request->filename,
                'actioneer' => $request->sender,
                'action' => "Requested your signature on a document",
                'seen' =>  "0",
                'created_at' =>  now(),
                'updated_at' =>  now(),
            ];
         }

        FileNotification::insert($reciepients);



           return back()->with('toast_success', 'Request for signature on this document was sent Successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function requestStamp(Request $request)
    {
        
        $request->validate([
            'url' => 'required',
            'filename' => 'required',
            'reciepient_id' => 'required',
            'sender' => 'required',
          ]);
          for ($i = 0; $i < count($request->reciepient_id); $i++) {

            $reciepients[] = [
                'url' => $request->url,
                'owner' => $request->reciepient_id[$i],
                'filename' => $request->filename,
                'actioneer' => $request->sender,
                'action' => "Requested you to stamp a document",
                'seen' =>  "0",
                'created_at' =>  now(),
                'updated_at' =>  now(),
            ];
         }

        FileNotification::insert($reciepients);



           return back()->with('toast_success', 'Request for a stamp on this document was sent Successfully');
    }

    public function requestSignatureStamp(Request $request)
    {
        
        $request->validate([
            'url' => 'required',
            'filename' => 'required',
            'reciepient_id' => 'required',
            'sender' => 'required',
          ]);
          for ($i = 0; $i < count($request->reciepient_id); $i++) {

            $reciepients[] = [
                'url' => $request->url,
                'owner' => $request->reciepient_id[$i],
                'filename' => $request->filename,
                'actioneer' => $request->sender,
                'action' => "Requested you to Sign and stamp a document",
                'seen' =>  "0",
                'created_at' =>  now(),
                'updated_at' =>  now(),
            ];
         }

        FileNotification::insert($reciepients);



           return back()->with('toast_success', 'Request for signature and stamp on this document was sent Successfully');
    }


    public function requestsheetSignatureStamp(Request $request)
    {
        
        $request->validate([
            'url' => 'required',
            'filename' => 'required',
            'reciepient_id' => 'required',
            'sender' => 'required',
          ]);
          for ($i = 0; $i < count($request->reciepient_id); $i++) {

            $reciepients[] = [
                'url' => $request->url,
                'owner' => $request->reciepient_id[$i],
                'filename' => $request->filename,
                'actioneer' => $request->sender,
                'action' => "Requested you to Sign and stamp a Spreadsheet",
                'seen' =>  "0",
                'created_at' =>  now(),
                'updated_at' =>  now(),
            ];
         }

        FileNotification::insert($reciepients);



           return back()->with('toast_success', 'Request for signature and stamp on this Sheet was sent Successfully');
    }
    

    public function requestAuthImage(Request $request)
    {
        
        $request->validate([
            'url' => 'required',
            'filename' => 'required',
            'reciepient_id' => 'required',
            'sender' => 'required',
          ]);
          for ($i = 0; $i < count($request->reciepient_id); $i++) {

            $reciepients[] = [
                'url' => $request->url,
                'owner' => $request->reciepient_id[$i],
                'filename' => $request->filename,
                'actioneer' => $request->sender,
                'action' => "Requested you to Authorize an Image File",
                'seen' =>  "0",
                'created_at' =>  now(),
                'updated_at' =>  now(),
            ];
         }

        FileNotification::insert($reciepients);
           return back()->with('toast_success', 'Request for Authorization on this File was sent Successfully');
    }

}
