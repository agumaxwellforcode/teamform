<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
   protected $fillable=[
       'announcement_id','sender_id','recipient_user_id','	seen','time_sent','	time_seen'
   ];
}
